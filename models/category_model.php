

<?php


class Category_Model extends Model{

    public $exc;

    public function __construct(){
        parent::__construct();
        $this->exc = 0;
    }

    public function getPopularBrands(){
        $this->exc = $this->db->prepare("SELECT brandID, brandName, brandPath, brandLogo, brandLikes, brandCollects, brandItems
         FROM brands ORDER BY brandLikes DESC LIMIT 4"); // to change the 10
        //$sth = $this->db->prepare("SELECT p.prodID, p.prodLikes, b.brandID, b.brandName, b.brandPath, b.brandLogo, b.brandLikes
        //                          FROM products AS p INNER JOIN brands AS b ON (p.brandID = b.brandID) ORDER BY b.brandLikes DESC LIMIT 10");
        return $this->exc->execute();
    }

    public function getPopularBrandsItems($brandID){
        $this->exc = $this->db->prepare("SELECT p.prodID, p.prodLikes, p.prodPhotoPath, p.prodPhotoName
                                  FROM products AS p
                                  WHERE p.brandID = :brandID ORDER BY p.prodLikes DESC LIMIT 4");
        return $this->exc->execute(array(':brandID'=>$brandID));
    }

    public function getHottestItems(){
        $this->exc = $this->db->prepare("SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
                                        p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
                                        p.prodPhotoName,
                                         b.brandName, b.brandPath, b.brandLogo
                                         FROM products AS p INNER JOIN brands AS b
                                         ON (p.brandID = b.brandID)
                                         ORDER BY p.prodLikes DESC LIMIT 10"); // to change the 10
        return $this->exc->execute();
        /**
         * For Normal SQL Just ORDER By created time
         */
         /* SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
            p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
            p.prodPhotoName,
             b.brandName, b.brandPath, b.brandLogo
             FROM products AS p INNER JOIN brands AS b
             ON (p.brandID = b.brandID)
             ORDER BY prodLikes DESC LIMIT 10
         */

         /**
         * For Date different Products find and SQL
         */
         /* $this->exc = $this->db->prepare("SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
            p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
            p.prodPhotoName,
             b.brandName, b.brandPath, b.brandLogo
             FROM products AS p INNER JOIN brands AS b
             ON (p.brandID = b.brandID)
         WHERE p.prodDateCreated >= DATE_SUB(CURDATE(), INTERVAL 2 WEEK)
         ORDER BY p.prodLikes DESC LIMIT 10");
         *
         * 1 month
         * 14 day
         */
    }

    public function getNewItems(){
        $this->exc = $this->db->prepare("SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
                                        p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
                                        p.prodPhotoName,
                                         b.brandName, b.brandPath, b.brandLogo
                                         FROM products AS p INNER JOIN brands AS b
                                         ON (p.brandID = b.brandID)
                                         ORDER BY p.prodDateCreated DESC LIMIT 10");

        return $this->exc->execute();
        /**
         * For Normal SQL Just ORDER By created time
         */
         /* SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
            p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
            p.prodPhotoName,
             b.brandName, b.brandPath, b.brandLogo
             FROM products AS p INNER JOIN brands AS b
             ON (p.brandID = b.brandID)
             ORDER BY p.prodDateCreated DESC LIMIT 10
         */

         /**
         * For Date different Products find and SQL
         * $this->exc = $this->db->prepare("SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
         * p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
         * p.prodPhotoName,
         * b.brandName, b.brandPath, b.brandLogo
         * FROM products AS p INNER JOIN brands AS b
         * ON (p.brandID = b.brandID)
         * WHERE p.prodDateCreated >= DATE_SUB(CURDATE(), INTERVAL 2 WEEK)
         * ORDER BY p.prodDateCreated DESC LIMIT 10");
         *
         * 1 month
         * 14 day
         */

    }

    public function getCelebItems(){
        $this->exc = $this->db->prepare("SELECT r.prodID, r.celebID, r.reviewPath, r.reviewPhotoName, r.reviewLikes, r.reviewCollects,
                                        c.celebName, c.celebPath, c.celebPhotoName,
                                        p.prodTitle, p.price, p.newPrice, p.priceEvent, p.prodReviews, p.prodRating
                                         FROM review AS r INNER JOIN celebs AS c
                                         ON (r.celebID = c.celebID)
                                         INNER JOIN products AS p
                                         ON (r.prodID = p.prodID)
                                         ORDER BY r.reviewLikes DESC LIMIT 10");
        return $this->exc->execute();
        /**
         * For Normal SQL Just ORDER By created time
         */
         /* SELECT r.prodID, r.celebID, r.reviewPath, r.reviewPhotoName, r.reviewLikes, r.reviewCollects,
            c.celebName, c.celebPath, c.celebPhotoName,
            p.prodTitle, p.price, p.newPrice, p.priceEvent, p.prodReviews, p.prodRating
             FROM review AS r INNER JOIN celebs AS c
             ON (r.celebID = c.celebID)
             INNER JOIN products AS p
             ON (r.prodID = p.prodID)
             ORDER BY r.reviewLikes DESC LIMIT 10
         */

         /**
         * For Date different Products find and SQL
         */
         /*SELECT r.prodID, r.celebID, r.reviewPath, r.reviewPhotoName, r.reviewLikes, r.reviewCollects,
        c.celebName, c.celebPath, c.celebPhotoName,
        p.prodTitle, p.price, p.newPrice, p.priceEvent, p.prodReviews, p.prodRating
         FROM review AS r INNER JOIN celebs AS c
         ON (r.celebID = c.celebID)
         INNER JOIN products AS p
         ON (r.prodID = p.prodID)
         WHERE r.reviewCreated >= DATE_SUB(CURDATE(), INTERVAL 2 WEEK)
         ORDER BY r.reviewLikes DESC LIMIT 10");
         *
         * 1 month
         * 14 day
         */
    }

    /**
     * get Recently Items from Brands products
     * pageNum : 초기 0이면 DISP_NUM의 2배의 데이터를 받아서 처리함
     */
    public function initGetDisplayItems(){
        $limit = DISP_NUM;
        $this->exc = $this->db->prepare("SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
                                        p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
                                        p.prodPhotoName,
                                         b.brandName, b.brandPath, b.brandLogo
                                         FROM products AS p INNER JOIN brands AS b
                                         ON (p.brandID = b.brandID)
                                         ORDER BY prodDateCreated DESC LIMIT $limit");
        return $this->exc->execute();
    }
    public function getDisplayItems($pageNum, $sortType, $filterType){
        $limit = 'LIMIT ' .($pageNum) * DISP_NUM .',' .DISP_NUM;
        if($sortType == 'date'){
            $order = 'prodDateCreated DESC';
            //echo '1111'; die;
        } else if($sortType == 'likes'){
            $order = 'prodLikes DESC, prodDateCreated DESC';
            //echo '2222'; die;
        } else if($sortType == 'collects'){
            $order = 'prodCollects DESC, prodDateCreated DESC';
            //echo '3333'; die;
        } else if($sortType == 'rated'){
            $order = 'prodRating DESC, prodDateCreated DESC';
        } else if($sortType == 'phigh'){
            $order = 'newPrice DESC, prodDateCreated DESC';
        } else if($sortType == 'plow'){
            $order = 'newPrice ASC, prodDateCreated DESC';
        } else {
            return false;
        }
        $this->exc = $this->db->prepare("SELECT p.prodID, p.brandID, p.prodTitle, p.price, p.newPrice, p.priceEvent,
                                        p.prodLikes, p.prodCollects, p.prodReviews, p.prodRating, p.prodPhotoPath,
                                        p.prodPhotoName, p.prodDateCreated,
                                         b.brandName, b.brandPath, b.brandLogo
                                         FROM products AS p INNER JOIN brands AS b
                                         ON (p.brandID = b.brandID)
                                         ORDER BY $order $limit");
        return $this->exc->execute();
    }

    // 전체
    public function findTotalNumOfItems(){
        $result = $this->db->select("SELECT COUNT(p.prodID)
                                FROM products AS p INNER JOIN brands AS b
                                ON (p.brandID = b.brandID)", array(),1);
        return $result['COUNT(p.prodID)'];
    }

    /**
     * get Recently Items from Celeb's review
     */
    public function getDisplayReviews(){
        $this->exc = $this->db->prepare("SELECT r.prodID, r.celebID, r.reviewPath, r.reviewPhotoName, r.reviewLikes, r.reviewCollects,
                                        c.celebName, c.celebPath, c.celebPhotoName,
                                        p.prodTitle, p.price, p.newPrice, p.priceEvent, p.prodReviews, p.prodRating
                                         FROM review AS r INNER JOIN celebs AS c
                                         ON (r.celebID = c.celebID)
                                         INNER JOIN products AS p
                                         ON (r.prodID = p.prodID)
                                         ORDER BY r.reviewLikes DESC LIMIT 40");
        return $this->exc->execute();
    }




}