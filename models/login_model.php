

<?php


class Login_Model extends Model{

    public $lastId;

    public function __construct(){
        parent::__construct();
    }

    public function login($email, $set){
        if($set == 1) {
            return $this->db->select('SELECT id, username, password, activated FROM users WHERE email = :email LIMIT 1',
                array(':email' => $email), 1);
        } else {
            return $this->db->select('SELECT id, username, password, activated FROM users WHERE username = :username LIMIT 1',
                array(':username' => $email), 1);
        }
    }

    public function setSessionPass($id, $salt, $pSession){
        $sth = $this->db->prepare("UPDATE useroptions SET spass=:spass, spass_key=:spass_key
            WHERE id=:id LIMIT 1");
        $sth->execute(array(':spass'=>$pSession, ':spass_key'=>$salt,':id'=>$id));
    }

    public function updateLoginInfo($ip, $username){
        $sth = $this->db->prepare("UPDATE users SET ip=:ip, lastlogin=now()
            WHERE username=:username LIMIT 1");
        $sth->execute(array(':ip'=>$ip, ':username'=>$username));
        //UPDATE users SET ip='dshjadh', lastlogin=now() WHERE username='wangjacsi' LIMIT 1
    }
}