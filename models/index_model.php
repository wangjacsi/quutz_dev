

<?php 


class Index_Model extends Model{

	public function __construct(){
		parent::__construct();
	}

	public function getUserList(){	
		/*return $this->db->select("SELECT username, avatar FROM users WHERE avatar IS NOT NULL  
		 	ORDER BY RAND() LIMIT 32", array(), 0);
		 return $this->db->select("SELECT username, avatar FROM users WHERE avatar IS NOT NULL  
		 	AND activated ='1' ORDER BY RAND() LIMIT 32", array(), 0);*/
		 return $this->db->select("SELECT username, avatar FROM users WHERE activated ='1' ORDER BY RAND() LIMIT 40", array(), 0);
		 //print_r($this->db->rowNum);		 						
	}

	public function getUserNum(){
		$result = $this->db->select("SELECT COUNT(id) FROM users WHERE activated='1'", array(), 1);
  		return $result['COUNT(id)']; //need check
	}

	public function getGroupList(){
		return $this->db->select("SELECT name,logo FROM groups ORDER BY RAND() LIMIT 40", array(), 0);
	}

	//my group list
	/*public function getMyGroupList($logUserName){
		return $this->db->select("SELECT gm.gname,gp.logo FROM gmembers AS gm LEFT JOIN 
                groups AS gp ON gp.name=gm.gname WHERE gm.mname=:mname", array(':mname'=>$logUserName), 0);
	}*/

}