

<?php


class Signup_Model extends Model{

    public $lastId;

    public function __construct(){
        parent::__construct();
    }

    public function userNameCheck($username){
         $this->db->select('SELECT id FROM users WHERE username = :username LIMIT 1',
            array(':username' => $username), 1);
         //print_r($this->db->rowNum);
         //die;
        return $this->db->rowNum;
    }

    public function emailCheck($email){
        $this->db->select('SELECT id FROM users WHERE email = :email LIMIT 1',
            array(':email' => $email), 1);
        //print_r($this->db->rowNum);
        //die;
        return $this->db->rowNum;
    }

    public function userAdd($data){
        $this->db->insert('users', $data);
        $this->lastId = $this->db->lastInsertId();
        return $this->lastId;
    }

    public function userOptionAdd($data){
        return $this->db->insert('useroptions', $data);
    }

    public function userProfileAdd($data){
        return $this->db->insert('users_profile', $data);
    }

    public function userLikes($data){
        return $this->db->insert('likes', $data);
    }

    public function setSessionPass($id, $salt, $pSession){
        $sth = $this->db->prepare("UPDATE useroptions SET spass=:spass, spass_key=:spass_key
            WHERE id=:id LIMIT 1");
        $sth->execute(array(':spass'=>$pSession, ':spass_key'=>$salt,':id'=>$id));
    }
}