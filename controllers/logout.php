

<?php

class Logout extends Controller {

    function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->sessionClose();
        $this->cookieClose();
        $this->finalCheck();
    }

    public function sessionClose(){
        Session::start();
        // Set session data to an empty array
        $_SESSION = array();
        // Destroy the session variables
        Session::destroy();
    }

    public function cookieClose(){
        if(isset($_COOKIE["id"])){
            Cookie::destroy('id');
        }
        if(isset($_COOKIE["user"])){
            Cookie::destroy('user');
        }
        if(isset($_COOKIE["pass"])){
            Cookie::destroy('pass');
        }
        /*if(isset($_COOKIE["rem"])){
            Cookie::destroy('rem');
        }
        if(isset($_COOKIE["remin"])){
            Cookie::destroy('remin');
        }*/
        $_COOKIE = array();
    }

    public function finalCheck(){
        if(isset($_SESSION['username'])){
            header('location: '. URL .'log/message?msg=Logout_Failed&code=0x0015&file=logout');
        }
        else{
            header('location: '. URL );
            exit();
        }
    }


}


