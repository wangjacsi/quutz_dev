<?php
class brands extends Controller{
    // common variables
    public $login;
    public $loadJSFiles;
    public $loadCSSFiles;
    public $naviMenu;
    public $_urlLen;
    public $_url;
    public $_page;
    public $_loadJS;
    public $_loadCSS;
    public $_storeID;
    public $_storeName;
    public $_storeSubFunction;
    public $_setJSValueFlag;

    // specific variables
    public $subFunction;
    public $subFuncArray;
    public $searchArray;
    public $searchKey;

    function __construct(){
        parent::__construct();
        $this->view->topMiddleUse = 0;
        $this->subFunction = 0;
        $this->subFuncArray = array('search', 'favorite', 'new_brands', 'event', 'store', 'category');
        $this->storeSubFuncArray = array('category', 'info','contact','celebs','followers','event','upload');
        $this->view->searchKey = '';
        $this->searchKey = '';
        $this->view->searchForm = 0;
        $this->_page = 'search';
        $this->_storeID = '';
        $this->_storeName = '';
        $this->_storeSubFunction='';

        $this->view->mainTitle = '';
        $this->view->subTitle = '';
        $this->view->category = '';
        $this->searchArray = array();
        // load files initial setting
        $this->_loadJS = array('brands');
        $this->_loadCSS = array('brands');
        // set for java variables setting
        $this->_setJSValueFlag = 0;
    }
    /**
     * $urlLen : URL length
     * $url : input url path array
     * URL[0~3]:brands, subfunctions:search-favorite-new-event, A-Z-ETC, brand name
     */
    function index($urlLen, $url){
        $this->_urlLen = $urlLen;
        $this->_url = $url;
        // if $urlLen = 1 and then just go to brands
        if($urlLen==1){
            //initialization : user security check
            $this->init();
            // set the navigation
            $this->setNavi();
            //load CSS
            $this->loadCSS();
            //load JS
            $this->loadJS();
            // Viewer install
            $this->viewRender('index');
        } else {
            // [1]: Search/a-z, Favorite, newbrands, event, store/brandname
            $this->subFunction = $url[1];
            if (in_array($this->subFunction, $this->subFuncArray)) {
                //initialization : user security check
                $this->init();
                // specific function call
                $this->{$this->subFunction}();
                // set the navigation
                $this->setNavi();
                //load CSS
                $this->loadCSS();
                //load JS
                $this->loadJS();
                // Viewer install
                $this->viewRender($this->_page);
                if($this->_setJSValueFlag==1){
                    $this->setJava();
                }
            } else {
                header('location: '. URL.'error' );
                exit();
            }
        }
    }

    public function search(){
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['search']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['search']['main'];
        $this->view->searchForm = 1;
        $this->_page = 'search';
        // URL[0~3]:brands, subfunctions:search-favorite-new-event, A-Z-ETC, brand name
        if($this->_urlLen > 3){  // have a brand name
            // go to brand's page :  redirection
            // we need to check the brand name from client form data even if made by server side
            $this->searchKey = Validate::wordSet($this->_url[2]); // only for letter not sensitive in capital
            $this->_storeName = Validate::fileNameSet($this->_url[3]); // include space
            $this->searchArray = array($this->searchKey, $this->_storeName);
            // if check the DB and is there in DB redirect the link to that brand page
            $result = $this->model->checkBrand($this->_storeName);
            if($result != 0){
                header('location: '. URL.'brands/store/'.$result['id'].'-'.$result['name'] );
                exit();
            } else {
                header('location: '. URL.'brands/search' );
                exit();
            }
        } else if($this->_urlLen == 3){
            // go to search page
            // key: a-z, etc: not start with alphabet
            // user summit find form text come to here
            $this->searchKey = Validate::wordSet($this->_url[2]); // only for letter not sensitive in capital
            $this->view->searchKey = $this->searchKey;
            $this->searchArray = array($this->searchKey);
        }
    }

    public function favorite(){
        // need to show the whole categories
        $this->_page = 'favorite';
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['favorite']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['favorite']['main'];
        $this->view->subTitle2 = $PAGETITLES['brands']['favorite']['sub2'];
        $this->view->mainTitle2 = $PAGETITLES['brands']['favorite']['main2'];
        if($this->_urlLen > 2){ // if have a category
            $this->searchArray = array($this->_url[2]);
            $this->view->category = preg_replace('/_and_/', ' & ', $this->_url[2]);
            $this->view->category = preg_replace('/_/', ' ', $this->view->category);
        }
    }

    public function new_brands(){
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['new']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['new']['main'];
        $this->_page = 'search';
    }

    public function event(){
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['event']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['event']['main'];
        $this->_page = 'search';
    }

    public function store(){
        // [0]:brands, [1]:store, [2]:ID-Name, [3]:
        // stroe page must be length more than 3
        // if less than 3 go to Home redirection
        if($this->_urlLen < 3){
            header('location: '. URL );
            exit();
        } else {
            $this->storeinit();
            if($this->_urlLen > 3){ // over 4
                // specific brands store function call
                $this->_storeSubFunction = $this->_url[3];
                if (in_array($this->_storeSubFunction, $this->storeSubFuncArray)) {
                    $this->{'store'.$this->_storeSubFunction}();
                    $this->_setJSValueFlag = 1;
                } else {
                    echo $this->_storeSubFunction;
                    die;
                    header('location: ' . URL.'brands');
                }
            } else{ // length 3 specific brand main page
                $this->searchArray = array($this->_storeID.'-'.$this->_storeName);
                $this->_setJSValueFlag = 1;
            }
        }
    }
    /**
     *  Start of Store subfunctions descriptions
     */
    public function storeinit(){
        global $PAGETITLES;
        $this->view->topMiddleUse = 1;
        $temp = $this->_url[2];
        $pieces = explode("-", $temp);
        $this->_storeID = Validate::userNameSet($pieces[0]); // only for letter and numbers not sensitive in capital
        $this->_storeName = Validate::fileNameSet($pieces[1]); // include space
        // get the brand information From Database and check the value is properly
        $this->_page = 'storeproduct';
        $this->_loadJS = array('storeproduct');//'storeproduct';
        $this->_loadCSS = array('itemdisplay','product');//'category';
        $this->view->subCategory = '';
        $this->view->subCategory2 = '';
        $this->view->subTitle = $PAGETITLES['brands']['store']['hot']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['store']['hot']['main'];
        $this->view->subTitle2 = $PAGETITLES['brands']['store']['product']['sub'];
        $this->view->mainTitle2 = $PAGETITLES['brands']['store']['product']['main'];
        $categoryArray = $this->model->getBrandCategory($this->_storeID);
        $this->view->categoryArray = $categoryArray;
    }

    public function storecategory(){
        // if sub category is, have to find data and items from database
        $subCategory = Validate::fileNameSet($this->_url[4]);
        $subCategory2 = '';
        if($this->_urlLen > 5){
            $subCategory2 = Validate::fileNameSet($this->_url[5]);
        }
        // need to gethering data from DB
        $this->view->subCategory = $subCategory;
        $this->view->subCategory2 = $subCategory2;
        if($subCategory2 != ''){
            $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction, $subCategory, $subCategory2);
        } else {
            $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction, $subCategory);
        }
    }

    public function storeinfo(){
        global $PAGETITLES;
        $this->_loadCSS = array();
        $this->_page = 'storeinfo';
        $this->view->subTitle = $PAGETITLES['brands']['store']['info']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['store']['info']['main'];
        $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction);
    }

    public function storecontact(){
        global $PAGETITLES;
        $this->_loadCSS = array('briefinfo');//'storecontact';
        $this->_loadJS = array('product', 'storeproduct');//'storecontact';
        $this->_page = 'storecontact';
        $this->view->subTitle = $PAGETITLES['brands']['store']['contact']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['store']['contact']['main'];
        $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction);
    }

    // celebs, followers, events 페이지는 추후에 정확한 피이지 뷰로 변경 필요. --> just use now storeowner
    public function storecelebs(){
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['store']['celebs']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['store']['celebs']['main'];
        $this->_page = 'storeowner';
        $this->_loadJS = array('brands', 'storeproduct');//;brands
        $this->_loadCSS = array('briefinfo', 'product','fancybox');//'storeowner';
        $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction);
    }

    public function storefollowers(){
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['store']['followers']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['store']['followers']['main'];
        $this->_page = 'storeowner';
        $this->_loadJS = array('brands', 'storeproduct');//'storeowner';
        $this->_loadCSS = array('briefinfo', 'product','fancybox');//'storeowner';
        $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction);
    }

    public function storeevent(){
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['brands']['store']['event']['sub'];
        $this->view->mainTitle = $PAGETITLES['brands']['store']['event']['main'];
        $this->_page = 'storeowner';
        $this->_loadJS = array('brands', 'storeproduct');//'storeowner';
        $this->_loadCSS = array('briefinfo', 'product','fancybox');//'storeowner';
        $this->searchArray = array($this->_storeID.'-'.$this->_storeName, $this->_storeSubFunction);
    }

    // javascript variables set
    public function setJava(){
        $a = $this->_storeSubFunction;
        if($this->_storeSubFunction == '' || $this->_storeSubFunction == 'category'){
            $a = 'product';
        }
        echo("<script language=javascript> brandSubMenu(\"$a\"); </script>");
    }
    /**
     * End of Store subfunctions descriptions
     */

    public function setNavi(){
        if($this->subFunction===0){
            $this->naviMenu = array('Home', 'brands');
        } else if($this->subFunction == 'search') {
            $this->naviMenu = array('Home', 'brands', 'search');
            $this->naviMenu = array_merge($this->naviMenu, $this->searchArray);
        } else if($this->subFunction == 'store') {
            $this->naviMenu = array('Home', 'brands', 'store');
            $this->naviMenu = array_merge($this->naviMenu, $this->searchArray);
        } else{
            $this->naviMenu = array('Home', 'brands', $this->subFunction);
            if(!empty($this->searchArray)){
                $this->naviMenu = array_merge($this->naviMenu, $this->searchArray);
            }
        }
        $this->view->naviMenu = $this->naviMenu;
    }

    public function loadCSS(){
        if(!empty($this->_loadCSS)){//$this->_loadCSS != ''){
            $loadCSS = new loadCSS();
            $this->loadCSSFiles = $loadCSS->loadCSSArray($this->_loadCSS);
            //$this->loadCSSFiles = $loadCSS->loadCSS($this->_loadCSS);
        }
    }

    public function loadJS(){
        //if(!empty($this->_loadJS)){//$this->_loadJS != ''){
        if($this->login->userOk==true){
            $mode = 1;
        } else {
            array_push($this->_loadJS, 'signup');
        }
        $loadJS = new loadJS();
        //$this->loadJSFiles = $loadJS->loadJS($this->_loadJS, $mode);
        $this->loadJSFiles = $loadJS->loadJSArray($this->_loadJS);
        //}
    }

    public function viewRender($page){
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->css = $this->loadCSSFiles;

        $this->view->render('include/header');
        $this->view->render('brands/'.$page);
        $this->view->render('include/footer');
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }
}
