

<?php
//include_once FUNC_PATH. "common.php";

class Login extends Controller {

    public $_urlLen;
    public $_url;
    public $_subFunction;

    function __construct(){
        parent::__construct();
    }

    public function index($urlLen, $url){
        $this->_urlLen = $urlLen;
        $this->_url = $url;
        $this->_subFunction = $url[1];
        $this->{$this->_subFunction}();
    }

    public function sessionCheck(){
        Session::start();
    }

    public function login(){

        // AJAX CALLS THIS LOGIN CODE TO EXECUTE
        if(isset($_POST["signup"]) && $_POST['signup'] == 'OK'){
            // GATHER THE POSTED DATA INTO LOCAL VARIABLES AND SANITIZE
            $emailSet = 0;
            $eChk = Validate::emailVal($_POST["e"]);
            if( $eChk != 'Success'){
                $e = Validate::userNameSet($_POST["e"]);
            } else{
                $e = $_POST["e"];//Validate::emailSet($_POST["e"]);
                $emailSet = 1;
            }

            $p = Hash::create(HASH_TYPE, $_POST['p'], HASH_PASSWORD_KEY);
            $salt = getSalt(24);//substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 23);
            $pSession = Hash::create(HASH_TYPE2, $_POST['p'], $salt);
            // GET USER IP ADDRESS
            $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
            // FORM DATA ERROR HANDLING
            if($e == "" || $p == ""){
                header('location: '. URL);
                exit();
            } else {
            // END FORM DATA ERROR HANDLING]
                $row = $this->model->login($e, $emailSet);
                $numRow = $this->model->db->rowNum;
                if($numRow != 1){
                    echo "login_failed";
                    exit();
                }
                $id = $row['id'];
                $username = $row['username'];
                $pass = $row['password'];

                if($p != $pass){
                    echo "login_failed";
                    exit();
                } else {
                    // CREATE THEIR SESSIONS AND COOKIES
                    Session::start();
                    Session::set('userid', $id);
                    Session::set('username', $username);
                    Session::set('password', $pSession);
                    //Session::set('password', $pass);
                    Cookie::set('id', $id, strtotime( '+1 days' ));
                    Cookie::set('user', $username, strtotime( '+1 days' ));
                    Cookie::set('pass', $pSession, strtotime( '+1 days' ));

                    if(isset($_POST['r']) && $_POST['r']==1){
                        Cookie::set('rem', '1', strtotime( '+7 days' ));
                        Cookie::set('remin', $e, strtotime( '+7 days' ));
                    } else {
                        Cookie::destroy('rem');
                        Cookie::destroy('remin');
                    }

                    // Password is correct but Activation check need
                    /*if($row['activated'] != 1){
                        echo "not_active|$id";
                        exit();
                    }*/

                    // user option set to security password, this is for session and cookie password
                    $this->model->setSessionPass($id, $salt, $pSession);

                    // UPDATE THEIR "IP" AND "LASTLOGIN" FIELDS
                    $this->model->updateLoginInfo($ip, $username);

                    echo $id;
                    //echo $username|$id;
                    exit();
                }
            }
            exit();
        }
    }

}


