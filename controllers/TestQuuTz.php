<?php

class TestQuuTz extends Controller{

    function __construct(){
        parent::__construct();
    }

    function index(){
        $this->init();
        //load CSS
        $this->loadCSS();
        //load JS
        $this->loadJS();

        $this->setNavi();

        // set the test function
        $this->testPagination();

        // Viewer install
        $this->viewRender();
    }
    public function testPagination(){
        $this->view->pagination = makePaginationHtml(8, 1);
        echo  $this->view->pagination; die;
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }

    public function loadCSS(){
        $cssArray = array();
        //$cssArray = array('TestIsotopeFilterButton');
        //$cssArray = array('TestIsotopeFilterSelect');
        //$cssArray = array('TestIsotopeFilterRadio');
        //$cssArray = array('TestIsotopeFilterCombination');
        //$cssArray = array('TestIsotopeSorting');
        //$cssArray = array('TestIsotopeLayout');
        //$cssArray = array('TestIsotopeLayoutVerticalTable');
        //$cssArray = array('TestIsotopeAppended');
        //$cssArray = array('TestIsotopeInsert');
        //$cssArray = array('TestIsotopeLayoutToggle');
        //$cssArray = array('TestIsotopePrepended');
        //$cssArray = array('TestIsotopeRemove');
        //$cssArray = array('TestIsotopeShuffle');
        //$cssArray = array('TestIsotopeStamp');
        //$cssArray = array('TestIsotopeEventLayoutComplete');
        $loadCSS = new loadCSS();
        $this->loadCSSFiles = $loadCSS->loadCSSArray($cssArray);
    }

    public function loadJS(){
        $jsArray = array();
        //$jsArray = array('TestIsotopeFilterButton');
        //$jsArray = array('TestIsotopeFilterSelect');
        //$jsArray = array('TestIsotopeFilterRadio');
        //$jsArray = array('TestIsotopeFilterCombination');
        //$jsArray = array('TestIsotopeSorting');
        //$jsArray = array('TestIsotopeLayout','isotope-cell-by-row');
        //$jsArray = array('TestIsotopeLayoutVerticalTable');
        //$jsArray = array('TestIsotopeAppended');
        //$jsArray = array('TestIsotopeInsert');
        //$jsArray = array('TestIsotopeLayoutToggle');
        //$jsArray = array('TestIsotopePrepended');
        //$jsArray = array('TestIsotopeRemove');
        //$jsArray = array('TestIsotopeShuffle');
        //$jsArray = array('TestIsotopeStamp');
        //$jsArray = array('TestIsotopeEventLayoutComplete');
        $loadJS = new loadJS();
        $this->loadJSFiles = $loadJS->loadJSArray($jsArray);
        //$this->loadJSFiles = $loadJS->loadJSArray(array(), $jsArray);
    }

    public function viewRender(){
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->css = $this->loadCSSFiles;
        $this->view->render('include/header');
        //$this->view->render('TestQuuTz/isotope/filterButton');
        //$this->view->render('TestQuuTz/isotope/filterSelect');
        //$this->view->render('TestQuuTz/isotope/filterRadio');
        //$this->view->render('TestQuuTz/isotope/filterCombination');
        //$this->view->render('TestQuuTz/isotope/sorting');
        //$this->view->render('TestQuuTz/isotope/layout');
        //$this->view->render('TestQuuTz/isotope/layoutVerticalTable');
        //$this->view->render('TestQuuTz/isotope/appended');
        //$this->view->render('TestQuuTz/isotope/insert');
        //$this->view->render('TestQuuTz/isotope/layoutToggle');
        //$this->view->render('TestQuuTz/isotope/prepended');
        //$this->view->render('TestQuuTz/isotope/remove');
        //$this->view->render('TestQuuTz/isotope/shuffle');
        //$this->view->render('TestQuuTz/isotope/stamp');
        //$this->view->render('TestQuuTz/isotope/eventLayoutComplete');
        $this->view->render('TestQuuTz/pagination/pagination');
        $this->view->render('include/footer');

    }

    public function setNavi(){

        $this->view->naviMenu =  array('Home');
    }

}
