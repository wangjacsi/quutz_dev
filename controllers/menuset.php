<?php

class menuset extends Controller {
    public $returnData;

    public $exceptLevel1;

    function __construct(){
        parent::__construct();
        $this->returnData = array();
        $this->exceptArray = array('my'=>array('feed'=>1));
    }

    /**
     * $urlLen : URL length
     * $url : input url path array
     */
    public function index($urlLen, $url){
        if($urlLen < 2){
            header('location: '. URL.'error' );
            exit();
        }
        if(!method_exists($this, $url[1])){
            header('location: '. URL.'error' );
        } else {
            $this->{$url[1]}();
        }
    }

    public function getmenu(){
        $level =0;
        $value = '';
        if(isset($_POST["level"]) && $_POST["level"]!= '' && isset($_POST["data"]) && $_POST["data"]!= ''){
            $level = $_POST["level"];
            $value = $_POST["data"];
            global $HOMEMENU;

            if(is_numeric($level)){
                // check the exception
                if($level == 0){ //main menu set
                    if($this->level0GetMenu($value[$level]))
                        echo json_encode($this->returnData);
                } else if($level==1) {
                    if($this->level0GetMenu($value[0])){
                        if($this->level1GetMenu($value[0], $value[1])){
                            echo json_encode($this->returnData);
                        }
                    }
                } else if($level==2){
                    if($this->level0GetMenu($value[0])){
                        if($value[0] == 'brands' && $value[1]=='store'){
                            echo json_encode($this->returnData);
                        } else{
                            if($this->level1GetMenu($value[0], $value[1])){
                                if($value[0] == 'brands' && $value[1]=='search' && $value[2]!=''){
                                    $this->searchBrands($value[2]);
                                    echo json_encode($this->returnData);
                                } else{
                                    if($value[0] == 'brands' && $value[1]=='favorite'){
                                        echo json_encode($this->returnData);
                                    }else if($this->level2GetMenu($value[0], $value[1], $value[2])){
                                        echo json_encode($this->returnData);
                                    }
                                }
                            }
                        }
                    }
                } else if($level==3 || $level==4 || $level==5){
                    if($this->level0GetMenu($value[0])){
                        if($value[0] == 'brands' && $value[1]=='store'){
                            echo json_encode($this->returnData);
                        } else{
                            if($this->level1GetMenu($value[0], $value[1])){
                                if($value[0] == 'brands' && $value[1]=='search' && $value[2]!=''){
                                    $this->searchBrands($value[2]);
                                    $this->lastGetMenu($value[3]);
                                    echo json_encode($this->returnData);
                                } else{
                                    if($this->level2GetMenu($value[0], $value[1], $value[2])){
                                        $this->lastGetMenu($value[3]);
                                        echo json_encode($this->returnData);
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                header('location: '. URL.'error' );
            }
        } else {
            header('location: '. URL.'error' );
        }
    }

    public function exceptionChk($level, $value){
        if($level == 0){
            if(array_key_exists($value[0], $this->exceptArray)){
                return 1;
            } else {
                return 0;
            }
        } else if($level == 1){
            if(array_key_exists($value[1], $this->exceptArray[$value[0]])){
                return 1;
            } else {
                return 0;
            }
        } else if($level == 2){
            if(array_key_exists($value[2], $this->exceptArray[$value[0]][$value[1]])){
                return 1;
            } else {
                return 0;
            }
        } else if($level == 3){
            if(array_key_exists($value[3], $this->exceptArray[$value[0]][$value[1]][$value[2]])){
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function searchBrands($key){
        $searchKey = Validate::wordSet($key);
        $searchArray = $this->model->searchBrands($searchKey);
        $this->returnData = array_merge($this->returnData, array($searchKey=>$searchArray));
    }

    public function lastGetMenu($value){
        $this->returnData = array_merge($this->returnData, array($value=>''));
    }

    public function level0GetMenu($value){
        global $HOMEMENU;
        if(array_key_exists($value, $HOMEMENU)){
            $data = array_keys($HOMEMENU[$value]);
            $this->returnData = array($value=>$data);
            return 1;
        } else {
            echo 'Access fail';
        }
    }

    public function level1GetMenu($value0, $value1){
        global $HOMEMENU;
        if(array_key_exists($value1, $HOMEMENU[$value0])){
            if(is_array($HOMEMENU[$value0][$value1])){
                $data = array_keys($HOMEMENU[$value0][$value1]);
                $this->returnData = array_merge($this->returnData, array($value1=>$data));
            }
            return 1;
        } else {
            if($value1 == 'store'){ // exception execution
                return 0;
            } else {
                echo 'Access fail';
            }
        }
    }

    public function level2GetMenu($value0, $value1, $value2){
        global $HOMEMENU;
        if(array_key_exists($value2, $HOMEMENU[$value0][$value1])){
            //$data = array_keys($HOMEMENU[$value0][$value1][$value2]);
            $data = ($HOMEMENU[$value0][$value1][$value2]);
            $this->returnData = array_merge($this->returnData, array($value2=>$data));
            return 1;
        } else {
            echo 'Access fail';
        }
    }


}


