

<?php

class Signup extends Controller {

    public $subFunction;
    public $subFuncArray;

    function __construct(){
        parent::__construct();
        $this->subFuncArray = array('userNameCheck','signup');
    }

    public function index($urlLen, $url){
        if($urlLen > 2){
            header('location: '. URL.'error' );
            exit();
        } else {
            $this->subFunction = $url[1];
            if (in_array($this->subFunction, $this->subFuncArray)) {
                $this->{$this->subFunction}();
            } else {
                header('location: '. URL.'error' );
                exit();
            }
        }
        /*$DynamicMenu = new DynamicMenu($this->model->db);
        $DynamicMenu->setMenu();
        $this->view->DMenu = $DynamicMenu;
        $this->view->countryList = Country::fetch_html2();*/
    }

    /*public function sessionCheck(){
        Session::start();
        // if user is logged in, header them away
        if(isset($_SESSION['username'])){
            header('location: '. URL .'log/message?msg=session check error&code=0x0020&file=signup');
            exit();
        }
    }*/

    public function userNameCheck() {
        // Ajax calls this NAME CHECK code to execute
        if(isset($_POST["usernamecheck"])){
            //include_once("php_includes/db_conx.php");
            //$username = preg_replace('#[^a-z0-9]#i', '', $_POST['usernamecheck']);
            $username = Validate::inputVal($_POST['usernamecheck']);
            $uChk = Validate::userNameVal($username);
            if( $uChk != 'Success'){
                echo "$uChk";
                exit();
            }
            $unameCheck = $this->model->userNameCheck($username);

            if (strlen($username) < MIN_USERNAME || strlen($username) > MAX_USERNAME) {
                echo MIN_USERNAME .' - '.MAX_USERNAME.' characters please';
                exit();
            }
            if (is_numeric($username[0])) {
                echo 'Usernames must begin with a letter';
                exit();
            }
            if ($unameCheck < 1) {
                echo 'You can use this name.';
                exit();
            } else {
                echo $username . ' is taken';
                exit();
            }
        }
    }

    public function signup(){
        if(isset($_POST["u"])){

            // Validation check
            //initialize
            $u =''; $e =''; $p =''; $ip=''; $cap ='';
            Session::start();
            // capture character check
            $capSs = Session::fetch('captcha');
            $cap = Validate::userNameSet($_POST['cap']);
            // username check
            $uChk = Validate::userNameVal($_POST['u']);
            if( $uChk != 'Success'){
                echo "$uChk";
                exit();
            }
            $u = $_POST['u'];
            // Email check
            $eChk = Validate::emailVal($_POST['e']);
            if( $eChk != 'Success'){
                echo "$eChk";
                exit();
            }
            $e = $_POST['e'];
            //password check
            $p = $_POST['p'];
            $pChk = Validate::passVal($p);
            if( $pChk != 'Success'){
                echo "$pChk";
                exit();
            }
            // GET USER IP ADDRESS
            // if Server API is ASAPI (IIS), use $_SERVER["REMOTE_ADDR"]
            $ip =getenv('REMOTE_ADDR');
            $ipChk = Validate::ipAddrVal($ip);
            if( $ipChk != 'Success'){
                echo "$ipChk";
                exit();
            }
            /**
             * This is optional part of user profile
            */
            // about
            /*$about = '';
            if(isset($_POST['about']) && $_POST['about']!=""){
                $about = Validate::inputVal2($_POST['about']);
            }
           // first name
            $first = '';
            if(isset($_POST['first']) && $_POST['first']!=""){
                $first = Validate::realNameSet($_POST['first']);
            }
            //last name
            $last = '';
            if(isset($_POST['last']) && $_POST['last']!=""){
                $last = Validate::realNameSet($_POST['last']);
            }
            //birthday
            $birthday = '';
            $birthday = array('day' => $_POST['day'],
                          'month' => $_POST['month'],
                          'year' => $_POST['year']);
            $dateChk = Validate::dateVal($birthday);
            if($dateChk != 'Success'){
                echo $dateChk;
                exit();
            } else {
                $birthday = dateFormat($birthday);
            }
            //gender check
            $gender = 'n';
            if(isset($_POST['g']) && $_POST['g']!=""){
                $gender = Validate::wordSet($_POST['g']);
            }
            // website check
            $website = '';
            if(isset($_POST['web']) && $_POST['web']!=""){
                $urlChk = Validate::urlVal($_POST['web']);
                if($urlChk == 'Success'){
                    $website = $_POST['web'];
                } else {
                    echo $urlChk;
                    exit();
                }
            }
            //SNS check we need to know How SNS id is combined with variables
            //It means that character, decimal numeric number or any others?
            // Facebook ID
            $sns = '';
            if(isset($_POST['sns']) && $_POST['sns']!=""){
                $sns = Validate::inputVal($_POST['sns']);
            }
            //prule check
            $prule = 0;
            if(isset($_POST['prule']) && $_POST['prule']!=""){
                $prule = Validate::userIDSet($_POST['prule']);
            }
            //trule check
            $trule = 0;
            if(isset($_POST['trule']) && $_POST['trule']!=""){
                $trule = Validate::userIDSet($_POST['trule']);
            }
            //country check
            $country = '';
            if(isset($_POST['c']) && $_POST['c']!=""){
                $country = Validate::wordSet($_POST['c']);
            }
            //add1 check
            $add1 = '';
            if(isset($_POST['add1']) && $_POST['add1']!=""){
                $add1 = Validate::inputVal($_POST['add1']);
            }
            //add2 check
            $add2 = '';
            if(isset($_POST['add2']) && $_POST['add2']!=""){
                $add2 = Validate::inputVal($_POST['add2']);
            }
            //city check
            $city = '';
            if(isset($_POST['city']) && $_POST['city']!=""){
                $city = Validate::inputVal($_POST['city']);
            }
            //state check
            $state = '';
            if(isset($_POST['state']) && $_POST['state']!=""){
                $state = Validate::inputVal($_POST['state']);
            }
            //zip check
            $zip = '';
            if(isset($_POST['zip']) && $_POST['zip']!=""){
                $zip = Validate::inputVal($_POST['zip']);
            }
            */
            //test here

            // DUPLICATE DATA CHECKS FOR USERNAME AND EMAIL
            $unameChk = $this->model->userNameCheck($u);
            // -------------------------------------------
            $emailChk = $this->model->emailCheck($e);

            // FORM DATA ERROR HANDLING
            if($u == "" || $e == "" || $p == "" || $cap == "" ){
                echo "The form submission is missing values.";
                exit();
            } else if ($unameChk > 0){
                echo "The username you entered is alreay taken";
                exit();
            } /*else if ($emailChk > 0){
                echo "That email address is already in use in the system";
                exit();
            } */else if (strlen($u) < MIN_USERNAME || strlen($u) > MAX_USERNAME) {
                echo "Username must be between " .MIN_USERNAME." and ".MAX_USERNAME." characters";
                exit();
            } else if ($capSs != $cap) {
                echo 'Capture characters are different!';
                exit();
            } /*else if (is_numeric($u[0])) {
                echo 'Username cannot begin with a number';
                exit();
            } */else {
                // END FORM DATA ERROR HANDLING
                // Begin Insertion of data into the database
                // Hash the password and apply your own mysterious unique salt
                // 74 characters
                /*$cryptpass = crypt($p);
                include_once (FUNC_PATH . "randStrGen.php");
                $p_hash = randStrGen(20)."$cryptpass".randStrGen(20);   */
                //$p_hash = md5($p);
                $p_hash = Hash::create(HASH_TYPE, $p, HASH_PASSWORD_KEY);
                $salt = getSalt(24);//substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 23);
                $pSession = Hash::create(HASH_TYPE2, $p, $salt);

                //copy avatar -> store avatar path
                global $DEFAULT_CONF;
                $avatar = $DEFAULT_CONF['AVATAR'];
                $cover = $DEFAULT_CONF['COVER'];
                // Add user info into the database table for the main site table
                $tempT = date('Y-m-d H:i:s');
                $tempStatus = $u.'\'s timeline!';
                //timezone timeoffset
                Session::start();
                if(isset($_SESSION['timeoffset'])){
                    $toffset = $_SESSION['timeoffset'];
                }
                $data = array(
                    'username' => $u, 'email' => $e,
                    'password' => $p_hash, //'gender' => $gender,
                    //'website' => $website, 'country' => $country,
                    'avatar' => $avatar, 'cover' => $cover,'ip' => $ip,
                    //'birthday' => $birthday, 'aboutme' => $about,
                    //'sns' => $sns, 'rprofile' => $prule,
                    //'rtimeline' => $trule,
                    'signup' => $tempT, 'lastlogin' => $tempT,
                    'notescheck' => $tempT, 'activated' => 1, 'toffset' => $toffset);
                    //'coverstatus' => $tempStatus,
                    //'visittime' => $tempT);
                $uid = $this->model->userAdd($data);

                //error check
                if($uid==''){
                    echo $uid .' User registration error occur! Code[01]';
                    exit();
                }
                // Establish their row in the useroptions table
                $data = array(
                    'id' => $uid,
                    'username' => $u,
                    'background' => 'original');
                $uoption = $this->model->userOptionAdd($data);
                //error check
                if($uoption != 1){
                    echo ' User registration error occur! Code[02]';
                    exit();
                }

                //user profile insert to DB
                /*$data = array(
                    'userid' => $uid,
                    'firstname' => $first,
                    'lastname' => $last,
                    'address1' => $add1,
                    'address2' => $add2,
                    'state' => $state,
                    'city' => $city,
                    'zip' => $zip
                    );
                $uoption = $this->model->userProfileAdd($data);
                //error check
                if($uoption != 1){
                    echo ' User profile insertion error occur!';
                    exit();
                }

                //user's default likes table
                $data = array('userid' => $userid);
                $ulikes = $this->model->userLikes($data);
                //error check
                if($ulikes != 1){
                    echo ' User likes table error occur!';
                    exit();
                }*/


                // Create directory(folder) to hold each user's files(pics, MP3s, etc.)
                if (!file_exists(LOCAL_USERS_PATH. "/$u")) {
                    //print_r(USERS_PATH. "/$u");
                    mkdir(LOCAL_USERS_PATH. "/$u", 0755);
                }
                //copy avatar -> store avatar path
                /*$avatar = ROOT.'/'.PRJ_NAME.'/'.IMAGE_PATH.'/'.DEFAULT_AVATAR;
                $avatar2 = LOCAL_USERS_PATH."/$u/".DEFAULT_AVATAR;
                if(!copy($avatar, $avatar2)){
                    echo "Failed to create avatar";
                }*/
                // New Pass for Activation
                $sec = new Security();
                $newPass = $sec->systemKeygen(ACTIVATE_KEY);

                // Email the user their activation link
                $email = new Emailset();
                $email->setTo($e, $u);
                $email->setSubject(OUR_NAME . " your Account Activation");
                $emailStr = file_get_contents(MAIL_CONTENTS . 'Activation.html');
                $postStr = "id=$uid&u=$u&e=$e&p=$newPass";
                $repStr = array($u, $e, $postStr);
                $searchStr = array("_userName_", "_emailAddr_", "_postVal_");
                //replace string
                $emailStr = str_replace($searchStr, $repStr, $emailStr);
                $email->setMessage($emailStr);
                $email->sendMail('off');

                //Session update for Avatar Image insertion process
                //Session::set('SIGNUPOK', 1);
                Session::set('username', $u);
                Session::set('userid',$uid);
                Session::set('password', $pSession);

                Cookie::set('id', $uid, strtotime( '+1 days' ));
                Cookie::set('user', $u, strtotime( '+1 days' ));
                Cookie::set('pass', $pSession, strtotime( '+1 days' ));

                // user option set to security password, this is for session and cookie password
                $this->model->setSessionPass($uid, $salt, $pSession);

                //echo return value include lots of white space why??
                echo "signup_success";
                exit();
            }
            exit();
        }
    }

}


