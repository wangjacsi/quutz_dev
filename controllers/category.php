<?php
class category extends Controller{
    public $login;
    public $loadJSFiles;
    public $loadCSSFiles;
    public $catNameForPage;
    public $catNameForPageDisp;
    public $subCatNameForPage;
    public $subCatNameForPageDisp;
    public $naviMenu;

    // Logic Variables here
    public $popularBrandItems;
    public $popularBrandInfo;
    public $hottestItems;
    public $newItems;
    public $celebItems;
    public $dispBrandItems;
    public $dispCelebReviews;
    public $totalNumItems;
    public $pageHtml;

    // sub function
    public $subFunction;

    function __construct(){
        parent::__construct();
        $this->catNameForPage = '';
        $this->catNameForPageDisp = '';
        $this->subCatNameForPage = '';
        $this->subCatNameForPageDisp = '';

        // logic variables
        $this->popularBrandItems = array();
        $this->popularBrandInfo = array();
        $this->hottestItems = array();
        $this->newItems = array();
        $this->celebItems = array();
        $this->dispBrandItems = array();
        $this->dispCelebReviews = array();
        $this->totalNumItems = 0;
        $this->pageHtml = '';

        // sub function
        $this->subFunction = 0;
        $this->subFuncArray = array('getMoreItems');
    }
    /**
     * $urlLen : URL length
     * $url : input url path array
     */
    function index($urlLen, $url){
        if($urlLen < 2){
            header('location: '. URL );
            exit();
        } else if($urlLen == 3){
            // category name set and check the category url name is correct or not
            $this->catNameForPage = $url[1];
            $this->subCatNameForPage = $url[2];
            $this->chkCatName(2);
        } else if($urlLen > 3){
            header('location: '. URL.'error' );
            exit();
        } else { //length 2, category, women
            // category name set and check the category url name is correct or not
            $this->catNameForPage = $url[1];
            $this->subFunction = $url[1];
            $this->chkCatName(1);
        }
        //initialization : user security check
        $this->init();
        // set the navigation
        $this->setNavi();
        //load JS
        $this->loadCSS();
        //load JS
        $this->loadJS();
        // set the variables for each category
        $this->setCategoryVar();
        // Find a Display items from DB
        $this->getPopularBrandItems();
        $this->getHottestItems();
        $this->getNewItems();
        $this->getCelebItems();
        // display products --> need to change category finding
        $this->findTotalNumOfItems();
        $this->setPagination();
        $this->getDisplayItems(0, 0, 0);
        //$this->getDisplayReviews();
        // Viewer install
        $this->viewRender();
    }

    public function setPagination(){
        $last = pagination($this->totalNumItems);
        $this->pageHtml = makePaginationHtml($last, 1);
        $this->view->pageHtml = $this->pageHtml;
    }

    public function findTotalNumOfItems(){
        $this->totalNumItems = $this->model->findTotalNumOfItems();
    }

    public function getMoreItems(){
        // DB 에서 읽어오도록 수정 필요
        if(isset($_POST["pagenum"])){
            //echo '<pre>';
            //print_r($_POST);
            //die;
            $pageNum = Validate::userIDSet($_POST["pagenum"]);
            $sortType = ($_POST["sort"]);//Validate::userNameSet($_POST["sort"]);
            $filterType = Validate::userNameSet($_POST["filter"]);
            //echo $sortType;
            //die;
            if($pageNum != ''){
                $result = $this->getDisplayItems($pageNum, $sortType, $filterType);
                if($result == 1){
                    echo json_encode($this->view->dispBrandItems);
                } else if($result == 2){ // no more data from DB
                    echo 'nodata';
                } else { // 0 error for search database
                    echo 'errordb';
                }
            }
        } else {
            exit();
        }
        exit();
    }

    public function getDisplayItems($pageNum, $sortType, $filterType){
        if($this->model->getDisplayItems($pageNum, $sortType, $filterType)){
            $this->dispBrandItems = $this->model->exc->fetchAll(FETCH_MODE);
            if(empty($this->dispBrandItems)){
                return 2;
            } else{
                foreach($this->dispBrandItems as $key => $value){
                    $brandLogo = photoNameSet('avatar_s1', $value['brandLogo']);
                    $this->dispBrandItems[$key]['brandLogo'] = $brandLogo;
                    $setName = photoNameSet('prod_s1', $value['prodPhotoName']);
                    $this->dispBrandItems[$key]['prodPhotoName'] = $setName;
                }
                $this->view->dispBrandItems = $this->dispBrandItems;
                return 1;
            }
        } else {
            return 0;
        }
    }

    public function getDisplayReviews(){
        if($this->model->getDisplayReviews()){
            $this->dispCelebReviews = $this->model->exc->fetchAll(FETCH_MODE);
            //shuffle($this->dispCelebReviews);
            //$this->dispCelebReviews = array_slice($this->dispCelebReviews, 0, 4);
            foreach($this->dispCelebReviews as $key => $value){
                $reviewPhoto = celebPhotoNameSet('review_s1', $value['reviewPhotoName'], $value['celebID'], $value['prodID']);
                $this->dispCelebReviews[$key]['reviewPhotoName'] = $reviewPhoto;
                $celebPhoto = photoNameSet('avatar_s1', $value['celebPhotoName']);
                $this->dispCelebReviews[$key]['celebPhotoName'] = $celebPhoto;
            }
            $this->view->dispCelebReviews = $this->dispCelebReviews;
        }
    }

    public function getCelebItems(){
        if($this->model->getCelebItems()){
            $this->celebItems = $this->model->exc->fetchAll(FETCH_MODE);
            shuffle($this->celebItems);
            $this->celebItems = array_slice($this->celebItems, 0, 4);
            foreach($this->celebItems as $key => $value){
                $reviewPhoto = celebPhotoNameSet('review_s1', $value['reviewPhotoName'], $value['celebID'], $value['prodID']);
                $this->celebItems[$key]['reviewPhotoName'] = $reviewPhoto;
                $celebPhoto = photoNameSet('avatar_s1', $value['celebPhotoName']);
                $this->celebItems[$key]['celebPhotoName'] = $celebPhoto;
            }
            $this->view->celebItems = $this->celebItems;
        }
    }

    public function getHottestItems(){
        if($this->model->getHottestItems()){
            $this->hottestItems = $this->model->exc->fetchAll(FETCH_MODE);
            shuffle($this->hottestItems);
            $this->hottestItems = array_slice($this->hottestItems, 0, 4);
            foreach ($this->hottestItems as $key => $value){
                $brandLogo = photoNameSet('avatar_s1', $value['brandLogo']);
                $this->hottestItems[$key]['brandLogo'] = $brandLogo;
                $setName = photoNameSet('prod_s1', $value['prodPhotoName']);
                $this->hottestItems[$key]['prodPhotoName'] = $setName;
            }
        }
        $this->view->hottestItems = $this->hottestItems;
    }

    public function getNewItems(){
        if($this->model->getNewItems()){
            $this->newItems = $this->model->exc->fetchAll(FETCH_MODE);
            shuffle($this->newItems);
            $this->newItems = array_slice($this->newItems, 0 , 4);
            foreach($this->newItems as $key => $value){
                $brandLogo = photoNameSet('avatar_s1', $value['brandLogo']);
                $this->newItems[$key]['brandLogo'] = $brandLogo;
                $setName = photoNameSet('prod_s1', $value['prodPhotoName']);
                $this->newItems[$key]['prodPhotoName'] = $setName;
            }
        }
        $this->view->newItems = $this->newItems;
    }

    // Need to change :if category send here to change the category's items
    // find popular brands from their number of likes
    public function getPopularBrandItems(){
        if($this->model->getPopularBrands()){
            $this->popularBrandInfo = $this->model->exc->fetchAll(FETCH_MODE);
            shuffle($this->popularBrandInfo);
            $this->popularBrandInfo = array_slice($this->popularBrandInfo, 0, 4);
            $this->popularBrandItems = array();
            foreach ($this->popularBrandInfo as $key => $value){
                $brandLogo = photoNameSet('avatar_s1', $value['brandLogo']);
                $this->popularBrandInfo[$key]['brandLogo'] = $brandLogo;
                if($this->model->getPopularBrandsItems($value['brandID'])){
                    $tempBrandItems = $this->model->exc->fetchAll(FETCH_MODE);
                    foreach($tempBrandItems as $key2 => $value2){
                        $setName = photoNameSet('prod_s2', $value2['prodPhotoName']);
                        $tempBrandItems[$key2]['prodPhotoName'] = $setName;
                    }
                    array_push($this->popularBrandItems, $tempBrandItems);
                }
            }
        }
        $this->view->popularBrandItems = $this->popularBrandItems;
        $this->view->popularBrandInfo = $this->popularBrandInfo;
    }

    public function setNavi(){
        if($this->subCatNameForPage!=''){
            $this->naviMenu = array('Home', 'category', $this->catNameForPage, $this->subCatNameForPage);
        } else {
            $this->naviMenu = array('Home', 'category', $this->catNameForPage);
        }
        $this->view->naviMenu = $this->naviMenu;
    }

    public function chkCatName($level){
        global $HOMEMENU;
        if(array_key_exists($this->catNameForPage, $HOMEMENU['category'])){
            $tempMenu = $this->catNameForPage;
            $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
            $tempMenu = preg_replace('/_/', ' ', $tempMenu);
            $this->catNameForPageDisp = $tempMenu;
            if($level==2){
                if(array_key_exists($this->subCatNameForPage, $HOMEMENU['category'][$this->catNameForPage])){
                    $tempMenu = $this->subCatNameForPage;
                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                    $this->subCatNameForPageDisp = $tempMenu;
                }
            }
        } else {
            // check for the Subfunctions
            if (in_array($this->subFunction, $this->subFuncArray)) {
                // specific function call
                $this->{$this->subFunction}();
                exit();
            } else{
                header('location: '. URL.'error' );
                exit();
            }
        }
    }

    public function setCategoryVar(){
        $this->view->catNameForPage = $this->catNameForPageDisp;
        $this->view->subCatNameForPage = $this->subCatNameForPageDisp;
    }

    public function loadCSS(){
        $cssArray = array('itemdisplay','product');
        $loadCSS = new loadCSS();
        $this->loadCSSFiles = $loadCSS->loadCSSArray($cssArray);
        //$this->loadCSSFiles = $loadCSS->loadCSS('category');
    }

    public function loadJS(){
        $jsArray = array('category');
        if($this->login->userOk==true){
           $mode = 1;
        } else {
            $mode = 0;
            array_push($jsArray, 'signup');
        }
        $loadJS = new loadJS();
        $this->loadJSFiles = $loadJS->loadJSArray($jsArray);
        //$this->loadJSFiles = $loadJS->loadJS('category', $mode);
    }

    public function viewRender(){
        $this->view->page = 'category';
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->css = $this->loadCSSFiles;
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['category']['sub'];
        $this->view->mainTitle = $PAGETITLES['category']['main'];
        $this->view->prodSubTitle = $PAGETITLES['category']['prod_sub'];
        $this->view->prodMainTitle = $PAGETITLES['category']['prod_main'];

        $this->view->render('include/header');
        $this->view->render('category/index');
        $this->view->render('include/footer');
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }
}
