<?php

class Index extends Controller{
    public $login;
    public $loadJSFiles;
    public $category_array;
    public $naviMenu;

    function __construct(){
        parent::__construct();
    }

    function index(){
        //initialization : user security check
        $this->init();
        // set the navigation
        $this->setNavi();
        //load JS
        $this->loadJS();
        // Viewer install
        $this->viewRender();
    }

    public function setNavi(){
        $this->naviMenu = array('Home');
    }

    public function loadJS(){
        $jsArray = array();
        if($this->login->userOk==true){
            $mode = 1;
        } else {
            $mode = 0;
            array_push($jsArray, 'signup');
        }
        $loadJS = new loadJS();
        $this->loadJSFiles = $loadJS->loadJSArray($jsArray);
        //$this->loadJSFiles = $loadJS->loadJS('index', $mode);
    }

    public function viewRender(){
        $this->view->page = 'index';
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->naviMenu = $this->naviMenu;
        global $HOMEMENU;
        $this->view->category_array = array();
        $this->view->category_array2 = array();
        foreach ($HOMEMENU['category'] as $key => $menu){
            $tempMenu = $key;
            $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
            $tempMenu = preg_replace('/_/', ' ', $tempMenu);
            array_push($this->view->category_array2, $key);
            array_push($this->view->category_array, $tempMenu);
        }

        $this->view->render('include/header');
        $this->view->render('index/index');
        $this->view->render('include/footer');
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }
}
