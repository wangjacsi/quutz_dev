<?php
class my extends Controller{
    // common variables
    public $login;
    public $loadJSFiles;
    public $loadCSSFiles;
    public $naviMenu;
    public $_urlLen;
    public $_url;
    public $_page;
    public $_loadJS;
    public $_loadCSS;
    //public $_setJSValueFlag;

    // specific variables
    public $subFunction;
    public $subFuncArray;
    public $searchArray;
    public $searchKey;

    function __construct(){
        parent::__construct();
        $this->view->topMiddleUse = 2;
        $this->subFunction = 0;
        $this->subFuncArray = array('feed'=>'feed',
                                    'shops'=>'favorite_shops',
                                    'celebs'=>'favorite_celebs',
                                    'collections'=>'collections',
                                    'wish'=>'wish_list',
                                    'purchase'=>'purchases_and_reviews',
                                    'contact'=>'contact',
                                    'setting'=>'account_settings');
        $this->view->searchKey = '';
        $this->searchKey = '';

        $this->_page = 'my';
        $this->view->mainTitle = '';
        $this->view->subTitle = '';

        $this->searchArray = array();
        // load files initial setting
        $this->_loadJS = array('category','storeproduct');
        $this->_loadCSS = array('itemdisplay','product');
        // set for java variables setting
        $this->_setJSValueFlag = 0;
   }
    /**
     * $urlLen : URL length
     * $url : input url path array
     * URL[0~3]:brands, subfunctions:search-favorite-new-event, A-Z-ETC, brand name
     */
    function index($urlLen, $url){
        $this->_urlLen = $urlLen;
        $this->_url = $url;
        //$this->_setJSValueFlag = 1;
        // if $urlLen = 1 and then just go to brands
        if($urlLen==1){
            //initialization : user security check
            $this->init();
            // set the navigation
            $this->setNavi();
            //load CSS
            $this->loadCSS();
            //load JS
            $this->loadJS();
            // Viewer install
            $this->viewRender('my');
            $this->setJava();
        } else {
            // [1]: feed, shops ...
            $this->subFunction = $url[1];
            if (array_key_exists($this->subFunction, $this->subFuncArray)) {
                //initialization : user security check
                $this->init();
                // specific function call
                $this->{$this->subFunction}();
                // set the navigation
                $this->setNavi();
                //load CSS
                $this->loadCSS();
                //load JS
                $this->loadJS();
                // Viewer install
                $this->viewRender($this->_page);
                //if($this->_setJSValueFlag==1){
                    $this->setJava();
                //}
            } else {
                header('location: '. URL.'error' );
                exit();
            }
        }
    }

    public function feed(){
        $this->_page = 'my';
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['my']['feed']['sub'];
        $this->view->mainTitle = $PAGETITLES['my']['feed']['main'];
    }

    public function shops(){
        $this->_page = 'shops';
        $this->_loadJS = array('brands','storeproduct');
        $this->_loadCSS = array('brands');
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['my']['shops']['sub'];
        $this->view->mainTitle = $PAGETITLES['my']['shops']['main'];
    }

    public function celebs(){
        $this->_page = 'shops';
        $this->_loadJS = array('brands','storeproduct');
        $this->_loadCSS = array('brands');
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['my']['celebs']['sub'];
        $this->view->mainTitle = $PAGETITLES['my']['celebs']['main'];
    }

    public function collections(){
        $this->_page = 'collections';
        $this->_loadJS = array('storeproduct');
        $this->_loadCSS = array('brands');
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['my']['collections']['sub'];
        $this->view->mainTitle = $PAGETITLES['my']['collections']['main'];
    }

    public function wish(){
        $this->_page = 'shops';
        $this->_loadJS = array('brands','storeproduct');
        $this->_loadCSS = array('brands');
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['my']['wish']['sub'];
        $this->view->mainTitle = $PAGETITLES['my']['wish']['main'];
    }

    public function contact(){
        $this->_page = 'contact';
        $this->_loadCSS = array('briefinfo');
        $this->_loadJS = array('product', 'storeproduct');
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['my']['contact']['sub'];
        $this->view->mainTitle = $PAGETITLES['my']['contact']['main'];
    }

    // javascript variables set
    public function setJava(){
        $a = $this->subFunction;
        if($this->subFunction === 0){
            $a = 'feed';
        }
        echo("<script language=javascript> brandSubMenu(\"$a\"); </script>");
    }

    public function setNavi(){
        if($this->subFunction===0){
            $this->naviMenu = array('Home', 'my');
        } else{
            $this->naviMenu = array('Home', 'my', $this->subFuncArray[$this->subFunction]);
            if(!empty($this->searchArray)){
                $this->naviMenu = array_merge($this->naviMenu, $this->searchArray);
            }
        }
        $this->view->naviMenu = $this->naviMenu;
    }

    public function loadCSS(){
        if(!empty($this->_loadCSS)){//$this->_loadCSS != ''){
            $loadCSS = new loadCSS();
            $this->loadCSSFiles = $loadCSS->loadCSSArray($this->_loadCSS);
            //$this->loadCSSFiles = $loadCSS->loadCSS($this->_loadCSS);
        }
    }

    public function loadJS(){
        //if($this->_loadJS != ''){
        if($this->login->userOk==true){
            $mode = 1;
        } else {
            $mode = 0;
            array_push($this->_loadJS, 'signup');
        }
        $loadJS = new loadJS();
        $this->loadJSFiles = $loadJS->loadJSArray($this->_loadJS);
            //$this->loadJSFiles = $loadJS->loadJS($this->_loadJS, $mode);
        //}
    }

    public function viewRender($page){
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->css = $this->loadCSSFiles;

        $this->view->render('include/header');
        $this->view->render('my/'.$page);
        $this->view->render('include/footer');
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }
}
