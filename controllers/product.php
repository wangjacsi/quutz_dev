<?php
class product extends Controller{
    public $login;
    public $loadJSFiles;
    public $loadFootJSFiles;
    public $loadCSSFiles;
    public $naviMenu;

    function __construct(){
        parent::__construct();
        $this->view->topMiddleUse = 1;
    }
    /**
     * $urlLen : URL length
     * $url : input url path array
     */
    function index($urlLen, $url){
        if($urlLen < 2){
            header('location: '. URL.'error' );
            exit();
        }
        // category name set and check the category url name is correct or not
        $this->productID = $url[1];
        //initialization : user security check
        $this->init();
        // set the navigation
        $this->setNavi();
        //load CSS
        $this->loadCSS();
        //load JS
        $this->loadJS();
        // Viewer install
        $this->viewRender();
    }

    public function setNavi(){
        $this->naviMenu = array('Home', 'product');
        $this->view->naviMenu = $this->naviMenu;
    }

    public function loadCSS(){
        $cssArray = array('briefinfo','product','fancybox','montage');
        $loadCSS = new loadCSS();
        $this->loadCSSFiles = $loadCSS->loadCSSArray($cssArray);
        //$this->loadCSSFiles = $loadCSS->loadCSS('product');
    }

    public function loadJS(){
        $jsArray = array('product','fancybox','montage');
        if($this->login->userOk==true){
           $mode = 1;
        } else {
            $mode = 0;
            array_push($jsArray, 'signup');
        }
        $loadJS = new loadJS();
        $this->loadJSFiles = $loadJS->loadJSArray($jsArray);
        //$this->loadJSFiles = $loadJS->loadJS('product', $mode);
        $this->loadFootJSFiles = $loadJS->footJsFiles;
    }

    public function viewRender(){
        $this->view->page = 'product';
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->css = $this->loadCSSFiles;
        $this->view->jsFooter = $this->loadFootJSFiles;

        //global $CATEGORY_MAIN;
        //$this->view->category_array = $CATEGORY_MAIN;

        $this->view->render('include/header');
        $this->view->render('product/index');
        $this->view->render('include/footer');
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }
}
