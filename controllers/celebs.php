<?php
class celebs extends Controller{
    // common variables
    public $login;
    public $loadJSFiles;
    public $loadCSSFiles;
    public $naviMenu;
    public $_urlLen;
    public $_url;
    public $_page;
    public $_loadJS;
    public $_loadCSS;

    // specific variables
    public $subFunction;
    public $subFuncArray;
    public $searchArray;
    public $searchKey;

    function __construct(){
        parent::__construct();
        $this->view->topMiddleUse = 0;
        $this->subFunction = 0;
        $this->subFuncArray = array('collections', 'review_items');
        $this->view->searchKey = '';
        $this->searchKey = '';

        $this->_page = 'collections';
        $this->view->mainTitle = '';
        $this->view->subTitle = '';

        $this->searchArray = array();
        // load files initial setting
        $this->_loadJS = array();//'celebs';
        $this->_loadCSS = array('brands');//'brands';
   }
    /**
     * $urlLen : URL length
     * $url : input url path array
     * URL[0~3]:brands, subfunctions:search-favorite-new-event, A-Z-ETC, brand name
     */
    function index($urlLen, $url){
        $this->_urlLen = $urlLen;
        $this->_url = $url;
        // if $urlLen = 1 and then just go to brands
        if($urlLen==1){
            //initialization : user security check
            $this->init();
            // set the navigation
            $this->setNavi();
            //load CSS
            $this->loadCSS();
            //load JS
            $this->loadJS();
            // Viewer install
            $this->viewRender('collections');
        } else {
            // [1]: collections, review_items
            $this->subFunction = $url[1];
            if (in_array($this->subFunction, $this->subFuncArray)) {
                //initialization : user security check
                $this->init();
                // specific function call
                $this->{$this->subFunction}();
                // set the navigation
                $this->setNavi();
                //load CSS
                $this->loadCSS();
                //load JS
                $this->loadJS();
                // Viewer install
                $this->viewRender($this->_page);
            } else {
                header('location: '. URL.'error' );
                exit();
            }
        }
    }

    public function collections(){
        // need to show the whole categories
        $this->_page = 'collections';
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['celebs']['collections']['sub'];
        $this->view->mainTitle = $PAGETITLES['celebs']['collections']['main'];
        $this->view->subTitle2 = $PAGETITLES['celebs']['collections']['sub2'];
        $this->view->mainTitle2 = $PAGETITLES['celebs']['collections']['main2'];
        if($this->_urlLen > 2){ // if have a category
            $this->searchArray = array($this->_url[2]);
            $this->view->category = preg_replace('/_and_/', ' & ', $this->_url[2]);
            $this->view->category = preg_replace('/_/', ' ', $this->view->category);
        }
    }

    public function review_items(){
        $this->_page = 'review';
        $this->_loadJS = array('category');//'category';
        $this->_loadCSS = array('itemdisplay', 'product');//'category';
        global $PAGETITLES;
        $this->view->subTitle = $PAGETITLES['celebs']['review_items']['sub'];
        $this->view->mainTitle = $PAGETITLES['celebs']['review_items']['main'];
        $this->view->subTitle2 = $PAGETITLES['celebs']['review_items']['sub2'];
        $this->view->mainTitle2 = $PAGETITLES['celebs']['review_items']['main2'];
        if($this->_urlLen > 2){ // if have a category
            $this->searchArray = array($this->_url[2]);
            $this->view->category = preg_replace('/_and_/', ' & ', $this->_url[2]);
            $this->view->category = preg_replace('/_/', ' ', $this->view->category);
        }
    }

    public function setNavi(){
        if($this->subFunction===0){
            $this->naviMenu = array('Home', 'brands');
        } else if($this->subFunction == 'search') {
            $this->naviMenu = array('Home', 'brands', 'search');
            $this->naviMenu = array_merge($this->naviMenu, $this->searchArray);
        } else{
            $this->naviMenu = array('Home', 'celebs', $this->subFunction);
            if(!empty($this->searchArray)){
                $this->naviMenu = array_merge($this->naviMenu, $this->searchArray);
            }
        }
        $this->view->naviMenu = $this->naviMenu;
    }

    public function loadCSS(){
        if(!empty($this->_loadCSS)){//$this->_loadCSS != ''){
            $loadCSS = new loadCSS();
            $this->loadCSSFiles = $loadCSS->loadCSSArray($this->_loadCSS);
            //$this->loadCSSFiles = $loadCSS->loadCSS($this->_loadCSS);
        }
    }

    public function loadJS(){
        //if(!empty($this->_loadJS)){//$this->_loadJS != ''){
        if($this->login->userOk==true){
            $mode = 1;
        } else {
            $mode = 0;
            array_push($this->_loadJS, 'signup');
        }
        $loadJS = new loadJS();
        $this->loadJSFiles = $loadJS->loadJSArray($this->_loadJS);
        //$this->loadJSFiles = $loadJS->loadJS($this->_loadJS, $mode);
        //}
    }

    public function viewRender($page){
        $this->view->login = $this->login;
        $this->view->js = $this->loadJSFiles;
        $this->view->css = $this->loadCSSFiles;

        $this->view->render('include/header');
        $this->view->render('celebs/'.$page);
        $this->view->render('include/footer');
    }

    public function init(){
        // Session and Cookie check and update login info -> lastlogin time set
        $this->login = new loginStatus($this->model->db);
        $this->login->statusCheck();
    }
}
