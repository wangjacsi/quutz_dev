<?php
/**
 * Add or Cahnges
 * /Date/What/HOW/WHY
 * 2013-12-25/Users Table Add/
 *  - Add: firstname, lastname, address1, address2, state, city, 
 *    zip, phone, youtube, shopname, accounttype, accountnotes, aboutme
 *  - users table need more informations
 * 
 * 
 */

// Tips
// Need to Change Engine -> ALTER TABLE  `pm` ENGINE = MYISAM ;


include_once "../config/paths.php";
require "../config/database.php";
require "../libs/db/database.php";

$db_conx = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

$tbl_blog_topics = "CREATE TABLE IF NOT EXISTS blog_topics (
                                id INT(11) NOT NULL AUTO_INCREMENT,
                                name VARCHAR(100) NOT NULL,                               
                                PRIMARY KEY (id)                                
                         )";
                         
//method 1 : mysqli                      
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_blog_topics); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
        echo "<h3>blog_topics table NOT created :( </h3>";     
} 
else {
        echo "<h3>blog_topics table created OK :) </h3>";
}
$tbl_blog_topics = "ALTER TABLE  `blog_topics` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_blog_topics); 


$tbl_blog_comments = "CREATE TABLE IF NOT EXISTS blog_comments (
				id INT(11) NOT NULL AUTO_INCREMENT,
				blog_id INT(11) NOT NULL,
				poster_id INT(11) NOT NULL,
				poster_name VARCHAR(100) NOT NULL,
				comment VARCHAR(500) NOT NULL,	
                datetime DATETIME NOT NULL,
                divstart VARCHAR(500) NOT NULL,			
				PRIMARY KEY (id)
			 )";
			 
//method 1 : mysqli			 
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_blog_comments); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>blog_comments table NOT created :( </h3>";	
} 
else {
	echo "<h3>blog_comments table created OK :) </h3>";
}
$tbl_blog_comments = "ALTER TABLE  `blog_comments` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_blog_comments); 



////////////////////////////////////
$tbl_blog = "CREATE TABLE IF NOT EXISTS blog ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                blog_cat VARCHAR(100) NOT NULL,
				blog_title VARCHAR(100) NOT NULL,
                blog_body TEXT NOT NULL,
                authid INT(11) NOT NULL,
                authname VARCHAR(20) NOT NULL,
                datetime DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
                likes INT(11) NOT NULL DEFAULT '0',
                likers TEXT NULL,
                views INT(11) NOT NULL DEFAULT '0',
                favorited_by TEXT NULL,
                fav_count INT(11) NOT NULL DEFAULT '0', 
                PRIMARY KEY (id) 
                )"; 

//method 1 : mysqli			 
//$query = mysqli_query($db_conx, $tbl_useroptions); 

//method 2 : PDO statment
$query = $db_conx->exec($tbl_blog); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>blog table NOT created :( </h3>";	
} 
else {
	echo "<h3>blog table created OK :) </h3>";
}
$tbl_blog = "ALTER TABLE  `blog` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_blog); 


