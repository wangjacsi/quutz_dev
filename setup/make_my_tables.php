<?php
/**
 * Add or Cahnges
 * /Date/What/HOW/WHY
 * 2013-12-25/Users Table Add/
 *  - Add: firstname, lastname, address1, address2, state, city,
 *    zip, phone, youtube, shopname, accounttype, accountnotes, aboutme
 *  - users table need more informations
 *
 * 2013-12-26/Private Message Table Add/
 *  - Add: Create new PM table
 *  - Private message system development
 *
 *
 */

// Tips
// Need to Change Engine -> ALTER TABLE  `pm` ENGINE = MYISAM ;

// -- Comment is --

include_once "../config/paths.php";
require "../config/database.php";
require "../libs/db/database.php";

$db_conx = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

$tbl_pm = "CREATE TABLE IF NOT EXISTS pm (
                                id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                                receiver INT(11) unsigned NOT NULL,
                                sender INT(11) unsigned NOT NULL,
                                senttime DATETIME NOT NULL,
                                subject VARCHAR(255) NOT NULL,
                                message text NOT NULL,
                                sdelete ENUM('0','1') NOT NULL DEFAULT '0',
                                rdelete ENUM('0','1') NOT NULL DEFAULT '0',
                                parent VARCHAR(2) NOT NULL,
                                hasreplies ENUM('0','1') NOT NULL DEFAULT '0',
                                rread ENUM('0','1') NOT NULL DEFAULT '0',
                                sread ENUM('0','1') NOT NULL DEFAULT '0',
                                PRIMARY KEY (id)
                         )";
$query = $db_conx->exec($tbl_pm);
//$tbl_pm = "ALTER TABLE  `pm` ENGINE = MYISAM";
//$query = $db_conx->exec($tbl_pm);
if($query===false) {
        echo "<h3>pm table NOT created :( </h3>";
}
else {
        echo "<h3>pm table created OK :) </h3>";
}


$tbl_users = "CREATE TABLE IF NOT EXISTS users (
				id INT(11) unsigned NOT NULL AUTO_INCREMENT,
				username VARCHAR(20) NOT NULL,
				email VARCHAR(255) NOT NULL,
				password VARCHAR(255) NOT NULL,
				gender ENUM('n','m','f') NOT NULL DEFAULT 'n',
				website VARCHAR(255) NULL,
				country VARCHAR(255) NULL,
				userlevel ENUM('a','b','c','d','e','f') NOT NULL DEFAULT 'a',
				avatar VARCHAR(500) NULL,
                cover VARCHAR(500) NULL,
				ip VARCHAR(255) NOT NULL,

                birthday date NOT NULL,
                aboutme text NULL,
                sns text NULL,
                rprofile ENUM('0', '1', '2') NOT NULL DEFAULT '0',
                rtimeline ENUM('0', '1', '2') NOT NULL DEFAULT '0',
                coverstatus VARCHAR(100) NULL,

				signup DATETIME NOT NULL,
				lastlogin DATETIME NOT NULL,
				notescheck DATETIME NOT NULL,
				activated ENUM('0','1','2') NOT NULL DEFAULT '0',
                toffset VARCHAR(4) DEFAULT '0',

                today_visit INT(11) unsigned DEFAULT '0',
                total_visit INT(11) unsigned DEFAULT '0',

                visittime DATETIME NOT NULL,
                numlikeme INT(11) unsigned DEFAULT '0',

				PRIMARY KEY (id),
				UNIQUE KEY username (username,email)
			 )";
$query = $db_conx->exec($tbl_users);
if($query===false) {
	echo "<h3>user table NOT created :( </h3>";
}
else {
	echo "<h3>user table created OK :) </h3>";
}

// subscriber : 구독자
// subscriptions : 내가 구독하는 것
// favorite_blogs : 좋은 블로그 글 좋아요 한 것
$tbl_actions = "CREATE TABLE IF NOT EXISTS uaction (
                userid INT(11) unsigned NOT NULL,
                subscribers text NULL,
                subscriptions text NULL,
                favorite_blogs text NULL,
                ilike text NULL,
                likeme text NULL,
                PRIMARY KEY (userid)
             )";

$query = $db_conx->exec($tbl_actions);
if($query===false) {
    echo "<h3>uaction table NOT created :( </h3>";
}
else {
    echo "<h3>uaction table created OK :) </h3>";
}




$tbl_users_profile = "CREATE TABLE IF NOT EXISTS users_profile (
                userid INT(11) unsigned NOT NULL references users(id),
                firstname VARCHAR(255) NOT NULL,
                lastname VARCHAR(255) NOT NULL,
                address1 VARCHAR(255) NOT NULL,
                address2 VARCHAR(255) NOT NULL,
                state VARCHAR(255) NOT NULL,
                city VARCHAR(255) NOT NULL,
                zip VARCHAR(255) NOT NULL,
                phone VARCHAR(255) NOT NULL,
                accounttype VARCHAR(255) NOT NULL,
                accountnotes VARCHAR(255) NOT NULL,
                PRIMARY KEY (userid)
             )";
$query = $db_conx->exec($tbl_users_profile);
if($query===false) {
    echo "<h3>users_profile table NOT created :( </h3>";
}
else {
    echo "<h3>users_profile table created OK :) </h3>";
}
//$tbl_users_profile = "ALTER TABLE  `users_profile` ENGINE = MYISAM";
//$query = $db_conx->exec($tbl_users_profile);




$tbl_useroptions = "CREATE TABLE IF NOT EXISTS useroptions (
                id INT(11) unsigned NOT NULL,
                username VARCHAR(20) NOT NULL,
				background VARCHAR(255) NOT NULL,
				question VARCHAR(255) NULL,
				answer VARCHAR(255) NULL,
                temppass VARCHAR(255) NOT NULL,
                spass VARCHAR(64) NOT NULL,
                spass_key VARCHAR(32) NOT NULL,
                PRIMARY KEY (id),
                UNIQUE KEY username (username)
                )";


$query = $db_conx->exec($tbl_useroptions);
if($query===false) {
	echo "<h3>useroptions table NOT created :( </h3>";
}
else {
	echo "<h3>useroptions table created OK :) </h3>";
}

$tbl_friends = "CREATE TABLE IF NOT EXISTS friends (
                id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                req INT(11) unsigned NOT NULL,
                acc INT(11) unsigned NOT NULL,
                name VARCHAR(20) NOT NULL,
                datemade DATETIME NOT NULL,
                accepted ENUM('0','1') NOT NULL DEFAULT '0',
                PRIMARY KEY (id)
                )";
$query = $db_conx->exec($tbl_friends);
if($query===false) {
	echo "<h3>friends table NOT created :( </h3>";
}
else {
	echo "<h3>friends table created OK :) </h3>";
}


$tbl_blockedusers = "CREATE TABLE IF NOT EXISTS blockedusers (
                id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                blocker INT(11) unsigned NOT NULL,
                blockee INT(11) unsigned NOT NULL,
                blockdate DATETIME NOT NULL,
                PRIMARY KEY (id)
                )";

$query = $db_conx->exec($tbl_blockedusers);
if($query===false) {
	echo "<h3>blockedusers table NOT created :( </h3>";
}
else {
	echo "<h3>blockedusers table created OK :) </h3>";
}


// like id: i like someone
// likeshopid: i like shop
// likegid: i like group
// likepid: i like product
// likephid: i like photo
// likepostid: i like post comment
// likepostrid: i like post comment reply
// likeblogid: i like blog
// likeblogcid: i like blog content
// likemeid: who likes me ->removed
// likemenum: number of likes me ->removed and move to users table
// likedate: last update like tables
/** likeid text NULL,
    likeshopid text NULL,
    likegid text NULL,
    likepid text NULL,
    likephid text NULL,
    likepostid text NULL,
    likepostrid text NULL,
    likeblogid text NULL,
    likeblogcid text NULL,
    likemeid text NULL,
    likemenum INT(11) unsigned DEFAULT '0',
    UNIQUE KEY userid (userid)
*/
$tbl_likes = "CREATE TABLE IF NOT EXISTS likes (
                userid INT(11) unsigned NOT NULL,
                numid INT(11) unsigned unsigned DEFAULT '0',
                type ENUM('user','shop','group','product','photo','posting','postreply','blog','blogreply') NOT NULL DEFAULT 'user',
                likeid INT(11) unsigned NOT NULL,
                likedate DATETIME NOT NULL
                )";

$query = $db_conx->exec($tbl_likes);
if($query===false) {
    echo "<h3>likes table NOT created :( </h3>";
}
else {
    echo "<h3>likes table created OK :) </h3>";
}



$tbl_status = "CREATE TABLE IF NOT EXISTS status (
                id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                osid INT(11) unsigned NOT NULL,
                owner INT(11) unsigned NOT NULL,
                author INT(11) unsigned NOT NULL,
                type ENUM('a','b','c') NOT NULL,
                data TEXT NOT NULL,
                postdate DATETIME NOT NULL,
                PRIMARY KEY (id)
                )";

$query = $db_conx->exec($tbl_status);
if($query===false) {
	echo "<h3>status table NOT created :( </h3>";
}
else {
	echo "<h3>status table created OK :) </h3>";
}

$tbl_photos = "CREATE TABLE IF NOT EXISTS photos (
                id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                userid INT(11) unsigned NOT NULL,
                gallery VARCHAR(30) NOT NULL,
	            filename VARCHAR(255) NULL,
                description VARCHAR(255) NULL,
                uploaddate DATETIME NOT NULL,
                PRIMARY KEY (id)
                )";

$query = $db_conx->exec($tbl_photos);
if($query===false) {
	echo "<h3>photos table NOT created :( </h3>";
}
else {
	echo "<h3>photos table created OK :) </h3>";
}

$tbl_notifications = "CREATE TABLE IF NOT EXISTS notifications (
                id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                userid INT(11) unsigned NOT NULL,
                initiator INT(11) unsigned NOT NULL,
                app VARCHAR(255) NOT NULL,
                note VARCHAR(255) NOT NULL,
                did_read ENUM('0','1') NOT NULL DEFAULT '0',
                date_time DATETIME NOT NULL,
                PRIMARY KEY (id)
                )";

$query = $db_conx->exec($tbl_notifications);
if($query===false) {
	echo "<h3>notifications table NOT created :( </h3>";
}
else {
	echo "<h3>notifications table created OK :) </h3>";
}



// make a posting table
// type: d(default), p(photo), li(link), v(video), lo(location)
// id is not auto increment but author is logged in user make posting then id is increment +1
// owner: owner is time line page owner so owner can be author and not the same
// likes: likes numbers
// likeuid: like user id
$tbl_posting = "CREATE TABLE IF NOT EXISTS posting (
                id INT(11) unsigned NOT NULL DEFAULT '0',
                owner INT(11) unsigned NOT NULL,
                author INT(11) unsigned NOT NULL,
                type ENUM('d','p','li','v','lo') NOT NULL DEFAULT 'd',
                content text NOT NULL,
                image text NOT NULL,
                title VARCHAR (100) NOT NULL,
                canonicalurl VARCHAR (300) NOT NULL,
                url VARCHAR (500) NOT NULL,
                description VARCHAR (500) NOT NULL,
                iframe text NOT NULL,
                likes int(11) unsigned NOT NULL DEFAULT '0',
                likeuid text NULL,
                postdate DATETIME NOT NULL
                )";

$query = $db_conx->exec($tbl_posting);
if($query===false) {
    echo "<h3>posting table NOT created :( </h3>";
}
else {
    echo "<h3>posting table created OK :) </h3>";
}

// pid: original posting id
// sid: reply sub id
// author: who make reply
// type: same as posting
//
$tbl_posting_reply = "CREATE TABLE IF NOT EXISTS postingreply (
                pid INT(11) unsigned NOT NULL,
                sid INT(11) unsigned NOT NULL DEFAULT '0',
                author INT(11) unsigned NOT NULL,
                type ENUM('d','p','li','v','lo') NOT NULL DEFAULT 'd',
                content text NOT NULL,
                image text NOT NULL,
                title VARCHAR (100) NOT NULL,
                canonicalurl VARCHAR (300) NOT NULL,
                url VARCHAR (500) NOT NULL,
                description VARCHAR (500) NOT NULL,
                iframe text NOT NULL,
                likes int(11) unsigned NOT NULL DEFAULT '0',
                likeuid text NULL,
                postdate DATETIME NOT NULL
                )";

$query = $db_conx->exec($tbl_posting_reply);
if($query===false) {
    echo "<h3>postingreply table NOT created :( </h3>";
}
else {
    echo "<h3>postingreply table created OK :) </h3>";
}
