<?php
/**
 * Add or Cahnges
 * /Date/What/HOW/WHY
 *
 *
 *
 */

// Tips
// Need to Change Engine -> ALTER TABLE  `pm` ENGINE = MYISAM ;


include_once "../config/paths.php";
require "../config/database.php";
require "../libs/db/database.php";

$db_conx = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

$tbl_orders = "CREATE TABLE IF NOT EXISTS orders (
                                orderid INT(11) NOT NULL AUTO_INCREMENT,
                                customerid INT(11) NOT NULL references users(id),
                                customername VARCHAR(20) NOT NULL,
                                amount float(6,2),
                                ordertime DATETIME NOT NULL,
                                orderstatus VARCHAR(10),
                                ship_name VARCHAR(60) NOT NULL,
                                ship_address1 VARCHAR(255) NOT NULL,
                                ship_address2 VARCHAR(255) NOT NULL,
                                ship_state VARCHAR(255) NOT NULL,
                                ship_city VARCHAR(255) NOT NULL,
                                ship_zip VARCHAR(255) NOT NULL,
                                ship_country VARCHAR(255) NULL,
                                PRIMARY KEY (orderid)
                         )";

//method 1 : mysqli
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_orders); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object

if($query===false) {
        echo "<h3>orders table NOT created :( </h3>";
}
else {
        echo "<h3>orders table created OK :) </h3>";
}
$tbl_orders = "ALTER TABLE  `orders` ENGINE = InnoDB";
$query = $db_conx->exec($tbl_orders);


$tbl_products = "CREATE TABLE IF NOT EXISTS products (
				productid INT(11) NOT NULL AUTO_INCREMENT,
				shopname VARCHAR(255) NOT NULL references users(shopname),
                title VARCHAR(255),
                catid INT(11),
                price float(6,2) NOT NULL,
                description TEXT NOT NULL,
                photo_path VARCHAR(255) NOT NULL,
                likes INT(11) NOT NULL DEFAULT '0',
                likers TEXT NULL,
				PRIMARY KEY (productid)
			 )";

$query = $db_conx->exec($tbl_products); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
    echo "<h3>products table NOT created :( </h3>";
}
else {
    echo "<h3>products table created OK :) </h3>";
}
$tbl_products = "ALTER TABLE  `products` ENGINE = InnoDB";
$query = $db_conx->exec($tbl_products);


$tbl_cart_options = "CREATE TABLE IF NOT EXISTS cart_options (
                productid INT(11) NOT NULL references products(productid),
                option_key TEXT NULL,
                option_value TEXT NULL,
                option_quantity TEXT NULL,
                PRIMARY KEY (productid)
             )";

//method 1 : mysqli
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_cart_options); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>cart_options table NOT created :( </h3>";
}
else {
	echo "<h3>cart_options table created OK :) </h3>";
}
$tbl_cart_options = "ALTER TABLE  `cart_options` ENGINE = InnoDB";
$query = $db_conx->exec($tbl_cart_options);


////////////////////////////////////
$tbl_cart_category = "CREATE TABLE IF NOT EXISTS cart_category (
                catid INT(11) NOT NULL AUTO_INCREMENT,
                catmain VARCHAR(255),
                catsub TEXT,
                PRIMARY KEY (catid)
                )";

//method 1 : mysqli
//$query = mysqli_query($db_conx, $tbl_useroptions);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_cart_category); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>cart_category table NOT created :( </h3>";
}
else {
	echo "<h3>cart_category table created OK :) </h3>";
}
$tbl_cart_category = "ALTER TABLE  `cart_category` ENGINE = InnoDB";
$query = $db_conx->exec($tbl_cart_category);

////////////////////////////////////
$tbl_order_items = "CREATE TABLE IF NOT EXISTS order_items (
                orderid INT(11) NOT NULL references orders(orderid),
                productid INT(11) NOT NULL references products(productid),
                option_key TEXT NULL,
                option_index TEXT NULL,
                item_price float(6,2) NOT NULL,
                quantity INT(11) NOT NULL,
                PRIMARY KEY (orderid, productid)
                )";

//method 1 : mysqli
//$query = mysqli_query($db_conx, $tbl_friends);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_order_items); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>order_items table NOT created :( </h3>";
}
else {
	echo "<h3>order_items table created OK :) </h3>";
}
$tbl_order_items = "ALTER TABLE  `order_items` ENGINE = InnoDB";
$query = $db_conx->exec($tbl_order_items);



$tbl_shops_info = "CREATE TABLE IF NOT EXISTS shops_info (
                shopid INT(11) NOT NULL AUTO_INCREMENT,
                shopname VARCHAR(100) NULL,
                adminid INT(11) NOT NULL,
                adminname VARCHAR(20) NOT NULL,
                creation DATETIME NOT NULL,
                logo VARCHAR(255) NOT NULL,
                members text NOT NULL,
                shopcheck DATETIME NOT NULL,
                activated ENUM('0','1') NOT NULL DEFAULT '0',

                address1 VARCHAR(255) NOT NULL,
                address2 VARCHAR(255) NOT NULL,
                state VARCHAR(255) NOT NULL,
                city VARCHAR(255) NOT NULL,
                zip VARCHAR(255) NOT NULL,
                phone VARCHAR(255) NOT NULL,
                youtube VARCHAR(255) NULL,
                accounttype VARCHAR(255) NOT NULL,
                accountnotes VARCHAR(255) NOT NULL,
                aboutshop text NOT NULL,
                PRIMARY KEY (shopid)
             )";


$query = $db_conx->exec($tbl_shops_info); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
    echo "<h3>shops_info table NOT created :( </h3>";
}
else {
    echo "<h3>shops_info table created OK :) </h3>";
}
$tbl_shops_info = "ALTER TABLE  `shops_info` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_shops_info);



$tbl_shopmembers = "CREATE TABLE IF NOT EXISTS shopmembers (
                id INT(11) NOT NULL AUTO_INCREMENT,
                shopid INT(11) NOT NULL,
                memberid INT(11) NOT NULL,
                membername VARCHAR(20) NOT NULL,
                approved ENUM('0','1') NOT NULL DEFAULT '0',
                admin ENUM('0','1') NOT NULL DEFAULT '0',
                PRIMARY KEY (id)
             )";

$query = $db_conx->exec($tbl_shopmembers); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
    echo "<h3>shopmembers table NOT created :( </h3>";
}
else {
    echo "<h3>shopmembers table created OK :) </h3>";
}
$tbl_shopmembers = "ALTER TABLE  `shopmembers` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_shopmembers);
