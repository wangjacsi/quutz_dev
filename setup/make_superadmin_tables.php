<?php
/**
 * Add or Cahnges
 * /Date/What/HOW/WHY
 *
 */


include_once "../config/paths.php";
include_once "../config/constants.php";
require "../config/database.php";
require "../libs/db/database.php";
require "../libs/core/hash.php";


$db_conx = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

$tbl_super_doodu = "CREATE TABLE IF NOT EXISTS super_doodu (
                                id TINYINT NOT NULL AUTO_INCREMENT,
                                name VARCHAR(20) NOT NULL,
                                pass VARCHAR(255) NOT NULL,
                                level ENUM('a','b','c','d','e','f') NOT NULL DEFAULT 'a',
                                lastlogin DATETIME NOT NULL,
                                lastaction DATETIME NOT NULL,
                                PRIMARY KEY (id)
                         )";

//method 1 : mysqli
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_super_doodu); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
        echo "<h3>super_doodu table NOT created :( </h3>";
}
else {
        echo "<h3>super_doodu table created OK :) </h3>";
}
$tbl_super_doodu = "ALTER TABLE  `super_doodu` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_super_doodu);




$tbl_doodu_action = "CREATE TABLE IF NOT EXISTS doodu_action (
                id INT(11) NOT NULL AUTO_INCREMENT,
                name VARCHAR(20) NOT NULL,
                actiontype VARCHAR(20) NOT NULL,
                action TEXT NOT NULL,
                actiondate DATETIME NOT NULL,
                PRIMARY KEY (id)
             )";

//method 1 : mysqli
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_doodu_action); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
    echo "<h3>doodu_action table NOT created :( </h3>";
}
else {
    echo "<h3>doodu_action table created OK :) </h3>";
}
$tbl_doodu_action = "ALTER TABLE  `doodu_action` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_doodu_action);

// inser super admin just one
$p = Hash::create(HASH_TYPE, SUPER_ADMIN_PASS, HASH_PASSWORD_KEY);
$sth = $db_conx->prepare("INSERT INTO super_doodu(name, pass, level, lastlogin, lastaction)
            VALUES (:name, :pass, :level, now(), now())");
        $data = array(':name'=>SUPER_ADMIN, ':pass'=>$p,':level'=>'f');
foreach ($data as $key => $value){
    $sth->bindValue("$key", $value);
}
$sth->execute();


