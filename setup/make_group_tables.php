<?php
/**
 * Add or Cahnges
 * /Date/What/HOW/WHY
 * 2013-12-25/Users Table Add/
 *  - Add: firstname, lastname, address1, address2, state, city, 
 *    zip, phone, youtube, shopname, accounttype, accountnotes, aboutme
 *  - users table need more informations
 * 
 * 
 */

// Tips
// Need to Change Engine -> ALTER TABLE  `pm` ENGINE = MYISAM ;


include_once "../config/paths.php";
require "../config/database.php";
require "../libs/db/database.php";

$db_conx = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

$tbl_groups = "CREATE TABLE IF NOT EXISTS groups (
                                id INT(11) NOT NULL AUTO_INCREMENT,
                                name VARCHAR(100) NOT NULL,
                                creation DATETIME NOT NULL,
                                logo VARCHAR(255) NOT NULL,
                                invrule ENUM('0','1') NOT NULL,
                                creator VARCHAR(20) NOT NULL, 
                                about TEXT NOT NULL,                               
                                PRIMARY KEY (id)                                
                         )";
                         
//method 1 : mysqli                      
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_groups); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
        echo "<h3>groups table NOT created :( </h3>";     
} 
else {
        echo "<h3>groups table created OK :) </h3>";
}
$tbl_groups = "ALTER TABLE  `groups` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_groups); 


$tbl_gmembers = "CREATE TABLE IF NOT EXISTS gmembers (
				id INT(11) NOT NULL AUTO_INCREMENT,
				gname VARCHAR(100) NOT NULL,
				mname VARCHAR(20) NOT NULL,
				approved ENUM('0','1') NOT NULL,
				admin ENUM('0','1') NOT NULL DEFAULT '0',				
				PRIMARY KEY (id)
			 )";
			 
//method 1 : mysqli			 
//$query = mysqli_query($db_conx, $tbl_users);

//method 2 : PDO statment
$query = $db_conx->exec($tbl_gmembers); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>gmembers table NOT created :( </h3>";	
} 
else {
	echo "<h3>gmembers table created OK :) </h3>";
}
$tbl_gmembers = "ALTER TABLE  `gmembers` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_gmembers); 



////////////////////////////////////
$tbl_grouppost = "CREATE TABLE IF NOT EXISTS grouppost ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                pid VARCHAR(20) NOT NULL,
				gname VARCHAR(100) NOT NULL,
                author VARCHAR(20) NOT NULL,
                type ENUM('0','1') NOT NULL,
                data TEXT NOT NULL,
                pdate DATETIME NOT NULL,
                PRIMARY KEY (id)
                )"; 

//method 1 : mysqli			 
//$query = mysqli_query($db_conx, $tbl_useroptions); 

//method 2 : PDO statment
$query = $db_conx->exec($tbl_grouppost); //single query execution return the number of rows
//$db_conx->query($tbl_users); //single query execution return result object
if($query===false) {
	echo "<h3>grouppost table NOT created :( </h3>";	
} 
else {
	echo "<h3>grouppost table created OK :) </h3>";
}
$tbl_grouppost = "ALTER TABLE  `grouppost` ENGINE = MYISAM";
$query = $db_conx->exec($tbl_grouppost); 


