
        <!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">
                <div class="block_slider_type_1 general_not_loaded">
                    <div id="slider" class="slider flexslider">
                        <ul class="slides">
                            <li>
                                <img src="<?php global $MAIN_SLIDER_PIC; echo $MAIN_SLIDER_PIC['pic1']; ?>">
                                <div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Travel, PHOTOGRAPHY</div>
                                <div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Mountains in Switzerland</div>
                                <div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
                            </li>

                            <li>
                                <img src="<?php echo $MAIN_SLIDER_PIC['pic2']; ?>">
                                <div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Travel, PHOTOGRAPHY</div>
                                <div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">My Love - New York</div>
                                <div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
                            </li>

                            <li>
                                <img src="<?php echo $MAIN_SLIDER_PIC['pic3']; ?>">
                                <div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Life, PHOTOGRAPHY</div>
                                <div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">My Awesome workplace</div>
                                <div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
                            </li>

                            <li>
                                <img src="<?php echo $MAIN_SLIDER_PIC['pic4']; ?>">
                                <div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Life, PHOTOGRAPHY</div>
                                <div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Flying over the mountains</div>
                                <div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
                            </li>

                            <li>
                                <img src="<?php echo $MAIN_SLIDER_PIC['pic5']; ?>">
                                <div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">People, Fashion</div>
                                <div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Young Businessman</div>
                                <div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
                            </li>

                            <li>
                                <img src="<?php echo $MAIN_SLIDER_PIC['pic6']; ?>">
                                <div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">People, Life</div>
                                <div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">The guy on the field</div>
                                <div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
                            </li>
                        </ul>
                    </div>

                    <script type="text/javascript">
                        jQuery(function() {
                            init_slider_1('#slider');
                        });
                    </script>
                </div>

                <div class="block_general_title_1 w_margin_1">
                    <h1>UNIQUE AND BETTER</h1>
                    <h2>Find your item and enjoy!</h2>
                </div>

                <div class="block_posts type_1 type_sort general_not_loaded">
                    <div class="posts">
                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[0]; ?>"><img src="<?php global $CATEGORY_PIC; echo $CATEGORY_PIC[$this->category_array2[0]]['main']; ?>"><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[0]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">AEEDFEsdasdadadsdadR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[0]; ?>"><span><?php echo $this->category_array[0]; ?></span> <?php echo $CATEGORY_PIC[$this->category_array2[0]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[0]]['sub'];?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">ATRS dssas dad Jd ddfsf dsdfdf sf dsdsdadadsd</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Accusantium doloremque lauda ntium, totam. >Accusantium doloremque lauda ntium, totam. >Accusantium doloremque lauda ntium, totam. >Accusantium doloremque lauda ntium, totam. >Accusantium doloremque lauda ntium, totam. >Accusantium doloremque lauda ntium, totam.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[1]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[1]]['main']; ?>"><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[1]; ?></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">dfasdasR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[1]; ?>"><span><?php echo $this->category_array[1]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[1]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[1]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">AT56d45</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[2]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[2]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[2]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">TRUE DJFU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[2]; ?>"><span><?php echo $this->category_array[2]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[2]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[2]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">ATRS dssas dad Jd ddfsf dsdfdf sf dsdsdadadsd</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae. Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_3">
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><a href="#">EVENT</a>, <a href="#">UNTIL</a></div>
                                    <div class="date">27, 2013</div>
                                    <div class="stats">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Veritatis et quasi architecto beatae vitae dicta sunt explicabo.</a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_3">
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><a href="#">EVENT</a>, <a href="#">UNTIL</a></div>
                                    <div class="date">27, 2013</div>
                                    <div class="stats">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Accusantium doloremque laudantium, totam rem aperiam, eaqu.</a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_3">
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><a href="#">EVENT</a>, <a href="#">UNTIL</a></div>
                                    <div class="date">27, 2013</div>
                                    <div class="stats">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Accusantium doloremque laudantium, totam rem aperiam, eaqu.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[3]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[3]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[3]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PAINTING BY <strong><a href="#" class="font-color1">YIDUF LU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[3]; ?>"><span><?php echo $this->category_array[3]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[3]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[3]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">Bdhrh 198</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup. Sed ut perspiciatis unde omnis iste natus sit volup. Sed ut perspiciatis unde omnis iste natus sit volup. Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[4]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[4]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[4]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">33dffTRUE DJFU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[4]; ?>"><span><?php echo $this->category_array[4]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[4]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[4]]['main']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">REAL madrid</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[5]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[5]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[5]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">1998 CREU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[5]; ?>"><span><?php echo $this->category_array[5]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[5]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[5]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">Yiufghjh - TRUY</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[6]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[6]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[6]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">Bdhrh 198</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[6]; ?>"><span><?php echo $this->category_array[6]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[6]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[6]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">Yiufghjh - TRUY</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[7]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[7]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[7]; ?></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">rttEU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[7]; ?>"><span><?php echo $this->category_array[7]; ?></span><?php echo $CATEGORY_PIC[$this->category_array2[7]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[7]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">Yiufghjh - TRUY</a></strong></div>
                                    <!--<div class="date">27, 2013</div>-->
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1">
                            <div class="feature">
                                <div class="image">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[8]; ?>"><img src="<?php echo $CATEGORY_PIC[$this->category_array2[8]]['main']; ?>" alt=""><span class="hover no_icon"></span><span class="overay1 hover no_icon"><?php echo $this->category_array[8]; ?></span></a>
                                </div>
                                <div class="review">
                                    <div class="num">7.5</div>
                                    <div class="type">score</div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">PHOTO BY <strong><a href="#" class="font-color1">rttEU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[8]; ?>"><?php echo $CATEGORY_PIC[$this->category_array2[8]]['title']; ?></a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_2">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="<?php echo $CATEGORY_PIC[$this->category_array2[8]]['sub']; ?>" alt=""><span class="hover"></span></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">Yiufghjh - TRUY</a></strong></div>
                                    <div class="stats show-stat f-left m-top1">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</a>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_3">
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><a href="#">EVENT</a>, <a href="#">UNTIL</a></div>
                                    <div class="date">27, 2013</div>
                                    <div class="stats">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Veritatis et quasi architecto beatae vitae dicta sunt explicabo.</a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_3">
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><a href="#">EVENT</a>, <a href="#">UNTIL</a></div>
                                    <div class="date">27, 2013</div>
                                    <div class="stats">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Accusantium doloremque laudantium, totam rem aperiam, eaqu.</a>
                                </div>
                            </div>
                        </article>
                        <article class="post_type_3">
                            <div class="content">
                                <div class="info">
                                    <div class="tags"><a href="#">EVENT</a>, <a href="#">UNTIL</a></div>
                                    <div class="date">27, 2013</div>
                                    <div class="stats">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="">Accusantium doloremque laudantium, totam rem aperiam, eaqu.</a>
                                </div>
                            </div>
                        </article>

                        <!--<article class="post_type_1">
                            <div class="feature">

                                <div class="slider">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[6]; ?>"><span class="cat-name"><?php echo $this->category_array[6]; ?></span></a>
                                    <div id="post_slider_1" class="flexslider">
                                        <ul class="slides">
                                            <li><img src="https://img1.etsystatic.com/035/0/6414460/il_340x270.628381141_iay4.jpg" alt=""></li>
                                            <li><img src="https://img1.etsystatic.com/043/2/8157280/il_340x270.586338037_b3t8.jpg" alt=""></li>
                                            <li><img src="https://img0.etsystatic.com/032/0/9722637/il_224xN.629185334_469v.jpg" alt=""></li>
                                        </ul>
                                        <span class="hover no_icon"></span>
                                    </div>

                                    <script type="text/javascript">
                                        jQuery(function() {
                                            init_post_slider_1('#post_slider_1');
                                        });
                                    </script>
                                </div>
                            </div>

                            <div class="content">
                                <div class="info">
                                    <div class="tags"><strong><a href="" class="font-color1">Bdhrh 198</a></strong></div>

                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>

                                <div class="title">
                                    <a href="<?php echo URL.'category/'.$this->category_array2[6]; ?>"><span><?php echo $this->category_array[6]; ?></span> Cute and sdksk.</a>
                                </div>
                            </div>
                        </article>-->

                    </div>

                    <!--<div class="controls">
                        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
                    </div>-->
                </div>

            </div>
        </div>
        <!-- CONTENT END -->

