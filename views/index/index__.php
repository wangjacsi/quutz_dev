
		<!-- CONTENT BEGIN -->
		<div id="content" class="">
			<div class="inner">
				<div class="block_slider_type_1 general_not_loaded">
					<div id="slider" class="slider flexslider">
						<ul class="slides">
							<li>
								<img src="http://enterprise-html.weblionmedia.com/images/pic_slider_1_3.jpg"> <!--<?php echo PUBLIC_PATH; ?>images/pic_slider_1_1.jpg" alt="">-->
								<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Travel, PHOTOGRAPHY</div>
								<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Mountains in Switzerland</div>
								<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
							</li>

							<li>
								<img src="http://enterprise-html.weblionmedia.com/images/pic_slider_1_2.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_slider_1_2.jpg" alt="">-->
								<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Travel, PHOTOGRAPHY</div>
								<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">My Love - New York</div>
								<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
							</li>

							<li>
								<img src="http://enterprise-html.weblionmedia.com/images/pic_slider_1_1.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_slider_1_3.jpg" alt="">-->
								<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Life, PHOTOGRAPHY</div>
								<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">My Awesome workplace</div>
								<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
							</li>

							<li>
								<img src="http://enterprise-html.weblionmedia.com/images/pic_slider_1_4.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_slider_1_4.jpg" alt="">-->
								<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Life, PHOTOGRAPHY</div>
								<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Flying over the mountains</div>
								<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
							</li>

							<li>
								<img src="http://enterprise-html.weblionmedia.com/images/pic_slider_1_5.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_slider_1_5.jpg" alt="">-->
								<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">People, Fashion</div>
								<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">Young Businessman</div>
								<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
							</li>

							<li>
								<img src="http://enterprise-html.weblionmedia.com/images/pic_slider_1_6.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_slider_1_6.jpg" alt="">-->
								<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">People, Life</div>
								<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown">The guy on the field</div>
								<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="#" class="general_button_type_1">Read More</a></div>
							</li>
						</ul>
					</div>

					<script type="text/javascript">
						jQuery(function() {
							init_slider_1('#slider');
						});
					</script>
				</div>

				<div class="block_general_title_1 w_margin_1">
					<h1>Latest Posts</h1>
					<h2>ENTER THE SUB TITLE FOR THIS SECTION</h2>
				</div>

				<div class="block_posts type_1 type_sort general_not_loaded">
					<div class="posts">
						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="http://enterprise-html.weblionmedia.com/images/pic_post_1_1.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_post_1_1.jpg" alt="">--><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html"><span>TOP NEWS.</span> Sed ut perspiciatis unde omnis iste natus sit volup.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="http://enterprise-html.weblionmedia.com/images/pic_post_1_6.jpg"><!--<?php echo PUBLIC_PATH; ?>images/pic_post_1_6.jpg" alt="">--><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Red ut perspiciatis unde omnis iste.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_11.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Perspiciatis unde omnis iste.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_7.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Totam rem aperiam, eaque ipsa quae ab illo inventore verit.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_12.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html"><span>NEWS.</span> Totam rem aperiam, eaque ipsa quae ab illo inventore.</a>
								</div>
							</div>
						</article>

						<article class="post_type_2">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_2.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</a>
								</div>
							</div>
						</article>

						<article class="post_type_3">
							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Veritatis et quasi architecto beatae vitae dicta sunt explicabo.</a>
								</div>
							</div>
						</article>

						<article class="post_type_2">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_13.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Accusantium doloremque lauda ntium, totam.</a>
								</div>
							</div>
						</article>

						<article class="post_type_2">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_3.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_14.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
								</div>
							</div>
						</article>

						<article class="post_type_2">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_8.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="slider">
									<div id="post_slider_1" class="flexslider">
										<ul class="slides">
											<li><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_4.jpg" alt=""></li>
											<li><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_16.jpg" alt=""></li>
											<li><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_17.jpg" alt=""></li>
										</ul>
									</div>

									<script type="text/javascript">
										jQuery(function() {
											init_post_slider_1('#post_slider_1');
										});
									</script>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html"><span>TOP NEWS.</span> Quae ab illo inventore veritatis et quasi architecto beatae.</a>
								</div>
							</div>
						</article>

						<article class="post_type_2">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_9.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post_video.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_10.jpg" alt=""><span class="hover no_icon"></span><span class="icon video"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post_video.html"><span>TOP NEWS.</span> Totam rem aperiam, eaque ipsa quae ab illo inven.</a>
								</div>
							</div>
						</article>

						<article class="post_type_2">
							<div class="feature">
								<div class="image">
									<a href="blog_post.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_15.jpg" alt=""><span class="hover"></span></a>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</a>
								</div>
							</div>
						</article>

						<article class="post_type_1">
							<div class="feature">
								<div class="image">
									<a href="blog_post_review.html"><img src="<?php echo PUBLIC_PATH; ?>images/pic_post_1_5.jpg" alt=""><span class="hover"></span></a>
								</div>
								<div class="review">
									<div class="num">7.5</div>
									<div class="type">score</div>
								</div>
							</div>

							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post_review.html">Voluptatem sequi nesciunt.</a>
								</div>
							</div>
						</article>

						<article class="post_type_3">
							<div class="content">
								<div class="info">
									<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
									<div class="date">27, 2013</div>
									<div class="stats">
										<div class="likes">15</div>
										<div class="comments">7</div>
									</div>
								</div>

								<div class="title">
									<a href="blog_post.html">Accusantium doloremque laudantium, totam rem aperiam, eaqu.</a>
								</div>
							</div>
						</article>
					</div>

					<div class="controls">
						<a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
					</div>
				</div>

			</div>
		</div>
		<!-- CONTENT END -->

