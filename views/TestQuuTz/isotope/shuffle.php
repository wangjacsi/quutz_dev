<h1>Isotope - shuffle</h1>

<p><button id="shuffle">Shuffle</button></p>

<div class="isotope">
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
  <div class="item width2 height2"></div>
  <div class="item width2"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
</div>
