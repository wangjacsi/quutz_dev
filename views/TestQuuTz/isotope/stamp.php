<h1>Isotope - stamp method</h1>

<p><button id="toggle-stamp">Toggle stamp</button></p>

<div class="isotope">
  <div class="stamp stamp1"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
  <div class="item width2 height2"></div>
  <div class="item width2"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
</div>
