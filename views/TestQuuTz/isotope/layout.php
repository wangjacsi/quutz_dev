<h1>Isotope - layout modes</h1>

<div id="layout-mode-button-group" class="button-group">
  <button class="button is-checked" data-layout-mode-value="masonry" checked="checked">masonry</button>
  <button class="button" data-layout-mode-value="fitRows">fitRows</button>
  <button class="button" data-layout-mode-value="cellsByRow">cellsByRow</button>
  <button class="button" data-layout-mode-value="vertical"> vertical</button>
  <button class="button" data-layout-mode-value="masonryHorizontal" data-is-horizontal="true">masonryHorizontal</button>
  <button class="button" data-layout-mode-value="fitColumns" data-is-horizontal="true">fitColumns</button>
  <button class="button" data-layout-mode-value="cellsByColumn" data-is-horizontal="true">cellsByColumn</button>
  <button class="button" data-layout-mode-value="horizontal" data-is-horizontal="true">horizontal</button>
</div>

<div id="isotope-demo" class="isotope rainbowed">
  <div class="element-item width2"><p class="symbol">1</p></div>
  <div class="element-item height2"><p class="symbol">2</p></div>
  <div class="element-item"><p class="symbol">3</p></div>
  <div class="element-item"><p class="symbol">4</p></div>
  <div class="element-item width2 height2"><p class="symbol">5</p></div>
  <div class="element-item width2"><p class="symbol">6</p></div>
  <div class="element-item height2"><p class="symbol">7</p></div>
  <div class="element-item"><p class="symbol">8</p></div>
  <div class="element-item"><p class="symbol">9</p></div>
  <div class="element-item width2"><p class="symbol">10</p></div>
  <div class="element-item"><p class="symbol">11</p></div>
  <div class="element-item"><p class="symbol">12</p></div>

</div>
