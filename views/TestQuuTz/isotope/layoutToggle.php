<h1>Isotope - layout</h1>

<p>Click item to toggle size</p>

<div class="isotope">
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
  <div class="item width2 height2"></div>
  <div class="item width2"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
</div>
