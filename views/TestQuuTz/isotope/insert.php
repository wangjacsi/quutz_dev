<h1>Isotope - insert</h1>

<p><button id="insert">Insert items</button></p>

<div class="isotope">
  <div class="item width2">
    <p class="number">59</p>
  </div>
  <div class="item height2">
    <p class="number">41</p>
  </div>
  <div class="item">
    <p class="number">12</p>
  </div>
  <div class="item">
    <p class="number">93</p>
  </div>
  <div class="item">
    <p class="number">5</p>
  </div>
  <div class="item height2">
    <p class="number">17</p>
  </div>
</div>
