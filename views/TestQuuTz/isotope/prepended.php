<h1>Isotope - prepended</h1>

<p><button id="prepend">Prepend items</button></p>

<div class="isotope">
  <div class="item width2"></div>
  <div class="item height2"></div>
  <div class="item"></div>
  <div class="item"></div>
  <div class="item height2"></div>
</div>
