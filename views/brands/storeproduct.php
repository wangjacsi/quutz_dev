
        <!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">

                <div id="title-brands" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="sel-title"><?php echo $this->mainTitle; ?></h1>
                    <h2 id="sel-subtitle"><?php echo $this->subTitle; ?></h2>
                    <div class="intro-filter">
                        <a onclick="introBrands('0')" class="general_colored_button intro-b0" title="Hot Items">HOT</a>
                        <a onclick="introBrands('1')" class="general_colored_button intro-b1" title="New Items">NEW</a>
                        <a onclick="introBrands('2')" class="general_colored_button intro-b2" title="Popular Celebs">CELEBS</a>
                    </div>
                </div>

                <div id="content-hot" class="block_posts type_2 brands owner">
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>1517.78</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>1243K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>33K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/023/0/5495810/il_170x135.570828173_iqq5.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img0.etsystatic.com/000/0/5676972/il_170x135.267997588.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/036/1/7025903/il_170x135.647572383_2saz.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-new" class="block_posts type_2 brands owner">
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/038/0/7800541/il_170x135.586953507_tj5b.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>1517.78</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>1243K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>33K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/021/0/8040395/il_170x135.540674409_m0gx.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/032/0/5439978/il_170x135.620711215_asnt.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img0.etsystatic.com/035/0/8082185/il_170x135.505932036_upkg.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-celebs" class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/038/0/7800541/il_170x135.560662467_d5ev.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/000/0/22097182/iusa_75x75.10034861.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items">Items 2223</div>-->
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img1.etsystatic.com/019/0/8040395/il_170x135.486960133_jpbq.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/009/0/32491922/iusa_75x75.22734352_p610.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items">Items 2223</div>-->
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img0.etsystatic.com/024/0/5439978/il_170x135.475003470_7kzs.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/000/0/8315140/iusa_75x75.7693610.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items">Items 2223</div>-->
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="https://img0.etsystatic.com/027/1/6524871/il_170x135.601638444_lcwc.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/000/0/22097182/iusa_75x75.10034861.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#">Indigo 2014 Premium Jeans df dfs df d </a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i>13227.7</div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items">Items 2223</div>-->
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>12433K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>343K</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div class="block_general_title_1 w_margin_1 m-top20">
                    <h1><?php if($this->subCategory=='') echo 'all '; else echo $this->subCategory.' '; echo $this->mainTitle2; if($this->subCategory2!='') echo ' - '.$this->subCategory2 ?></h1>
                    <h2><?php echo $this->subTitle2; ?></h2>
                    <div class="intro-filter favorite">
                        <a href="<?php echo URL.'brands/store/storeID-storeName';?>" class="general_colored_button botton-color1" title="">ALL</a>
                        <?php
                            if($this->subCategory!=''){
                                echo '<a id="categoryBtn" onclick="showBrandCategory()" class="general_colored_button botton-color1" title="">Sub Category</a><div class="sub-cat">';
                                foreach ($this->categoryArray[$this->subCategory] as $menu){
                                    $tempMenu = $menu;
                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                    echo '<a href="'.URL.'brands/store/storeID-storeName/category/'.$this->subCategory.'/'.$menu.'" class="general_colored_button" title="'.$tempMenu.'">'.$tempMenu.'</a>';
                                }
                                echo '</div><div class="main-cat">';
                                foreach ($this->categoryArray as $key => $menu){
                                    $tempMenu = $key;
                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                    echo '<a href="'.URL.'brands/store/storeID-storeName/category/'.$key.'" class="general_colored_button" title="'.$tempMenu.'">'.$tempMenu.'</a>';
                                }
                                echo '</div>';
                            } else{
                                foreach ($this->categoryArray as $key => $menu){
                                    $tempMenu = $key;
                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                    echo '<a href="'.URL.'brands/store/storeID-storeName/category/'.$key.'" class="general_colored_button" title="'.$tempMenu.'">'.$tempMenu.'</a>';
                                }
                            }
                        ?>
                    </div>
                </div>

                <div class="block_posts type_1 type_sort general_not_loaded">
                    <div class="posts">
                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/041/0/6416669/il_570xN.587135873_p4mx.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <!--<div class="num">4.8</div>-->
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Product name is djks sads sa sd</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>1517.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">AEEDFEsdasdadadsdadR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>15K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/027/1/7789766/il_340x270.628067245_abm6.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">PGJ sjsksaiis jean</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>87.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">ABC 189</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/025/0/8925614/il_570xN.543457281_tgit.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">156 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">fgf fg fgdads sadda sdas d dasd ksaiis jean</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>37.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">57HRFG</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>5K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/039/0/5631970/il_300x300.631159389_4kpi.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">556 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">TU fhns jsjdh jsdh ds sds </a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>55
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">OUTSTANDING</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>35K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>1342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="//resize-ec1.thefancy.com/resize/crop/313/thingd/default/668048234370832136_af3cd9d0be32.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:85%;"></div></div>
                                    <div class="review-num">2.5K reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Ejhd fsdi isdj sjdj dk spkd s</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>39.99
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">SHOP 365</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/037/0/7972092/il_570xN.605326585_sfzm.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:70%;"></div></div>
                                    <div class="review-num">23 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">rhs hdhs hshd h sds sda</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>224.74
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">CARTER GOU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>3235K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>842</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="http://thefancy-media-ec5.thefancy.com/310/20140110/536673437096089618_bdfc848dfbdc.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:95%;"></div></div>
                                    <div class="review-num">7 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">T-shirt hds shs dah shsd dshas d</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>25
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">DDR CHAMPS</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>5K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>42</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/017/1/5322868/il_224xN.475417888_sfzn.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:86%;"></div></div>
                                    <div class="review-num">76 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Beautiful care rice dd cook</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>37.8
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">CCURE 54 GOU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>35K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>42</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/042/0/6122716/il_570xN.618992274_48ry.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:60%;"></div></div>
                                    <div class="review-num">74 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">SO OS jdj Beautiful care rice dd cook</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>65
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">VIMEO FFR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>935K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>402</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>

                    <div class="controls">
                        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
                    </div>
                </div>

            </div>
        </div>
        <!-- CONTENT END -->

