<!-- CONTENT BEGIN -->
        <div id="content" class="sidebar_right">
            <div class="inner">
                <div class="block_general_title_1">
                    <h1><?php if($this->searchKey!='') echo $this->searchKey.' <i class="fa fa-caret-right"></i> '; echo $this->mainTitle; ?></h1>
                    <h2><?php echo $this->subTitle; ?></h2>
                </div>

                <div class="main_content">
                    <?php if($this->searchForm==1): ?>
                    <div class="block_search_page">
                        <div class="form">
                            <form action="#">
                                <div class="field"><input type="text"></div>
                                <div class="button"><a href="#" class="general_button_type_3 submit">Search</a></div>
                            </form>
                        </div>

                        <div class="text">
                            <p>If you're not happy with the results, please do another search</p>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="block_posts type_6">
                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">Sdfd4ARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_1"></div>
                    </div>

                    <div class="separator" style="height:43px;"></div>

                    <div class="block_pager_1">
                        <ul>
                            <li class="current"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#" class="next">Next</a></li>
                        </ul>

                        <div class="info">Page 1 of 3</div>

                        <div class="clearboth"></div>
                    </div>
                </div>

                <div class="sidebar">
                    <aside>
                        <div class="sidebar_title_1">Social</div>
                        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=744520785567641" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:30px;" allowTransparency="true"></iframe>
                        <!--<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=standard&amp;action=recommend&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=744520785567641" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;" allowTransparency="true"></iframe>-->
<iframe allowtransparency="true" frameborder="0" scrolling="no"
        src="https://platform.twitter.com/widgets/tweet_button.html"
        style="width:130px; height:20px;"></iframe>
        <a href="http://www.pinterest.com/pin/create/button/
        ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
        &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
        &description=Next%20stop%3A%20Pinterest"
        data-pin-do="buttonPin"
        data-pin-config="above">
        <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
    </a>

    <script src="https://apis.google.com/js/plusone.js"></script>
<g:plus action="share"></g:plus>
                        <!--<div class="block_sidebar_social">
                            <div class="row fb">
                                <div class="text">25,750 Fans</div>

                                <div class="button"><a href="#">Like</a></div>
                            </div>

                            <div class="row tw">
                                <div class="text">16,321 Followers</div>

                                <div class="button"><a href="#">Follow</a></div>
                            </div>

                            <div class="row rss">
                                <div class="text">56,124 Subscribers</div>

                                <div class="button"><a href="#">Subscribers</a></div>
                            </div>
                        </div>-->
                    </aside>

                    <aside>
                        <div class="sidebar_title_1"><a href="" class="brand-name">Clark Cordon</a>'s favorite items</div>
                        <!--<div class="brand-other"><a href="#"><img src="https://img0.etsystatic.com/044/0/5185722/iusa_75x75.26999598_6o50.jpg" alt=""></a></div>-->
                        <div class="other-item">

                        <article class="brand-other product f-left">
                            <div class="feature">
                                <div class="image">
                                    <a brand="CREW JJ1 cbb" likes="111" collect="124" title="Premium Jeans toward to the top world" price="124.23" class="fancy2" rel="fancy2" href="https://img1.etsystatic.com/041/0/6416669/il_570xN.587135873_p4mx.jpg"><img alt="BRAND name is CSI" title="BRAND name is CSI" src="https://img1.etsystatic.com/041/0/6416669/il_570xN.587135873_p4mx.jpg"></a>
                                </div>
                            </div>
                            <!--<div class="content">
                                <div class="title">
                                    <a href="">Ejhd fsdi isdj sjdj dk spkd s</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>39.99
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes">335K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                    </div>
                                </div>
                            </div>-->
                        </article>
                        <article class="brand-other product f-left">
                            <div class="feature">
                                <div class="image">
                                    <a brand="CREW JJ1 cbb" likes="111" collect="124" title="Premium Jeans toward to the top world" price="124.23" class="fancy2" rel="fancy2" href="//resize-ec1.thefancy.com/resize/crop/313/thingd/default/668048234370832136_af3cd9d0be32.jpg"><img alt="BRAND name is CSI" title="BRAND name is CSI" src="//resize-ec1.thefancy.com/resize/crop/313/thingd/default/668048234370832136_af3cd9d0be32.jpg"></a>
                                </div>
                            </div>
                        </article>
                        <article class="brand-other product f-left">
                            <div class="feature">
                                <div class="image">
                                    <a brand="CREW JJ1 cbb" likes="111" collect="124" title="Premium Jeans toward to the top world" price="124.23" class="fancy2" rel="fancy2" href="https://img1.etsystatic.com/037/0/7972092/il_570xN.605326585_sfzm.jpg"><img alt="BRAND name is CSI" title="BRAND name is CSI" src="https://img1.etsystatic.com/037/0/7972092/il_570xN.605326585_sfzm.jpg"></a>
                                </div>
                            </div>
                        </article>
                        <article class="brand-other product f-left">
                            <div class="feature">
                                <div class="image">
                                    <a brand="CREW JJ1 cbb" likes="111" collect="124" title="Premium Jeans toward to the top world" price="124.23" class="fancy2" rel="fancy2" href="https://img1.etsystatic.com/027/1/7789766/il_340x270.628067245_abm6.jpg"><img alt="BRAND name is CSI" title="BRAND name is CSI" src="https://img1.etsystatic.com/027/1/7789766/il_340x270.628067245_abm6.jpg"></a>
                                </div>
                            </div>
                        </article>
                        <article class="brand-other product f-left">
                            <div class="feature">
                                <div class="image">
                                    <a brand="CREW JJ1 cbb" likes="111" collect="124" title="Premium Jeans toward to the top world" price="124.23" class="fancy2" rel="fancy2" href="https://img1.etsystatic.com/025/0/8925614/il_570xN.543457281_tgit.jpg"><img alt="BRAND name is CSI" title="BRAND name is CSI" src="https://img1.etsystatic.com/025/0/8925614/il_570xN.543457281_tgit.jpg"></a>
                                </div>
                            </div>
                        </article>
                        <article class="brand-other product f-left">
                            <div class="feature">
                                <div class="image">
                                    <a brand="CREW JJ1 cbb" likes="111" collect="124" title="Premium Jeans toward to the top world" price="124.23" class="fancy2" rel="fancy2" href="https://img1.etsystatic.com/039/0/5631970/il_300x300.631159389_4kpi.jpg"><img alt="BRAND name is CSI" title="BRAND name is CSI" src="https://img1.etsystatic.com/039/0/5631970/il_300x300.631159389_4kpi.jpg"></a>
                                </div>
                            </div>
                        </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Popular Celebs of <a href="" class="brand-name">Clark Cordon</a></div>

                        <div class="block_sidebar_popular_posts celeb-brand">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">CSLEB name here</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                        <!--<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
                                        <div class="date">27, 2013</div>-->
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="https://img0.etsystatic.com/000/0/15554424/iusa_75x75.9371160.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">CSLEB name here Anskjdhf sjd199</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                        <!--<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
                                        <div class="date">27, 2013</div>-->
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="https://img0.etsystatic.com/044/0/5185722/iusa_75x75.26999598_6o50.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Erhfu ds 11</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                        <!--<div class="tags"><a href="#">TRAVEL</a>, <a href="#">LIFE</a></div>
                                        <div class="date">27, 2013</div>-->
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Most Commented</div>
                        <div class="block_sidebar_latest_comments">
                            <article>
                                <div class="num">1</div>
                                <div class="content">
                                    <div class="author"><a href=""><span>Mark Summers</span></a> says: <span class="time-ago">10 hours ago</span></div>
                                    <div class="comment">Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</div>
                                    <div class="comment-info">
                                        <span class="disagree"><i class="fa fa-thumbs-down"></i>243</span>
                                        <span class="agree"><i class="fa fa-thumbs-up"></i>243</span>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="num">2</div>
                                <div class="content">
                                    <div class="author"><a href=""><span>Sara hornet</span></a> says:<span class="time-ago">1 day ago</span></div>
                                    <div class="comment">Magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                                    <div class="comment-info">
                                        <span class="disagree"><i class="fa fa-thumbs-down"></i>243</span>
                                        <span class="agree"><i class="fa fa-thumbs-up"></i>243</span>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <!--<div class="block_sidebar_most_commented">
                            <article>
                                <div class="title">Quae ab illo inventore veritatis et quasi.</div>
                                <div class="comment-info">
                                    <span class="date">Sept 24, 2013</span>
                                    <span class="disagree"><i class="fa fa-thumbs-down"></i>243</span>
                                    <span class="agree"><i class="fa fa-thumbs-up"></i>243</span>
                                </div>
                            </article>

                            <article>
                                <div class="title">Fugit, sed quia consequuntur. Fugit, sed quia consequuntur. </div>
                                <div class="comment-info">
                                    <span class="date">Sept 24, 2013</span>
                                    <span class="disagree"><i class="fa fa-thumbs-down"></i>243</span>
                                    <span class="agree"><i class="fa fa-thumbs-up"></i>243</span>
                                </div>
                            </article>

                            <article>
                                <div class="title">Sed quia consequuntur magni dolores eos qui Sed quia consequuntur magni dolores eos qui Sed quia consequuntur magni dolores eos qui Sed quia consequuntur magni dolores eos qui.</div>
                                <div class="comment-info">
                                    <span class="date">Sept 24, 2013</span>
                                    <span class="disagree"><i class="fa fa-thumbs-down"></i>243</span>
                                    <span class="agree"><i class="fa fa-thumbs-up"></i>243</span>
                                </div>
                            </article>
                        </div>-->
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Latest Comments</div>

                        <div class="block_sidebar_latest_comments">
                            <article>
                                <div class="num">1</div>
                                <div class="content">
                                    <div class="author"><a href=""><span>Mark Summers</span></a> says: <span class="time-ago">10 hours ago</span></div>
                                    <div class="comment">Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</div>
                                </div>
                            </article>

                            <article>
                                <div class="num">2</div>
                                <div class="content">
                                    <div class="author"><a href=""><span>Sara hornet</span></a> says:<span class="time-ago">1 day ago</span></div>
                                    <div class="comment">Magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <!--<aside>
                        <div class="sidebar_title_1">Most commented</div>

                        <div class="block_sidebar_most_commented">
                            <article>
                                <a href="#">
                                    <span class="num">12<span class="tail"></span></span>
                                    <span class="title">Quae ab illo inventore veritatis et quasi.</span>
                                </a>
                            </article>

                            <article>
                                <a href="#">
                                    <span class="num">8<span class="tail"></span></span>
                                    <span class="title">Fugit, sed quia consequuntur.</span>
                                </a>
                            </article>

                            <article>
                                <a href="#">
                                    <span class="num">7<span class="tail"></span></span>
                                    <span class="title">Sed quia consequuntur magni dolores eos qui.</span>
                                </a>
                            </article>
                        </div>
                    </aside>-->

                    <aside>
                        <div class="sidebar_title_1">Events - <a class="event" href="">more</a></div>

                        <div class="block_sidebar_latest_comments">
                            <article>
                                <div class="num">1</div>
                                <div class="content">
                                    <div class="author"><a href><span>Mark Summers</span></a></div>
                                    <div class="comment">Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</div>
                                </div>
                            </article>
                            <article>
                                <div class="num">2</div>
                                <div class="content">
                                    <div class="author"><a href><span>Sara hornet</span></a></div>
                                    <div class="comment">Magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                                </div>
                            </article>
                        </div>
                    </aside>

                </div>

                <div class="clearboth"></div>

            </div>
        </div>
        <!-- CONTENT END -->