<!-- CONTENT BEGIN -->
        <div id="content" class="sidebar_right">
            <div class="inner">
                <div class="block_slider_type_2 general_not_loaded">
                    <div id="slider" class="slider flexslider">
                        <ul class="slides">
                            <li>
                                <div class="caption type_1">
                                    <img src="http://enterprise-html.weblionmedia.com/images/pic_slider_2_2_1.jpg" alt="">
                                    <div class="text_1_1">Travel, PHOTOGRAPHY</div>
                                    <div class="text_1_2">Mountains in Switzerland</div>
                                    <div class="text_1_3"><a href="#" class="general_button_type_1">Read More</a></div>
                                </div>

                                <div class="caption type_2">
                                    <img src="http://enterprise-html.weblionmedia.com/images/pic_slider_2_2_2.jpg" alt="">
                                    <div class="text_2_1">Travel, PHOTOGRAPHY</div>
                                    <div class="text_2_2">My love - new york</div>
                                    <div class="text_2_3"><a href="#" class="general_button_type_2">Read More</a></div>
                                </div>

                                <div class="caption type_3">
                                    <img src="http://enterprise-html.weblionmedia.com/images/pic_slider_2_2_3.jpg" alt="">
                                    <div class="text_3_1">Life, PHOTOGRAPHY</div>
                                    <div class="text_3_2">My Awesome workplace</div>
                                    <div class="text_3_3"><a href="#" class="general_button_type_2">Read More</a></div>
                                </div>
                            </li>

                            <li>
                                <div class="caption type_1">
                                    <img src="http://enterprise-html.weblionmedia.com/images/pic_slider_2_2_1.jpg" alt="">
                                    <div class="text_1_1">Life, PHOTOGRAPHY</div>
                                    <div class="text_1_2">Flying over the mountains</div>
                                    <div class="text_1_3"><a href="#" class="general_button_type_1">Read More</a></div>
                                </div>

                                <div class="caption type_2">
                                    <img src="http://enterprise-html.weblionmedia.com/images/pic_slider_2_2_2.jpg" alt="">
                                    <div class="text_2_1">Travel, PHOTOGRAPHY</div>
                                    <div class="text_2_2">Young Businessman</div>
                                    <div class="text_2_3"><a href="#" class="general_button_type_2">Read More</a></div>
                                </div>

                                <div class="caption type_3">
                                    <img src="http://enterprise-html.weblionmedia.com/images/pic_slider_2_2_3.jpg" alt="">
                                    <div class="text_3_1">People, Life</div>
                                    <div class="text_3_2">The Guy on the field</div>
                                    <div class="text_3_3"><a href="#" class="general_button_type_2">Read More</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <script type="text/javascript">
                        jQuery(function() {
                            init_slider_2('#slider');
                        });
                    </script>
                </div>

                <div id="title-today" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="sel-title">Today's brands</h1>
                    <h2 id="sel-subtitle">Unique brands here! Find your style and Enjoy!</h2>
                    <div class="intro-filter">
                        <a onclick="introBrands('0')" class="general_colored_button intro-b0" title="Today's brands">Today</a>
                        <a onclick="introBrands('1')" class="general_colored_button intro-b1" title="Favorite brands">FAVORITE</a>
                        <a onclick="introBrands('2')" class="general_colored_button intro-b2" title="New brands">NEW</a>
                        <a onclick="allIntroBrands()" class="general_colored_button open-all" title="Open or Close All Introductions">OPEN ALL</a>
                    </div>
                </div>

                <div id="content-today" class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                        <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                        <a href="#">Photography</a>
                                        <a href="#">Photography</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/023/0/5495810/il_170x135.570828173_iqq5.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/026/0/6298498/il_170x135.564434267_to15.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/039/1/5495810/il_170x135.535951909_ml0p.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/044/2/7259782/il_170x135.577550623_7c0w.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">indigo</a>
                                        <a href="#" title="Jeans">distiction</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>


                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img0.etsystatic.com/000/0/5676972/il_170x135.267997588.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/000/0/5676972/il_170x135.284793663.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/000/0/5676972/il_170x135.284241821.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/042/0/5676972/il_170x135.644062835_r8s8.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/036/1/7025903/il_170x135.647572383_2saz.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/028/0/7025903/il_170x135.630368567_1l21.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/039/1/7025903/il_170x135.587002268_stoj.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/025/1/7025903/il_170x135.616900680_mhj3.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/002/0/22747148/iusa_75x75.20241743_t7ra.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="title-new" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="">New brands</h1>
                    <h2 id="">Additional brands come to us!</h2>
                </div>

                <div id="content-new" class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img0.etsystatic.com/022/0/6241621/il_170x135.507282562_1ca5.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/040/0/7430397/il_170x135.574860430_evsc.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/027/0/9822616/il_170x135.643568244_kr7b.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/000/0/5755408/il_170x135.294967019.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/000/0/17267750/iusa_75x75.9073801.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                        <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                        <a href="#">Photography</a>
                                        <a href="#">Photography</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/008/1/7100318/il_170x135.406307069_4med.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/033/0/5795925/il_170x135.545607192_9ouf.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/037/1/8374594/il_170x135.561758888_k22z.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/021/1/7662141/il_170x135.554901321_1q0v.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/007/0/21609045/iusa_75x75.20863973_d4xc.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">indigo</a>
                                        <a href="#" title="Jeans">distiction</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>


                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/019/0/5682053/il_170x135.496607621_7ivw.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/024/0/6664244/il_170x135.486323550_sj3c.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/030/0/6664244/il_170x135.515037795_dn2r.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/004/0/6664244/il_170x135.359520566_qlpu.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/000/0/8272863/iusa_75x75.6567076.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img0.etsystatic.com/039/1/5837037/il_170x135.564083004_pf3s.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/013/0/6854097/il_170x135.442628584_evdg.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/012/1/5837037/il_170x135.453374534_tmcy.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/009/1/5837037/il_170x135.438357808_lay1.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/000/0/17819425/iusa_75x75.20056104.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="title-favorite" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="">Favorite brands</h1>
                    <h2 id="">Popular brands here!</h2>
                </div>

                <div id="content-favorite" class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/040/1/7800541/il_170x135.572535087_amjw.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/038/0/7800541/il_170x135.560662467_d5ev.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/038/0/7800541/il_170x135.586953507_tj5b.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/034/0/7800541/il_170x135.607889498_qtoi.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/027/0/7800541/isc_190x190.5212614365_ort4.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                        <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                        <a href="#">Photography</a>
                                        <a href="#">Photography</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/041/2/8040395/il_170x135.542730657_5nsf.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/021/0/8040395/il_170x135.540674409_m0gx.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/019/0/8040395/il_170x135.540339077_pr4y.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/019/0/8040395/il_170x135.486960133_jpbq.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/009/0/32491922/iusa_75x75.22734352_p610.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">indigo</a>
                                        <a href="#" title="Jeans">distiction</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>


                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img0.etsystatic.com/030/0/5439978/il_170x135.619915010_54v3.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/5439978/il_170x135.129935496.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/024/0/5439978/il_170x135.475003470_7kzs.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/032/0/5439978/il_170x135.620711215_asnt.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/000/0/8315140/iusa_75x75.7693610.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/038/0/7222521/il_170x135.550374763_4azz.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/018/0/7222521/il_170x135.497727234_d2cy.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/027/1/6524871/il_170x135.601638444_lcwc.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/035/0/8082185/il_170x135.505932036_upkg.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/000/0/22097182/iusa_75x75.10034861.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div class="block_general_title_1 w_margin_1 brand-filter">
                    <h1>All Brands</h1>
                    <h2>Find brands for your style</h2>
                    <div class="intro-filter">
                        <a onclick="toggleCategory()" class="general_colored_button" title="">CATEGORY</a>
                        <a onclick="toggleSearch()" class="general_colored_button" title="">SEARCH BRANDS</a>
                        <div id="searchCategory" class="searchCategory" style="display:none">
                            <a href="<?php echo URL.'brands/favorite'; ?>" class="general_colored_button intro-b0" title="All">ALL</a>
                            <?php global $HOMEMENU;
                                foreach ($HOMEMENU['category'] as $key => $menu){
                                    $tempMenu = $key;
                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                    //echo '<a href="'.URL.'brands/favorite/'.$key.'" class="general_colored_button intro-b1" title="'.$tempMenu.'">'.$tempMenu.'</a>';
                                    echo '<a href="'.URL.'brands/favorite/'.$key.'" class="general_colored_button" title="'.$tempMenu.'">'.$tempMenu.'</a>';
                                }
                            ?>
                        </div>
                        <div id="searchBar" class="searchBar" style="display:none">
                            <a onclick="searchBrands('a')" class="general_colored_button" title="">A</a>
                            <a onclick="searchBrands('b')" class="general_colored_button" title="">B</a>
                            <a onclick="searchBrands('c')" class="general_colored_button" title="">C</a>
                            <a onclick="searchBrands('d')" class="general_colored_button" title="">D</a>
                            <a onclick="searchBrands('e')" class="general_colored_button" title="">E</a>
                            <a onclick="searchBrands('f')" class="general_colored_button" title="">F</a>
                            <a onclick="searchBrands('g')" class="general_colored_button" title="">G</a>
                            <a onclick="searchBrands('h')" class="general_colored_button" title="">H</a>
                            <a onclick="searchBrands('i')" class="general_colored_button" title="">I</a>
                            <a onclick="searchBrands('j')" class="general_colored_button" title="">J</a>
                            <a onclick="searchBrands('k')" class="general_colored_button" title="">K</a>
                            <a onclick="searchBrands('l')" class="general_colored_button" title="">L</a>
                            <a onclick="searchBrands('m')" class="general_colored_button" title="">M</a>
                            <a onclick="searchBrands('n')" class="general_colored_button" title="">N</a>
                            <a onclick="searchBrands('o')" class="general_colored_button" title="">O</a>
                            <a onclick="searchBrands('p')" class="general_colored_button" title="">P</a>
                            <a onclick="searchBrands('q')" class="general_colored_button" title="">Q</a>
                            <a onclick="searchBrands('r')" class="general_colored_button" title="">R</a>
                            <a onclick="searchBrands('s')" class="general_colored_button" title="">S</a>
                            <a onclick="searchBrands('t')" class="general_colored_button" title="">T</a>
                            <a onclick="searchBrands('u')" class="general_colored_button" title="">U</a>
                            <a onclick="searchBrands('v')" class="general_colored_button" title="">V</a>
                            <a onclick="searchBrands('w')" class="general_colored_button" title="">W</a>
                            <a onclick="searchBrands('x')" class="general_colored_button" title="">X</a>
                            <a onclick="searchBrands('y')" class="general_colored_button" title="">Y</a>
                            <a onclick="searchBrands('z')" class="general_colored_button" title="">Z</a>
                            <a onclick="searchBrands('etc')" class="general_colored_button" title="">ETC</a>
                            <a onclick="searchBrands('fav')" class="general_colored_button d-color" title="">FAVORITE</a>
                            <a onclick="searchBrands('new')" class="general_colored_button d-color" title="">NEW</a>
                            <a onclick="searchBrands('eve')" class="general_colored_button d-color" title="">EVENT</a>
                        </div>
                    </div>
                </div>

                <div class="main_content">
                    <div class="block_posts type_6 brands">
                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                            <a href="#" title="Premium jeans">indigo</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_1"></div>
                    </div>

                    <div class="separator" style="height:43px;"></div>

                    <div class="block_pager_1">
                        <ul>
                            <li class="current"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="skip">...</li>
                            <li><a href="#">7</a></li>
                            <li><a href="#" class="next">Next</a></li>
                        </ul>

                        <div class="info">Page 1 of 7</div>

                        <div class="clearboth"></div>
                    </div>
                </div>

                <div class="sidebar">
                    <aside>
                        <div class="sidebar_title_1">Social</div>
                        <div class="block_sidebar_social">
                            <div class="row fb">
                                <div class="text">25,750 Fans</div>

                                <div class="button"><a href="#">Like</a></div>
                            </div>

                            <div class="row tw">
                                <div class="text">16,321 Followers</div>

                                <div class="button"><a href="#">Follow</a></div>
                            </div>

                            <div class="row rss">
                                <div class="text">56,124 Subscribers</div>

                                <div class="button"><a href="#">Subscribers</a></div>
                            </div>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Brands of the month</div>

                        <div class="block_sidebar_popular_posts">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Special Sale Information</div>

                        <div class="block_sidebar_popular_posts">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Events - <a class="event" href="<?php echo URL.'brands/event';?>">more</a></div>

                        <div class="block_sidebar_latest_comments">
                            <article>
                                <div class="num">1</div>
                                <div class="content">
                                    <div class="author"><a href><span>Mark Summers</span></a></div>
                                    <div class="comment">Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</div>
                                </div>
                            </article>
                            <article>
                                <div class="num">2</div>
                                <div class="content">
                                    <div class="author"><a href><span>Sara hornet</span></a></div>
                                    <div class="comment">Magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                                </div>
                            </article>
                        </div>
                    </aside>

                </div>

                <div class="clearboth"></div>

            </div>
        </div>