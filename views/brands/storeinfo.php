<!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">
                <div class="block_general_title_1">
                    <h1><?php echo $this->mainTitle; ?></h1>
                    <h2><?php echo $this->subTitle; ?></h2>
                </div>

                <div class="block_content">
                    <div class="pic"><img src="http://enterprise-html.weblionmedia.com/images/pic_1_1.jpg" alt=""></div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.</p>
                    <p>Qnde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                    <p>Sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet onsectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.</p>

                    <div class="line_1"></div>
                </div>

                <div class="block_info_1">
                    <div class="social brand-sns">
                        <div class="title"><span>Visit Brand SNS</span></div>

                        <ul class="general_social_3">
                            <li><a href="#" class="social_1">Twitter</a></li>
                            <li><a href="#" class="social_2">Facebook</a></li>
                            <li><a href="#" class="social_3">Pinterest</a></li>
                            <li><a href="#" class="social_4">Google Plus</a></li>
                            <li><a href="#" class="social_5">Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->