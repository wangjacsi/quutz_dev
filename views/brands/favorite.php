<!-- CONTENT BEGIN -->
        <div id="content" class="sidebar_right" style="padding-top: 0px;">
            <div class="inner">
                <div id="title-today" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="sel-title"><?php if($this->category!='') echo $this->category.' - ';
                        echo $this->mainTitle; ?></h1>
                    <h2 id="sel-subtitle"><?php echo $this->subTitle; ?></h2>
                    <div class="intro-filter favorite">
                        <a href="<?php echo URL.'brands/favorite'; ?>" class="general_colored_button intro-b0" title="All">All</a>
                        <?php global $HOMEMENU;
                            foreach ($HOMEMENU['category'] as $key => $menu){
                                $tempMenu = $key;
                                $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                echo '<a href="'.URL.'brands/favorite/'.$key.'" class="general_colored_button" title="'.$tempMenu.'">'.$tempMenu.'</a>';
                            }
                        ?>
                        </div>
                </div>

                <div class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                        <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                        <a href="#">Photography</a>
                                        <a href="#">Photography</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/023/0/5495810/il_170x135.570828173_iqq5.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/026/0/6298498/il_170x135.564434267_to15.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/039/1/5495810/il_170x135.535951909_ml0p.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/044/2/7259782/il_170x135.577550623_7c0w.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">indigo</a>
                                        <a href="#" title="Jeans">distiction</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img0.etsystatic.com/000/0/5676972/il_170x135.267997588.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/000/0/5676972/il_170x135.284793663.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img1.etsystatic.com/000/0/5676972/il_170x135.284241821.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img1.etsystatic.com/042/0/5676972/il_170x135.644062835_r8s8.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/036/1/7025903/il_170x135.647572383_2saz.jpg" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="https://img1.etsystatic.com/028/0/7025903/il_170x135.630368567_1l21.jpg" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/039/1/7025903/il_170x135.587002268_stoj.jpg" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/025/1/7025903/il_170x135.616900680_mhj3.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="https://img1.etsystatic.com/002/0/22747148/iusa_75x75.20241743_t7ra.jpg" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div class="block_general_title_1 w_margin_1 brand-filter">
                    <h1><?php echo $this->mainTitle2; ?></h1>
                    <h2><?php echo $this->subTitle2; ?></h2>
                </div>

                <div class="main_content">
                    <div class="block_posts type_6 brands">
                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                            <a href="#" title="Premium jeans">indigo</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_5">
                            <div class="feature pic4">
                                <div class="image">
                                    <a href=""><img class="radius-lt" src="https://img1.etsystatic.com/020/0/6779482/il_570xN.487898583_msoj.jpg" alt=""></a>
                                    <a class="m-left2" href=""><img class="radius-rt" src="https://img0.etsystatic.com/000/0/6218975/il_340x270.336094756.jpg" alt=""></a>
                                    <a class="m-top2" href=""><img class="radius-lb" src="https://img0.etsystatic.com/013/1/6366366/il_340x270.425379832_b6n0.jpg" alt=""></a>
                                    <a class="m-top2 m-left2" href=""><img class="radius-rb" src="https://img0.etsystatic.com/022/0/5245250/il_340x270.553537610_m751.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar2">
                                        <a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="tags"><strong><a href="#" class="font-color1">17 EddDF</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                        </div>
                                        <div class="tag-info">
                                            <a href="#" title="Premium jeans">indigo</a>
                                            <a href="#" title="Jeans">distiction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur. Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_1"></div>
                    </div>

                    <div class="separator" style="height:43px;"></div>

                    <div class="block_pager_1">
                        <ul>
                            <li class="current"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="skip">...</li>
                            <li><a href="#">7</a></li>
                            <li><a href="#" class="next">Next</a></li>
                        </ul>

                        <div class="info">Page 1 of 7</div>

                        <div class="clearboth"></div>
                    </div>
                </div>

                <div class="sidebar">
                    <aside>
                        <div class="sidebar_title_1">Social</div>
                        <div class="block_sidebar_social">
                            <div class="row fb">
                                <div class="text">25,750 Fans</div>

                                <div class="button"><a href="#">Like</a></div>
                            </div>

                            <div class="row tw">
                                <div class="text">16,321 Followers</div>

                                <div class="button"><a href="#">Follow</a></div>
                            </div>

                            <div class="row rss">
                                <div class="text">56,124 Subscribers</div>

                                <div class="button"><a href="#">Subscribers</a></div>
                            </div>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Brands of the month</div>

                        <div class="block_sidebar_popular_posts">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Special Sale Information</div>

                        <div class="block_sidebar_popular_posts">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Events - <a class="event" href="">more</a></div>

                        <div class="block_sidebar_latest_comments">
                            <article>
                                <div class="num">1</div>
                                <div class="content">
                                    <div class="author"><a href><span>Mark Summers</span></a></div>
                                    <div class="comment">Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</div>
                                </div>
                            </article>
                            <article>
                                <div class="num">2</div>
                                <div class="content">
                                    <div class="author"><a href><span>Sara hornet</span></a></div>
                                    <div class="comment">Magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                                </div>
                            </article>
                        </div>
                    </aside>
                </div>

                <div class="clearboth"></div>

            </div>
        </div>