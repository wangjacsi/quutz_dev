<!-- CONTENT BEGIN -->
        <div id="content" class="sidebar_right">
            <div class="inner">
                <div class="block_general_title_1">
                    <h1><?php if($this->searchKey!='') echo $this->searchKey.' <i class="fa fa-caret-right"></i> '; echo $this->mainTitle; ?></h1>
                    <h2><?php echo $this->subTitle; ?></h2>
                </div>

                <div class="main_content">
                    <?php if($this->searchForm==1): ?>
                    <div class="block_search_page">
                        <div class="form">
                            <form action="#">
                                <div class="field"><input type="text"></div>
                                <div class="button"><a href="#" class="general_button_type_3 submit">Search</a></div>
                            </form>
                        </div>

                        <div class="text">
                            <p>If you're not happy with the results, please do another search</p>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="block_posts type_6">
                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">Sdfd4ARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_2"></div>

                        <article class="post_type_6">
                            <div class="feature search">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/000/0/8236526/iusa_75x75.8108114.jpg" alt=""></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="author"><a href="#">SARA FOX</a></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Sed ut perspiciatis unde omnis iste natus sit volup.</a>
                                </div>
                                <div class="text">
                                    <p>Magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum.</p>
                                </div>
                            </div>
                        </article>
                        <div class="line_1"></div>
                    </div>

                    <div class="separator" style="height:43px;"></div>

                    <div class="block_pager_1">
                        <ul>
                            <li class="current"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#" class="next">Next</a></li>
                        </ul>

                        <div class="info">Page 1 of 3</div>

                        <div class="clearboth"></div>
                    </div>
                </div>

                <div class="sidebar">
                    <aside>
                        <div class="sidebar_title_1">Social</div>
                        <div class="block_sidebar_social">
                            <div class="row fb">
                                <div class="text">25,750 Fans</div>

                                <div class="button"><a href="#">Like</a></div>
                            </div>

                            <div class="row tw">
                                <div class="text">16,321 Followers</div>

                                <div class="button"><a href="#">Follow</a></div>
                            </div>

                            <div class="row rss">
                                <div class="text">56,124 Subscribers</div>

                                <div class="button"><a href="#">Subscribers</a></div>
                            </div>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Brands of the month</div>

                        <div class="block_sidebar_popular_posts">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>

                    <aside>
                        <div class="sidebar_title_1">Special Sale Information</div>

                        <div class="block_sidebar_popular_posts">
                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="image"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">Quae ab illo inventore veritatis et quasi.</a>
                                    </div>
                                    <div class="info">
                                        <div class="stats show-stat">
                                            <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </aside>


                    <!--<aside>
                        <div class="sidebar_title_1">Most commented</div>

                        <div class="block_sidebar_most_commented">
                            <article>
                                <a href="#">
                                    <span class="num">12<span class="tail"></span></span>
                                    <span class="title">Quae ab illo inventore veritatis et quasi.</span>
                                </a>
                            </article>

                            <article>
                                <a href="#">
                                    <span class="num">8<span class="tail"></span></span>
                                    <span class="title">Fugit, sed quia consequuntur.</span>
                                </a>
                            </article>

                            <article>
                                <a href="#">
                                    <span class="num">7<span class="tail"></span></span>
                                    <span class="title">Sed quia consequuntur magni dolores eos qui.</span>
                                </a>
                            </article>
                        </div>
                    </aside>-->

                    <aside>
                        <div class="sidebar_title_1">Events - <a class="event" href="">more</a></div>

                        <div class="block_sidebar_latest_comments">
                            <article>
                                <div class="num">1</div>
                                <div class="content">
                                    <div class="author"><a href><span>Mark Summers</span></a></div>
                                    <div class="comment">Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</div>
                                </div>
                            </article>
                            <article>
                                <div class="num">2</div>
                                <div class="content">
                                    <div class="author"><a href><span>Sara hornet</span></a></div>
                                    <div class="comment">Magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                                </div>
                            </article>
                        </div>
                    </aside>

                </div>

                <div class="clearboth"></div>

            </div>
        </div>
        <!-- CONTENT END -->