var setInd1 = 0;
var brandsTitle;

$(document).ready(function() {
    $('#content-hot').css('display', 'block');
    $('.main-cat').css('display','none');

    var celeb='', likes=0, collect=0, brand='', title='', price=0;

    if($('a.fancy2').length >0){
        $('a.fancy2').attr('rel', 'gallery').fancybox({
            padding : 5,
            margin      : [20, 60, 20, 60],
            openEffect  : 'none',
            closeEffect : 'none',
            beforeLoad: function() {
                celeb = $(this.element).attr('celeb');
                likes = $(this.element).attr('likes');
                collect = $(this.element).attr('collect');
                brand = $(this.element).attr('brand');
                title = $(this.element).attr('title');
                price = $(this.element).attr('price');
            },
            afterShow: function() { //mouse over after show the title
                $('<div class="product"><div class="user-action"><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>'+
                  '<a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>'+
                  '<a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>'+
                  '</div><div class="cart"><a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>'+
                '</div></div>').appendTo(this.inner);

                $(".fancybox-title").wrapInner('<div />').show();

                $(".fancybox-wrap").hover(function() {
                    $(".fancybox-title").show();
                    //$(".expander").show();
                    $(".product").show();
                }, function() {
                    $(".fancybox-title").hide();
                    //$(".expander").hide();
                    $(".product").hide();
                });
            },
            afterLoad: function() {
                if (this.title) {
                    this.title = '<div class="product"><div class="title"><a href="">'+title+'</a>'+
                                '</div><div class="price"><i class="fa fa-dollar"></i>'+price+'</div><div class="f-clear"></div>'+
                                '<div class="info"><div class="tags">BRAND <strong><a href="#" class="font-color1">'+brand+'</a></strong></div>'+
                    '<div class="stats show-stat"><div class="likes" title="Likes"><i class="fa fa-heart"></i>'+likes+'</div>'+
                    '<div class="comments" title="Collections"><i class="fa fa-cube"></i>'+collect+'</div>'+
                    '</div></div></div>';

                    this.title += '<div class="social"><ul class="general_social_2">'+
                                '<li><a href="#" class="social_1">Twitter</a></li>'+
                                '<li><a href="#" class="social_2">Facebook</a></li>'+
                                '<li><a href="#" class="social_3">Pinterest</a></li>'+
                                '<li><a href="#" class="social_4">Google Plus</a></li>'+
                                '<li><a href="#" class="social_5">Instagram</a></li>'+
                            '</ul></div>';
                }
            },
            beforeShow: function () {

            },
            beforeClose: function(){

            },
            afterClose: function() {
                $('#slider article').css('display', 'block');
                $('#post_slider_1 .slider .slides li').css('display', 'block');
                $('.isotope article').css('display', 'block');
            },
            helpers : {
                title: {
                    type: 'inside'
                }
            }
        });
    };

});

function introBrands(ind){
    if(setInd1==0){
        titles = setBrandsPageVar(2);
        setInd1 = 1;
    }
    if(ind==0){
        $('.intro-b0').css('background-color', color1);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color2);
        $('#content-hot').css('display', 'block');
        $('#content-new').css('display', 'none');
        $('#content-celebs').css('display', 'none');
        $('#sel-title').html(titles.hot[0]);
        $('#sel-subtitle').html(titles.hot[1]);
    } else if(ind==1){
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color1);
        $('.intro-b2').css('background-color', color2);
        $('#content-hot').css('display', 'none');
        $('#content-new').css('display', 'block');
        $('#content-celebs').css('display', 'none');
        $('#sel-title').html(titles.newitems[0]);
        $('#sel-subtitle').html(titles.newitems[1]);
    } else if(ind==2){
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color1);
        $('#content-hot').css('display', 'none');
        $('#content-new').css('display', 'none');
        $('#content-celebs').css('display', 'block');
        $('#sel-title').html(titles.celebs[0]);
        $('#sel-subtitle').html(titles.celebs[1]);
    }
}

function showBrandCategory(){
    if($('.main-cat').css('display') == 'inline-block'){
        $('.main-cat').css('display','none');
        $('.sub-cat').css('display','inline-block');
        $('#categoryBtn').html('Sub Category');
    } else {
        $('.main-cat').css('display', 'inline-block');
        $('.sub-cat').css('display','none');
        $('#categoryBtn').html('Main Category');
    }
}

function brandSubMenu(ind){
    alert(ind);
    $('.brand-menu .'+ind).css('color','#969696');
}