var setInd1 = 0;
var brandsTitle;
function introBrands(ind){
    //alert(ind);
    if(setInd1==0){
        brandsTitle = setBrandsPageVar(1);
        setInd1 = 1;
    }
    //debugObject(brandsTitle);
    if(ind==0){
        $('#content-favorite').css('display', 'none');
        $('#content-today').css('display', 'block');
        $('#content-new').css('display', 'none');
        $('.intro-b0').css('background-color', color1);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color2);
        $('.open-all').css('background-color', color2);
        $('.open-all').html('OPEN ALL');
        $('#sel-title').html(brandsTitle.today[0]);
        $('#sel-subtitle').html(brandsTitle.today[1]);
        $('#title-favorite').css('display', 'none');
        $('#title-new').css('display', 'none');
    }
    else if(ind==1){
        $('#content-favorite').css('display', 'block');
        $('#content-today').css('display', 'none');
        $('#content-new').css('display', 'none');
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color1);
        $('.intro-b2').css('background-color', color2);
        $('.open-all').css('background-color', color2);
        $('.open-all').html('OPEN ALL');
        $('#sel-title').html(brandsTitle.favorite[0]);
        $('#sel-subtitle').html(brandsTitle.favorite[1]);
        $('#title-favorite').css('display', 'none');
        $('#title-new').css('display', 'none');
    } else if(ind==2){
        $('#content-new').css('display', 'block');
        $('#content-today').css('display', 'none');
        $('#content-favorite').css('display', 'none');
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color1);
        $('.open-all').css('background-color', color2);
        $('.open-all').html('OPEN ALL');
        $('#sel-title').html(brandsTitle.newbrand[0]);
        $('#sel-subtitle').html(brandsTitle.newbrand[1]);
        $('#title-favorite').css('display', 'none');
        $('#title-new').css('display', 'none');
    }
}

function allIntroBrands(){
    //alert('all');
    if(setInd1==0){
        brandsTitle = setBrandsPageVar(1);
        setInd1 = 1;
    }
    //debugObject(brandsTitle);
    if($('.open-all').html() == 'CLOSE'){
        $('#content-favorite').css('display', 'none');
        $('#content-today').css('display', 'block');
        $('#content-new').css('display', 'none');
        $('#title-favorite').css('display', 'none');
        $('#title-new').css('display', 'none');
        $('.open-all').html('OPEN ALL');
        $('.open-all').css('background-color', color2);
        $('.intro-b0').css('background-color', color1);
    } else{
        $('#content-favorite').css('display', 'block');
        $('#content-today').css('display', 'block');
        $('#content-new').css('display', 'block');
        $('#title-favorite').css('display', 'block');
        $('#title-new').css('display', 'block');
        $('.open-all').css('background-color', color1);
        $('.open-all').html('CLOSE');
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color2);
        $('#sel-title').html(brandsTitle.today[0]);
        $('#sel-subtitle').html(brandsTitle.today[1]);
    }
}

function toggleSearch(){
    $('#searchCategory').css('display','none');
    if($('#searchBar').css('display') == 'block'){
        $('#searchBar').css('display','none');
    } else {
        $('#searchBar').css('display', 'block');
    }
}
function toggleCategory(){
    $('#searchBar').css('display','none');
    if($('#searchCategory').css('display') == 'block'){
        $('#searchCategory').css('display','none');
    } else {
        $('#searchCategory').css('display', 'block');
    }
}

function searchBrands(ind){
alert(ind);
}