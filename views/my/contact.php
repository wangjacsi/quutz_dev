<!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">
                <div class="block_general_title_1">
                    <h1><?php echo $this->mainTitle; ?></h1>
                    <h2><?php echo $this->subTitle; ?></h2>
                </div>

                <div class="block_comments_1 brand">
                    <h3>Comments <span>26</span></h3>

                    <div class="comments">
                        <div class="comment" id="comment1">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="https://img1.etsystatic.com/018/0/26880813/iusa_75x75.25532419_kdp8.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Rojer Moor</a></div>
                                    <div class="type" title="comment"><i class="fa fa-comment"></i></div>

                                    <div class="disagree" title="Disagree"><a onclick="commentDisagree('comment1')"><i class="fa fa-thumbs-down"></i></a>&nbsp;123</div>
                                    <div class="agree" title="Agree"><a onclick="commentAgree('comment1')"><i class="fa fa-thumbs-up"></i></a>&nbsp;3</div>

                                    <div class="delete" title="delete"><a onclick="commentDelete('comment1')"><i class="fa fa-times"></i></a></div>
                                    <div class="edit" title="edit"><a onclick="commentEdit('comment1')"><i class="fa fa-pencil"></i></a></div>
                                    <div class="info">Sept 24, 2013<a onclick="replyShow('comment1')">REPLY</a></div>
                                    <div class="text">
                                        <p>Natus sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis.</p>
                                    </div>
                                </div>
                                <div class="clearboth"></div>
                            </div>
                            <div id="comment1-reply"><div class="comment reply reply-form" id="comment1-reply1" style="display: none;"><div class="inside"><div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div><div class="content"><div class="author"><a href="#">Admin</a></div><div class="field"><input type="text" name="reply" class="w_focus_mark"></div><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-reply fa-fw"></i>&nbsp;&nbsp;Reply</a></div><div class="clearboth"></div></div></div></div>
                        </div>

                        <div class="comment" id="comment2">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="http://resize-ec1.thefancy.com/resize/crop/33/thefancy/UserImages/GADUEN_8ca52c522927.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Simon Fowler</a></div>
                                    <div class="type" title="Like it"><i class="fa fa-heart"></i></div>
                                    <div class="delete" title="delete"><a onclick="commentDelete('comment2')"><i class="fa fa-times"></i></a></div>
                                    <div class="edit" title="edit"><a onclick="commentEdit('comment2')"><i class="fa fa-pencil"></i></a></div>
                                    <div class="info">Sept 24, 2013<a onclick="replyShow('comment2')">REPLY</a></div>
                                    <div class="text">
                                        <p>Accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarc.</p>
                                    </div>
                                </div>
                                <div class="clearboth"></div>
                            </div>
                            <div id="comment2-reply">
                                <div class="comment">
                                    <div class="inside">
                                        <div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div>
                                        <div class="content">
                                            <div class="author"><a href="#">Admin</a></div>
                                            <div class="delete" title="delete"><a onclick="replyDelete('comment2', 'replyID')"><i class="fa fa-times"></i></a></div>
                                            <div class="edit" title="edit"><a onclick="replyEdit('comment2', 'replyID')"><i class="fa fa-pencil"></i></a></div>
                                            <div class="info">Sept 24, 2013<!--<a href="#">REPLY</a>--></div>
                                            <div class="text">
                                                <p>Dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto.</p>
                                            </div>
                                        </div>
                                        <div class="clearboth"></div>
                                    </div>
                                </div>
                            <div class="comment reply reply-form" id="comment1-reply1" style="display: none;"><div class="inside"><div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div><div class="content"><div class="author"><a href="#">Admin</a></div><div class="field"><input type="text" name="reply" class="w_focus_mark"></div><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-reply fa-fw"></i>&nbsp;&nbsp;Reply</a></div><div class="clearboth"></div></div></div></div>
                        </div>

                        <div class="comment" id="comment3">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/000/0/15554424/iusa_75x75.9371160.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Ronald King</a></div>
                                    <div class="type" title="Question"><i class="fa fa-question-circle"></i></div>
                                    <div class="delete" title="delete"><a onclick="commentDelete('comment3')"><i class="fa fa-times"></i></a></div>
                                    <div class="edit" title="edit"><a onclick="commentEdit('comment3')"><i class="fa fa-pencil"></i></a></div>
                                    <div class="info">Sept 24, 2013<a onclick="replyShow('comment3')">REPLY</a></div>
                                    <div class="text">
                                        <p>Qolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto beatae vitae.</p>
                                    </div>
                                </div>
                                <div class="clearboth"></div>
                            </div>
                            <div id="comment3-reply"><div class="comment reply reply-form" id="comment1-reply1" style="display: none;"><div class="inside"><div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div><div class="content"><div class="author"><a href="#">Admin</a></div><div class="field"><input type="text" name="reply" class="w_focus_mark"></div><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-reply fa-fw"></i>&nbsp;&nbsp;Reply</a></div><div class="clearboth"></div></div></div></div>
                        </div>

                        <div class="comment" id="comment4">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Sandra Downing</a></div>
                                    <div class="star-rating" title="Rating 4.5">
                                        <span class="rating-star nocolor">
                                            <span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span>
                                        </span>
                                        <div class="star-rating2" style="width:87%; overflow:hidden;">
                                            <span class="rating-star color">
                                                <span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="delete" title="delete"><a onclick="commentDelete('comment4')"><i class="fa fa-times"></i></a></div>
                                    <div class="edit" title="edit"><a onclick="commentEdit('comment4')"><i class="fa fa-pencil"></i></a></div>
                                    <div class="info">Sept 24, 2013<a onclick="replyShow('comment4')">REPLY</a></div>
                                    <div class="text">
                                        <p>Ratus sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.</p>
                                    </div>
                                </div>
                                <div class="clearboth"></div>
                            </div>
                            <div id="comment4-reply"><div class="comment reply reply-form" id="comment1-reply1" style="display: none;"><div class="inside"><div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div><div class="content"><div class="author"><a href="#">Admin</a></div><div class="field"><input type="text" name="reply" class="w_focus_mark"></div><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-reply fa-fw"></i>&nbsp;&nbsp;Reply</a></div><div class="clearboth"></div></div></div></div>
                        </div>

                        <div class="comment" id="comment5">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="http://thefancy-media-ec3.thefancy.com/310/20140122/545367179697063472_112c9f5de1c3.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Sandra Downing</a></div>
                                    <div class="type" title="Private"><i class="fa fa-envelope"></i></div>
                                    <div class="delete" title="delete"><a onclick="commentDelete('comment5')"><i class="fa fa-times"></i></a></div>
                                    <div class="edit" title="edit"><a onclick="commentEdit('comment5')"><i class="fa fa-pencil"></i></a></div>
                                    <div class="info">Sept 24, 2013<a onclick="replyShow('comment5')">REPLY</a></div>
                                    <!--<div class="text">
                                        <p>Ratus sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.</p>
                                    </div>-->
                                </div>
                                <div class="clearboth"></div>
                            </div>
                            <div id="comment5-reply"><div class="comment reply reply-form" id="comment1-reply1" style="display: none;"><div class="inside"><div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div><div class="content"><div class="author"><a href="#">Admin</a></div><div class="field"><input type="text" name="reply" class="w_focus_mark"></div><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-reply fa-fw"></i>&nbsp;&nbsp;Reply</a></div><div class="clearboth"></div></div></div></div>
                        </div>
                        <div class="comment" id="comment6">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">ADMIN</a></div>
                                    <div class="type" title="Info. notice"><i class="fa fa-bullhorn"></i></div>
                                    <div class="delete" title="delete"><a onclick="commentDelete('comment6')"><i class="fa fa-times"></i></a></div>
                                    <div class="edit" title="edit"><a onclick="commentEdit('comment6')"><i class="fa fa-pencil"></i></a></div>
                                    <div class="info">Sept 24, 2013<a onclick="replyShow('comment6')">REPLY</a></div>
                                    <div class="text">
                                        <p>Ratus sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.</p>
                                    </div>
                                </div>
                                <div class="clearboth"></div>
                            </div>
                            <div id="comment6-reply"><div class="comment reply reply-form" id="comment1-reply1" style="display: none;"><div class="inside"><div class="avatar"><a href="#"><img src="https://img0.etsystatic.com/011/0/27158461/iusa_75x75.21906740_l1md.jpg" alt=""></a></div><div class="content"><div class="author"><a href="#">Admin</a></div><div class="field"><input type="text" name="reply" class="w_focus_mark"></div><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-reply fa-fw"></i>&nbsp;&nbsp;Reply</a></div><div class="clearboth"></div></div></div></div>
                        </div>
                    </div>

                    <!--<div class="comments">
                        <div class="comment">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="images/ava_default_1.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Rojer Moor</a></div>
                                    <div class="info">Sept 24, 2013<a href="#">REPLY</a></div>
                                    <div class="text">
                                        <p>Natus sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto beatae vitae dicta.Sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur.</p>
                                    </div>
                                </div>

                                <div class="clearboth"></div>
                            </div>
                        </div>

                        <div class="comment">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="images/ava_default_1.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Simon Fowler</a></div>
                                    <div class="info">Sept 24, 2013<a href="#">REPLY</a></div>
                                    <div class="text">
                                        <p>Accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto beatae vitae dicta.Sunt explicabo. Nemo enim ipsam voluptatem.</p>
                                    </div>
                                </div>

                                <div class="clearboth"></div>
                            </div>

                            <div class="comment">
                                <div class="inside">
                                    <div class="avatar"><a href="#"><img src="images/ava_1.jpg" alt=""></a></div>
                                    <div class="content">
                                        <div class="author"><a href="#">Admin</a></div>
                                        <div class="info">Sept 24, 2013<a href="#">REPLY</a></div>
                                        <div class="text">
                                            <p>Dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto beatae vitae dicta.Sunt explicabo. Nemo enim ipsam voluptatem.</p>
                                        </div>
                                    </div>

                                    <div class="clearboth"></div>
                                </div>
                            </div>
                        </div>

                        <div class="comment">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="images/ava_default_1.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Ronald King</a></div>
                                    <div class="info">Sept 24, 2013<a href="#">REPLY</a></div>
                                    <div class="text">
                                        <p>Qolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto beatae vitae dicta.Sunt explicabo. Nemo enim ipsam voluptatem.</p>
                                    </div>
                                </div>

                                <div class="clearboth"></div>
                            </div>
                        </div>

                        <div class="comment last_comment">
                            <div class="inside">
                                <div class="avatar"><a href="#"><img src="images/ava_default_1.jpg" alt=""></a></div>
                                <div class="content">
                                    <div class="author"><a href="#">Sandra Downing</a></div>
                                    <div class="info">Sept 24, 2013<a href="#">REPLY</a></div>
                                    <div class="text">
                                        <p>Ratus sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque ipsa quae.  Ab illo inventore veritatis et quasiarchitecto beatae vitae dicta.Sunt explicabo. Nemo enim ipsam voluptatem.</p>
                                    </div>
                                </div>

                                <div class="clearboth"></div>
                            </div>
                        </div>
                    </div>-->

                    <div class="block_pager_1 brand">
                        <ul>
                            <li class="current"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#" class="next">Next</a></li>
                        </ul>

                        <div class="info">Page 1 of 3</div>

                        <div class="clearboth"></div>
                    </div>
                </div>

                <div class="block_leave_comment_1">
                    <h3>Live a comment</h3>

                    <div class="form">
                        <form action="#">
                            <div class="fields">
                                <div class="label">Name <span>*</span></div>
                                <div class="field"><input type="text" name="name" class="w_focus_mark required brand" value="Lucas55" disabled></div>

                                <!--<div class="label">E-mail <span>*</span></div>
                                <div class="field"><input type="text" name="email" class="w_focus_mark required"></div>-->

                                <div class="label">Message Type</div>
                                <select class="comment-type">
                                    <option value="0">&nbsp;&nbsp;&nbsp;Common Comment</option>
                                    <option value="1">&nbsp;&nbsp;&nbsp;Like this</option>
                                    <option value="2">&nbsp;&nbsp;&nbsp;Question</option>
                                    <option value="3">&nbsp;&nbsp;&nbsp;Private Comment</option>
                                    <!--<option value="4">&nbsp;&nbsp;&nbsp;Evaluation - purchased</option>-->
                                    <option value="4">&nbsp;&nbsp;&nbsp;Infomation</option>
                                </select>
                                <!--<div class="field"><input type="text" name="subject" class="w_focus_mark"></div>-->
                            </div>

                            <div class="oh">
                                <div class="label">Message <span>*</span></div>
                                <div class="textarea"><textarea name="message" class="w_focus_mark" cols="1" rows="1"></textarea></div>

                                <div class="button"><a href="#" class="general_button_type_3 submit">Submit Your Comment</a></div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!-- CONTENT END -->