
        <!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">

                <div id="title-brands" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="sel-title"><?php echo $this->mainTitle; ?></h1>
                    <h2 id="sel-subtitle"><?php echo $this->subTitle; ?></h2>
                </div>

                <div class="block_posts type_1 type_sort general_not_loaded">
                    <div class="posts">
                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/041/0/6416669/il_570xN.587135873_p4mx.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <!--<div class="num">4.8</div>-->
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Product name is djks sads sa sd</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>1517.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">AEEDFEsdasdadadsdadR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>15K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/027/1/7789766/il_340x270.628067245_abm6.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">PGJ sjsksaiis jean</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>87.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">ABC 189</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/025/0/8925614/il_570xN.543457281_tgit.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">156 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">fgf fg fgdads sadda sdas d dasd ksaiis jean</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>37.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">57HRFG</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>5K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/039/0/5631970/il_300x300.631159389_4kpi.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">556 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">TU fhns jsjdh jsdh ds sds </a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>55
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">OUTSTANDING</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>35K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>1342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="//resize-ec1.thefancy.com/resize/crop/313/thingd/default/668048234370832136_af3cd9d0be32.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:85%;"></div></div>
                                    <div class="review-num">2.5K reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Ejhd fsdi isdj sjdj dk spkd s</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>39.99
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">SHOP 365</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/037/0/7972092/il_570xN.605326585_sfzm.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:70%;"></div></div>
                                    <div class="review-num">23 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">rhs hdhs hshd h sds sda</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>224.74
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">CARTER GOU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>3235K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>842</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <!--<article class="post_type_1">
                            <div class="feature">
                                <div class="slider">
                                    <a href=""><span class="cat-name"></span></a>
                                    <div id="post_slider_1" class="flexslider">
                                        <ul class="slides">
                                            <li><img src="https://img1.etsystatic.com/035/0/6414460/il_340x270.628381141_iay4.jpg" alt=""></li>
                                            <li><img src="https://img1.etsystatic.com/043/2/8157280/il_340x270.586338037_b3t8.jpg" alt=""></li>
                                            <li><img src="https://img0.etsystatic.com/032/0/9722637/il_224xN.629185334_469v.jpg" alt=""></li>
                                        </ul>
                                        <span class="hover"></span>
                                    </div>
                                    <div class="review">
                                        <div class="num">4.9</div>
                                    </div>
                                    <script type="text/javascript">
                                        jQuery(function() {
                                            init_post_slider_1('#post_slider_1');
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">brand <strong><a href="" class="font-color1">Bdhrh 198</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes">7895</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>57</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Cute and sdksk.</a>
                                </div>
                            </div>
                        </article>-->

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="http://thefancy-media-ec5.thefancy.com/310/20140110/536673437096089618_bdfc848dfbdc.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:95%;"></div></div>
                                    <div class="review-num">7 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">T-shirt hds shs dah shsd dshas d</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>25
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">DDR CHAMPS</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>5K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>42</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/017/1/5322868/il_224xN.475417888_sfzn.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:86%;"></div></div>
                                    <div class="review-num">76 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Beautiful care rice dd cook</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>37.8
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">CCURE 54 GOU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>35K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>42</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/042/0/6122716/il_570xN.618992274_48ry.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:60%;"></div></div>
                                    <div class="review-num">74 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">SO OS jdj Beautiful care rice dd cook</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>65
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">VIMEO FFR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>935K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>402</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>

                    <div class="controls">
                        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
                    </div>
                </div>

            </div>
        </div>
        <!-- CONTENT END -->

