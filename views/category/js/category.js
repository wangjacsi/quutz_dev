var setInd1 = 0;
var brandsTitle;
function introBrands(ind){
    //alert(ind);
    if(setInd1==0){
        titles = setBrandsPageVar(2);
        setInd1 = 1;
    }
    if(ind==0){
        $('.intro-b0').css('background-color', color1);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color2);
        $('.intro-b3').css('background-color', color2);
        $('#content-brands').css('display', 'block');
        $('#content-hot').css('display', 'none');
        $('#content-new').css('display', 'none');
        $('#content-celebs').css('display', 'none');
        $('#sel-title').html(titles.brands[0]);
        $('#sel-subtitle').html(titles.brands[1]);
    }
    else if(ind==1){
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color1);
        $('.intro-b2').css('background-color', color2);
        $('.intro-b3').css('background-color', color2);
        $('#content-brands').css('display', 'none');
        $('#content-hot').css('display', 'block');
        $('#content-new').css('display', 'none');
        $('#content-celebs').css('display', 'none');
        $('#sel-title').html(titles.hot[0]);
        $('#sel-subtitle').html(titles.hot[1]);
    } else if(ind==2){
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color1);
        $('.intro-b3').css('background-color', color2);
        $('#content-brands').css('display', 'none');
        $('#content-hot').css('display', 'none');
        $('#content-new').css('display', 'block');
        $('#content-celebs').css('display', 'none');
        $('#sel-title').html(titles.newitems[0]);
        $('#sel-subtitle').html(titles.newitems[1]);
    } else if(ind==3){
        $('.intro-b0').css('background-color', color2);
        $('.intro-b1').css('background-color', color2);
        $('.intro-b2').css('background-color', color2);
        $('.intro-b3').css('background-color', color1);
        $('#content-brands').css('display', 'none');
        $('#content-hot').css('display', 'none');
        $('#content-new').css('display', 'none');
        $('#content-celebs').css('display', 'block');
        $('#sel-title').html(titles.celebs[0]);
        $('#sel-subtitle').html(titles.celebs[1]);
    }
}
/**
 * Page after load function
 * jQuery load
 */
var lastGetFlag;
var cntDisp; // range from 0 ~ last
var getItemFlag;
var viewType;
var getType;
var initState;
//var $container;
$( function() {
    var filters = {};
    var $buttonGroup;
    var filterGroup;
    //initialize global values
    init_value();
    // init Isotope
    $container = $('.block_posts.type_sort .posts');
    $container.isotope({
        itemSelector: 'article',
        filter: function(){
            var isMatched = true;
            var $this = $(this);
            for (var prop in filters){
                var filter = filters[prop];
                filter = filterFns[filter] || filter;
                if(filter){
                    isMatched = isMatched && $(this).is(filter);
                }
                if(!isMatched){
                    break;
                }
            }
            return isMatched;
        },
        getSortData: {
            date:function( itemElem ) {
                var date2 = $( itemElem ).find('.date2').text();
                return date2;
            },
            collects:'.collects2 parseInt',
            rated: function( itemElem ) {
                var rated2 = $( itemElem ).find('.rated2').text();
                return parseFloat( rated2);
            },
            phigh: function( itemElem ) {
                var price2 = $( itemElem ).find('.price2').text();
                return parseFloat( price2);
            },
            plow: function( itemElem ) {
                var price2 = $( itemElem ).find('.price2').text();
                return parseFloat( price2);
            },
            likes: '.likes2 parseInt'
        },
        sortAscending: {
            date:false,
            likes: false,
            collects: false,
            rated:false,
            phigh:false,
            plow:true
        }
    });

    // pagination
    $(document).on('click','.block_pager_1 ul li a.pointer', function(){
        //alert($(this).attr('value'));
        if($(this).attr('value')=='next'){
            cntDisp += 1;
        } else if($(this).attr('value')=='prev'){
            cntDisp -= 1;
        } else{
            cntDisp = parseInt($(this).attr('value'))-1;
        }
        if(cntDisp < 0){
            cntDisp = 0;
        }
        initState = 1;
        empty_isotope();
        sortSetVar();
        getItems();
        pagiNation(cntDisp);
    });

    // View mode: grid type or fixed type
    // Get data type define: masonry for scroll or fitRow for paging
    var $viewMode = $('.select-group .view');
    $viewMode.change( function() {
        viewModeChk();
        //alert('cntDisp: '+cntDisp+' initState: '+initState);
        if(viewType=="masonry"){
            if(initState == 0){
                isotopeAddClass('fixed-layout', 'remove');
                cntDisp=1;
                $dispItemsBefore ='';
                $dispItemsCurrent = '';
                getItems();
            } else {
                init_value();
                empty_isotope();
                getItems();
            }
        } else { // fitRow
            if(initState == 0){
                isotopeAddClass('fixed-layout', 'add');
            } else {
                init_value();
                empty_isotope();
                getItems();
            }
        }
        $container.isotope({ layoutMode: viewType });
    });
    cntDisp = 1;
    getItems();

    // Filter
    // store filter for each group
    var filterFns = {
        priceFind: function() {
            var number = $(this).find('.price2').text();
            var lowPrice = parseFloat($('#lprice')[0].value);
            var highPrice = parseFloat($('#hprice')[0].value);
            return (parseFloat( number) >= lowPrice) && (parseFloat( number) <= highPrice);
        }
    }
    var $eventFilter = $('.select-group .filter-event');
    $eventFilter.change( function() {
        var filterValue = $eventFilter[0].value;
        var $this = $(this);
        filterGroup = $this.attr('data-filter-group');
        filters[ filterGroup ] = filterValue;
        console.log(filters);
        $container.isotope('arrange');
        //filterValue = filterFns[ filterValue ] || filterValue;
        //  $container.isotope({ filter: filterValue });
    });
    // use value of search field to filter
    var lpriceValue = 0;
    var hpriceValue = 0;
    var $lprice = $('#lprice').keyup( debounce( function() {
        var numbers = /^[0-9]+$/;
        if($lprice[0].value.match(numbers)){
            //alert($lprice[0].value);
            lpriceValue = $lprice[0].value;
            if(chkNumber()){
                filters[ 'priceFind' ] = 'priceFind';
                $container.isotope('arrange');
            }
        } else{
            $lprice[0].value = '';
            alert('Only digit number!');
        }
    }, 200 ) );
    // use value of search field to filter
    var $hprice = $('#hprice').keyup( debounce( function() {
        var numbers = /^[0-9]+$/;
        if($hprice[0].value.match(numbers)){
            //alert($hprice[0].value);
            hpriceValue = $hprice[0].value;
            if(chkNumber()){
                filters[ 'priceFind' ] = 'priceFind';
                $container.isotope('arrange');
            }
        } else{
            $hprice[0].value = '';
            alert('Only digit number!');
        }
    }, 200 ) );

    function chkNumber(){
        if(parseInt(lpriceValue) < parseInt(hpriceValue)){
            return 1;
        } else{
            return 0;
        }
    }

    // sorting
    var $selects = $('.select-group .sort-main');
    $selects.change( function() {
        sortSetVar();
        //alert(sortData);
        if(lastGetFlag == 1){
            $container.isotope({ sortBy: sortData });
        } else {
            init_value();
            empty_isotope();
            getItems();
        }
    });
    // Get Ajax Data from DB
    /*$('#button_load_more').on( 'click', function(e) {
        // create new item elements
        getItems();
        jQuery(this).removeClass('loading');
        e.preventDefault();
        initState = 2;
    });*/

    // Price filter button toggle
    var setPrice = 1;
    $('.priceFilter').on( 'click', function() {
        if( setPrice ==1 ){
            setPrice = 0;
            $( "#lprice" ).prop( "disabled", true );
            $( "#hprice" ).prop( "disabled", true );
            $( "#lprice" ).val('');
            $( "#hprice" ).val('');
            $(this).css('background-color', '#969696');
            //console.log(filters);
            // filters set empty
            if( filters['priceFind'] ) {
                filters[ 'priceFind' ] = '';
                //console.log(filters);
                $container.isotope('arrange');
            }
        } else {
            setPrice = 1;
            $( "#lprice" ).prop( "disabled", false );
            $( "#hprice" ).prop( "disabled", false );
            $(this).css('background-color', '#161e26');
        }
    });
});

// for pagination
function pagiNation(numpage){
    var last = parseInt($('.block_pager_1 .info').attr('last'));
    var pageHtml = makePaginationHtml(last, numpage+1);
    $('.block_pager_1').empty().html(pageHtml);
}

//infinite scroll
function element_in_scroll(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom+120 <= docViewBottom) && (elemTop >= docViewTop));
}

function scrollControl(){
    $(window).scroll( function(e) {
        if (element_in_scroll(".block_posts .posts article:last")) {
            $('.controls .scroll-status').css('display', 'block');
            var scrollStatusHtml = '<i class="fa fa-spinner fa-spin"></i><span>Loading items</span>';
            $('.controls .scroll-status').html(scrollStatusHtml);

            var last = parseInt($('.block_pager_1 .info').attr('last'));
            if(cntDisp == last){
                $(window ).unbind('scroll');
            }
            getItems();
            cntDisp++;
            initState = 2;
        }
    });
}




function viewModeChk(){
    var $viewMode = $('.select-group .view');
    viewType = $viewMode[0].value;
    if(viewType=="masonry"){
        getType = 'scroll';
        $('.block_pager_1').css('display', 'none');
        scrollControl();
    } else{
        getType = 'paging';
        $(window ).unbind('scroll');
        $('.controls .scroll-status').css('display','none');
        $('.block_pager_1').css('display', 'block');
    }
}

function sortSetVar(){
    var $selects = $('.select-group .sort-main');
    var sortByValue = $selects[0].value;
    sortData = sortByValue;
}

// type:1 add, type:0 remove
function isotopeAddClass(classname, type){
    var elems = $container.isotope('getItemElements');
    if(type=='add'){
        for(var idx in elems){
            //console.log(elems[idx]);
            $(elems[idx]).addClass(classname);
        }
    } else {
        for(var idx in elems){
            //console.log(elems[idx]);
            $(elems[idx]).removeClass(classname);
        }
    }

}

var sortData;
function init_value(){
    //for design
    $('.block_pager_1').css('display', 'none');
    //for functional
    $dispItemsBefore ='';
    $dispItemsCurrent = '';
    lastGetFlag = 0;
    getItemFlag = 1;
    sortSetVar();
    cntDisp = 0;
    initState = 0;
    viewModeChk();
}

function empty_isotope(){
    var elems = $container.isotope('getItemElements');
    $container.isotope( 'remove', elems );
    //$container.empty();
    //$container.css('height', '0px');
}

var $dispItemsBefore;
var $dispItemsCurrent;
function getItems(){
    if(getItemFlag){
        var request = $.ajax({
            url: URL+"category/getMoreItems",
            data:{
                pagenum : cntDisp,
                sort : sortData,
                filter : 0
            },
            type: 'post'
        });
        request.done(function(data){
            var resp = data;
                resp = resp.trim();
            var jsonFlag;
            try {
                var json = JSON.parse(resp);
                jsonFlag = 1;
            } catch(e) {
                // Situation 1 like this : No more data from DB and so returning
                // response message is 'nodata'
                // situation 2 : db response error so neet to try again
                alert('data is not json' + resp);
                jsonFlag = 0;
                if(getType =='scroll'){
                    if(resp == 'nodata'){ //situation 1
                        lastGetFlag = 1;
                        getItemFlag = 0;
                        // scroll event remove
                        $(window ).unbind('scroll');
                        if($dispItemsCurrent != '' && $dispItemsCurrent != null){
                            $container.append( $dispItemsCurrent )
                            // add and lay out newly appended elements
                            .isotope( 'appended', $dispItemsCurrent ).isotope('layout').isotope('updateSortData').isotope();
                        }
                        $dispItemsBefore = '';
                        $dispItemsCurrent = '';
                        $('.controls .scroll-status').css('display','block');
                        var scrollStatusHtml = '<i class="fa fa-exclamation-circle"></i><span>No more items</span>';
                        $('.controls .scroll-status').html(scrollStatusHtml);
                    } else if(resp == 'errordb'){ //situation 2
                        $('.controls .scroll-status').css('display','block');
                        var scrollStatusHtml = '<i class="fa fa-exclamation-circle"></i><span>Network delay occur try again</span>';
                        $('.controls .scroll-status').html(scrollStatusHtml);
                        getItemFlag = 1;
                    }
                }
            }
            if(jsonFlag == 1 && getType =='scroll'){
                $('.controls .scroll-status').css('display','none');

                $dispItemsBefore = $dispItemsCurrent;
                $dispItemsCurrent = makeDispHtml(json);
                if($dispItemsBefore != ''){
                    $container.append( $dispItemsBefore )
                    // add and lay out newly appended elements
                    .isotope( 'appended', $dispItemsBefore ).isotope('layout').isotope('updateSortData').isotope();
                }
                if(cntDisp == 0){
                    cntDisp = 1;
                    getItems();
                } else if(cntDisp == 1){
                    cntDisp = 2;
                }
                /*else {
                    cntDisp++;
                }*/
            } else if(jsonFlag == 1){// paging
                $dispItemsCurrent = makeDispHtml(json);
                if($dispItemsCurrent != ''){
                    $container.append( $dispItemsCurrent )
                    // add and lay out newly appended elements
                    .isotope( 'appended', $dispItemsCurrent ).isotope('layout').isotope('updateSortData').isotope();
                }
            }
        });
    }
}
function makeDispHtml(data){
    //debugObject(data);
    var str = '';
    viewType = $('.select-group .view')[0].value;
    //alert(viewType);
    for(var key in data){
        //alert(key+'  -  '+data[key]['brandID']);
        if(viewType == 'fitRows'){
            if(data[key]['priceEvent']=='1')
                str += '<article class="post_type_1 product pEvent fixed-layout"><div style="display:none;">';
            else
                str += '<article class="post_type_1 product fixed-layout"><div style="display:none;">';
        } else {
            if(data[key]['priceEvent']=='1')
                str += '<article class="post_type_1 pEvent product"><div style="display:none;">';
            else
                str += '<article class="post_type_1 product"><div style="display:none;">';
        }
        str += '<div class="date2">'+data[key]['prodDateCreated']+'</div>';
        str += '<div class="likes2">'+data[key]['prodLikes']+'</div><div class="collects2">'+data[key]['prodCollects']+'</div>';
        str += '<div class="rated2">'+data[key]['prodRating']+'</div>';
        str += '<div class="price2">'+data[key]['newPrice']+'</div></div>';
        str += '<div class="feature"><div class="image">';
        str += '<a href=""><img src="'+data[key]['prodPhotoPath']+data[key]['prodPhotoName']+'">';
        str += '<span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a></div>';
        if(data[key]['priceEvent']=='1'){
            str += '<div class="ribbon"><a href="#">SALE</a></div>';
            str += '<div class="price-old2"><i class="fa fa-dollar"></i>'+data[key]['price']+'</div>';
        }
        str += '<div class="review"><div class="value" title="'+data[key]['prodRating']+'"><div style="width:'+setRatingStar(data[key]['prodRating'])+'%;"></div></div>';
        str += '<div class="review-num">'+shortNumStr(data[key]['prodReviews'])+' reviews</div></div>';
        str += '<div class="user-action"><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>';
        str += '<a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>';
        str += '<a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a></div>';
        str += '<div class="cart"><a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a></div></div>';
        str += '<div class="content"><div class="title"><a href="">'+data[key]['prodTitle']+'</a></div>';
        str += '<div class="price"><i class="fa fa-dollar"></i>'+data[key]['newPrice']+'</div><div class="f-clear"></div>';
        str += '<div class="info"><div class="tags">BY <strong><a href="#" class="font-color1">'+data[key]['brandName']+'</a></strong></div>';
        str += '<div class="stats show-stat"><div class="likes" title="Likes"><i class="fa fa-heart"></i>'+shortNumStr(data[key]['prodLikes'])+'</div>';
        str += '<div class="comments" title="Collections"><i class="fa fa-cube"></i>'+shortNumStr(data[key]['prodCollects'])+'</div></div></div></div></article>';
    }
    var $item =$(str);
    //$dispItems = $(str);
    return $item;
}

// debounce so filtering doesn't happen every millisecond
function debounce( fn, threshold ) {
  var timeout;
  return function debounced() {
    if ( timeout ) {
      clearTimeout( timeout );
    }
    function delayed() {
      fn();
      timeout = null;
    }
    timeout = setTimeout( delayed, threshold || 100 );
  }
}
