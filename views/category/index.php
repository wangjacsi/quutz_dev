
        <!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">

                <div id="title-brands" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="sel-title"><?php echo $this->mainTitle; ?></h1>
                    <h2 id="sel-subtitle"><?php echo $this->subTitle; ?></h2>
                    <div class="intro-filter">
                        <a onclick="introBrands('0')" class="general_colored_button intro-b0" title="Today's brands">Brands</a>
                        <a onclick="introBrands('1')" class="general_colored_button intro-b1" title="Hot Items">HOT</a>
                        <a onclick="introBrands('2')" class="general_colored_button intro-b2" title="New Items">NEW</a>
                        <a onclick="introBrands('3')" class="general_colored_button intro-b3" title="Popular Celebs">CELEBS</a>
                    </div>
                </div>

                <div id="content-brands" class="block_posts type_2 brands">
                    <?php
                    foreach ($this->popularBrandInfo as $key => $value) {
                        echo '<article class="post_type_4">
                                    <div class="feature pic4">
                                        <div class="image">
                                            <a href=""><img class="radius-lt" src="'.$this->popularBrandItems[$key][0]['prodPhotoPath'].$this->popularBrandItems[$key][0]['prodPhotoName'].'" alt=""></a>
                                            <a class="m-left2" href=""><img class="radius-rt" src="'.$this->popularBrandItems[$key][1]['prodPhotoPath'].$this->popularBrandItems[$key][1]['prodPhotoName'].'" alt=""></a>
                                            <a class="m-top2" href=""><img class="radius-lb" src="'.$this->popularBrandItems[$key][2]['prodPhotoPath'].$this->popularBrandItems[$key][2]['prodPhotoName'].'" alt=""></a>
                                            <a class="m-top2 m-left2" href=""><img class="radius-rb" src="'.$this->popularBrandItems[$key][3]['prodPhotoPath'].$this->popularBrandItems[$key][3]['prodPhotoName'].'" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="info">
                                            <div class="avatar">
                                                <a href="#"><img src="'.$value['brandPath'].$value['brandLogo'].'" alt=""></a>
                                            </div>
                                            <div class="brand-info">
                                                <div class="tags"><strong><a href="#" class="font-color1">'.$value['brandName'].'</a></strong></div>
                                                <div class="stats show-stat">
                                                    <div class="items" title="Items"><i class="fa fa-th-large"></i>'.shortNumStr($value['brandItems']).'</div>
                                                    <div class="likes" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['brandLikes']).'</div>
                                                    <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['brandCollects']).'</div>
                                                </div>
                                                <div class="tag-info">
                                                    <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                                    <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                                    <a href="#">Photography</a>
                                                    <a href="#">Photography</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>';
                    } ?>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-hot" class="block_posts type_2 brands">
                <!-- example of Price old and new -->
                    <?php foreach ($this->hottestItems as $key => $value) {
                        echo '<article class="post_type_4">
                                    <div class="product">
                                        <div class="feature">
                                            <div class="image">
                                                <a href=""><img class="radius5" src="'.$value['prodPhotoPath'].$value['prodPhotoName'].'"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                            </div>';
                                            if($value['priceEvent']=='1')
                                                echo '<div class="ribbon">
                                              <a href="#">SALE</a>
                                            </div>';
                                            echo '<div class="review">
                                                <div class="value" title="'.$value['prodRating'].'"><div style="width:'.setRatingStar($value['prodRating']).'%;"></div></div>
                                                <div class="review-num">'.shortNumStr($value['prodReviews']).' reviews</div>
                                            </div>
                                            <div class="user-action">
                                                <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                                <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                                <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                            </div>
                                            <div class="cart">
                                                <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="info">
                                            <div class="avatar">
                                                <a href="#"><img src="'.$value['brandPath'].$value['brandLogo'].'" alt=""></a>
                                            </div>
                                            <div class="brand-info">
                                                <div class="title-item"><a href="#">'.$value['prodTitle'].'</a></div>';
                                                 if($value['priceEvent']=='1')
                                                    echo '<div class="price-group">
                                                    <div class="price-old"><i class="fa fa-dollar"></i>'.$value['price'].'</div><br/>
                                                    <div class="price-new"><i class="fa fa-dollar"></i>'.$value['newPrice'].'</div>
                                                </div>';
                                                else echo '<div class="price"><i class="fa fa-dollar"></i>'.$value['price'].'</div>';
                                                echo '<div class="info-left">
                                                    <div class="tags"><strong><a href="#" class="font-color1">'.$value['brandName'].'</a></strong></div>
                                                    <br/>
                                                </div>
                                                <div class="stats show-stat m-top2">
                                                    <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['prodLikes']).'</div>
                                                    <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['prodCollects']).'</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>';
                    }
                    ?>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-new" class="block_posts type_2 brands">
                    <?php foreach ($this->newItems as $key => $value) {
                            echo '<article class="post_type_4">
                            <div class="product">
                                <div class="feature">
                                    <div class="image">
                                        <a href=""><img class="radius5" src="'.$value['prodPhotoPath'].$value['prodPhotoName'].'"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                    </div>';
                                    if($value['priceEvent']=='1')
                                            echo '<div class="ribbon">
                                          <a href="#">SALE</a>
                                        </div>';
                                    echo '<div class="review">
                                        <div class="value" title="'.$value['prodRating'].'"><div style="width:'.setRatingStar($value['prodRating']).'%;"></div></div>
                                        <div class="review-num">'.shortNumStr($value['prodReviews']).' reviews</div>
                                    </div>
                                    <div class="user-action">
                                        <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                        <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                        <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                    </div>
                                    <div class="cart">
                                        <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar">
                                        <a href="#"><img src="'.$value['brandPath'].$value['brandLogo'].'" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="title-item"><a href="#">'.$value['prodTitle'].'</a></div>';
                                        if($value['priceEvent']=='1')
                                            echo '<div class="price-group">
                                                    <div class="price-old"><i class="fa fa-dollar"></i>'.$value['price'].'</div><br/>
                                                    <div class="price-new"><i class="fa fa-dollar"></i>'.$value['newPrice'].'</div>
                                                </div>';
                                        else echo '<div class="price"><i class="fa fa-dollar"></i>'.$value['price'].'</div>';
                                        echo '<div class="info-left">
                                            <div class="tags"><strong><a href="#" class="font-color1">'.$value['brandName'].'</a></strong></div>
                                            <br/>
                                        </div>
                                        <div class="stats show-stat m-top2">
                                            <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['prodLikes']).'</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['prodCollects']).'</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>';
                    }
                    ?>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-celebs" class="block_posts type_2 brands">
                    <?php foreach ($this->celebItems as $key => $value) {
                            echo '<article class="post_type_4">
                            <div class="product">
                                <div class="feature">
                                    <div class="image">
                                        <a href=""><img class="radius5" src="'.$value['reviewPath'].$value['reviewPhotoName'].'"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                    </div>';
                                    if($value['priceEvent']=='1')
                                            echo '<div class="ribbon">
                                          <a href="#">SALE</a>
                                        </div>';
                                    echo '<div class="review">
                                        <div class="value" title="'.$value['prodRating'].'"><div style="width:'.setRatingStar($value['prodRating']).'%;"></div></div>
                                        <div class="review-num">'.shortNumStr($value['prodReviews']).' reviews</div>
                                    </div>
                                    <div class="user-action">
                                        <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                        <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                        <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                    </div>
                                    <div class="cart">
                                        <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="avatar">
                                        <a href="#"><img src="'.$value['celebPath'].$value['celebPhotoName'].'" alt=""></a>
                                    </div>
                                    <div class="brand-info">
                                        <div class="title-item"><a href="#">'.$value['prodTitle'].'</a></div>';
                                        if($value['priceEvent']=='1')
                                            echo '<div class="price-group">
                                                    <div class="price-old"><i class="fa fa-dollar"></i>'.$value['price'].'</div><br/>
                                                    <div class="price-new"><i class="fa fa-dollar"></i>'.$value['newPrice'].'</div>
                                                </div>';
                                        else echo '<div class="price"><i class="fa fa-dollar"></i>'.$value['price'].'</div>';
                                        echo '<div class="info-left">
                                            <div class="tags"><strong><a href="#" class="font-color1">'.$value['celebName'].'</a></strong></div>
                                            <br/>
                                        </div>
                                        <div class="stats show-stat m-top2">
                                            <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['reviewLikes']).'</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['reviewCollects']).'</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>';
                    }
                    ?>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div class="block_general_title_1 w_margin_1 m-top20">
                    <h1><?php echo $this->catNameForPage.$this->prodMainTitle; if($this->subCatNameForPage!='') echo ' - '.$this->subCatNameForPage;?></h1>
                    <h2><?php echo $this->prodSubTitle;?></h2>
                </div>
                <div class="select-group">
                    <i class="fa fa-eye"></i> VIEW&nbsp;
                    <select class="view">
                        <option value="masonry">Grid</option>
                        <option value="fitRows">Fixed</option>
                    </select>&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-sitemap"></i> CATEGORY&nbsp;
                    <select onclick="if (typeof(this.selectedIndex) != 'undefined') selectChk(this.selectedIndex)">
                        <option value="0">&nbsp;&nbsp;All</option>
                        <option value="1">&nbsp;&nbsp;Men</option>
                    </select>&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-sort-amount-desc"></i> SORT BY&nbsp;
                    <select class="sort-main">
                        <option value="date">&#xf073;&nbsp;&nbsp;Newest Items</option>
                        <option value="likes">&#xf004;&nbsp;&nbsp;Most Likes</option>
                        <option value="collects">&#xf1b2;&nbsp;&nbsp;Most Collections</option>
                        <option value="rated">&#xf005;&nbsp;&nbsp;Rated: High to Low</option>
                        <option value="phigh">&#xf155;&nbsp;&nbsp;&nbsp;&nbsp;Price: High to Low</option>
                        <option value="plow">&#xf155;&nbsp;&nbsp;&nbsp;&nbsp;Price: Low to High</option>
                    </select>&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-gift"></i> EVENT&nbsp;
                    <select class="filter-event" data-filter-group="event">
                        <option value="">Any</option>
                        <option value=".dEvent">&#xf0d1;&nbsp;&nbsp;Delivery Event</option>
                        <option value=".pEvent">&#xf155;&nbsp;&nbsp;&nbsp;&nbsp;Price Event</option>
                    </select>&nbsp;&nbsp;&nbsp;
                    <a class="general_colored_button default priceFilter"><i class="fa fa-dollar"></i> PRICE</a>
                    <input type="text" id="lprice" placeholder="0"> <input type="text" id="hprice" placeholder="500">&nbsp;&nbsp;&nbsp;

                </div>

                <div class="block_posts type_1 type_sort general_not_loaded">
                    <div class="posts">
                        <?php foreach ($this->dispBrandItems as $key => $value) {
                            if($value['priceEvent']=='1')
                                echo '<article class="post_type_1 product pEvent">';
                            else echo '<article class="post_type_1 product">';
                            echo '<div style="display:none;">
                                <div class="date2">'.$value['prodDateCreated'].'</div>
                                <div class="likes2">'.$value['prodLikes'].'</div>
                                <div class="collects2">'.$value['prodCollects'].'</div>
                                <div class="rated2">'.$value['prodRating'].'</div>
                                <div class="price2">'.$value['newPrice'].'</div>';
                            echo '</div>
                                <div class="feature">
                                    <div class="image">
                                        <a href=""><img src="'.$value['prodPhotoPath'].$value['prodPhotoName'].'"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                    </div>';
                                    if($value['priceEvent']=='1')
                                            echo '<div class="ribbon">
                                          <a href="#">SALE</a>
                                        </div>
                                        <div class="price-old2">
                                            <i class="fa fa-dollar"></i>'.$value['price'].'
                                        </div>';
                                    echo '<div class="review">
                                        <div class="value" title="'.$value['prodRating'].'"><div style="width:'.setRatingStar($value['prodRating']).'%;"></div></div>
                                        <div class="review-num">'.shortNumStr($value['prodReviews']).' reviews</div>
                                    </div>
                                    <div class="user-action">
                                        <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                        <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                        <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                    </div>
                                    <div class="cart">
                                        <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                                <div class="content">
                                    <div class="title">
                                        <a href="">'.$value['prodTitle'].'</a>
                                    </div>
                                    <div class="price">
                                        <i class="fa fa-dollar"></i>'.$value['newPrice'].'
                                    </div>
                                    <div class="f-clear"></div>
                                    <div class="info">
                                        <div class="tags">BY <strong><a href="#" class="font-color1">'.$value['brandName'].'</a></strong></div>
                                        <div class="stats show-stat">
                                            <div class="likes" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['prodLikes']).'</div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['prodCollects']).'</div>
                                        </div>
                                    </div>
                                </div>
                            </article>';
                        }
                        ?>
                    </div>

                    <div class="controls">
                        <a class="scroll-status">
                            <i class="fa fa-exclamation-circle"></i><span></span>
                        </a>
                    </div>
                    <!--<div class="controls">
                        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
                    </div>-->
                    <?php echo $this->pageHtml; ?>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->

