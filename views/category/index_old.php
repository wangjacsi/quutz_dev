
        <!-- CONTENT BEGIN -->
        <div id="content" class="">
            <div class="inner">

                <div id="title-brands" class="block_general_title_1 w_margin_1 brands">
                    <h1 id="sel-title"><?php echo $this->mainTitle; ?></h1>
                    <h2 id="sel-subtitle"><?php echo $this->subTitle; ?></h2>
                    <div class="intro-filter">
                        <a onclick="introBrands('0')" class="general_colored_button intro-b0" title="Today's brands">Brands</a>
                        <a onclick="introBrands('1')" class="general_colored_button intro-b1" title="Hot Items">HOT</a>
                        <a onclick="introBrands('2')" class="general_colored_button intro-b2" title="New Items">NEW</a>
                        <a onclick="introBrands('3')" class="general_colored_button intro-b3" title="Popular Celebs">CELEBS</a>
                    </div>
                </div>

                <div id="content-brands" class="block_posts type_2 brands">
                    <?php
                    foreach ($this->popularBrandInfo as $key => $value) {
                        echo '<article class="post_type_4">
                                    <div class="feature pic4">
                                        <div class="image">
                                            <a href=""><img class="radius-lt" src="'.$this->popularBrandItems[$key][0]['prodPhotoPath'].$this->popularBrandItems[$key][0]['prodPhotoName'].'" alt=""></a>
                                            <a class="m-left2" href=""><img class="radius-rt" src="'.$this->popularBrandItems[$key][1]['prodPhotoPath'].$this->popularBrandItems[$key][1]['prodPhotoName'].'" alt=""></a>
                                            <a class="m-top2" href=""><img class="radius-lb" src="'.$this->popularBrandItems[$key][2]['prodPhotoPath'].$this->popularBrandItems[$key][2]['prodPhotoName'].'" alt=""></a>
                                            <a class="m-top2 m-left2" href=""><img class="radius-rb" src="'.$this->popularBrandItems[$key][3]['prodPhotoPath'].$this->popularBrandItems[$key][3]['prodPhotoName'].'" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="info">
                                            <div class="avatar">
                                                <a href="#"><img src="'.$value['brandPath'].$value['brandLogo'].'" alt=""></a>
                                            </div>
                                            <div class="brand-info">
                                                <div class="tags"><strong><a href="#" class="font-color1">'.$value['brandName'].'</a></strong></div>
                                                <div class="stats show-stat">
                                                    <div class="items" title="Items"><i class="fa fa-th-large"></i>'.shortNumStr($value['brandItems']).'</div>
                                                    <div class="likes" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['brandLikes']).'</div>
                                                    <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['brandCollects']).'</div>
                                                </div>
                                                <div class="tag-info">
                                                    <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                                    <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                                    <a href="#">Photography</a>
                                                    <a href="#">Photography</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>';
                    } ?>
                    <!--
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="<?php echo $this->popularBrandItems[0][0]['prodPhotoPath'].$this->popularBrandItems[0][0]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="<?php echo $this->popularBrandItems[0][1]['prodPhotoPath'].$this->popularBrandItems[0][1]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="<?php echo $this->popularBrandItems[0][2]['prodPhotoPath'].$this->popularBrandItems[0][2]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="<?php echo $this->popularBrandItems[0][3]['prodPhotoPath'].$this->popularBrandItems[0][3]['prodPhotoName']; ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->popularBrandInfo[0]['brandPath'].$this->popularBrandInfo[0]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->popularBrandInfo[0]['brandName']; ?></a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i><?php echo shortNumStr($this->popularBrandInfo[0]['brandItems']); ?></div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->popularBrandInfo[0]['brandLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->popularBrandInfo[0]['brandCollects']); ?></div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">premdfsdfsdfdfsfsfsdfium</a>
                                        <a href="#" title="Jeans">jedsfsdfsdfsdfdsfdans</a>
                                        <a href="#">Photography</a>
                                        <a href="#">Photography</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="<?php echo $this->popularBrandItems[1][0]['prodPhotoPath'].$this->popularBrandItems[1][0]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="<?php echo $this->popularBrandItems[1][1]['prodPhotoPath'].$this->popularBrandItems[1][1]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="<?php echo $this->popularBrandItems[1][2]['prodPhotoPath'].$this->popularBrandItems[1][2]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="<?php echo $this->popularBrandItems[1][3]['prodPhotoPath'].$this->popularBrandItems[1][3]['prodPhotoName']; ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->popularBrandInfo[1]['brandPath'].$this->popularBrandInfo[1]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->popularBrandInfo[1]['brandName']; ?></a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i><?php echo shortNumStr($this->popularBrandInfo[1]['brandItems']); ?></div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->popularBrandInfo[1]['brandLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->popularBrandInfo[1]['brandCollects']); ?></div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">indigo</a>
                                        <a href="#" title="Jeans">distiction</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="<?php echo $this->popularBrandItems[2][0]['prodPhotoPath'].$this->popularBrandItems[2][0]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="<?php echo $this->popularBrandItems[2][1]['prodPhotoPath'].$this->popularBrandItems[2][1]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="<?php echo $this->popularBrandItems[2][2]['prodPhotoPath'].$this->popularBrandItems[2][2]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="<?php echo $this->popularBrandItems[2][3]['prodPhotoPath'].$this->popularBrandItems[2][3]['prodPhotoName']; ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->popularBrandInfo[2]['brandPath'].$this->popularBrandInfo[2]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->popularBrandInfo[2]['brandName']; ?></a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i><?php echo shortNumStr($this->popularBrandInfo[2]['brandItems']); ?></div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->popularBrandInfo[2]['brandLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->popularBrandInfo[2]['brandCollects']); ?></div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="feature pic4">
                            <div class="image">
                                <a href=""><img class="radius-lt" src="<?php echo $this->popularBrandItems[3][0]['prodPhotoPath'].$this->popularBrandItems[3][0]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-left2" href=""><img class="radius-rt" src="<?php echo $this->popularBrandItems[3][1]['prodPhotoPath'].$this->popularBrandItems[3][1]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2" href=""><img class="radius-lb" src="<?php echo $this->popularBrandItems[3][2]['prodPhotoPath'].$this->popularBrandItems[3][2]['prodPhotoName']; ?>" alt=""></a>
                                <a class="m-top2 m-left2" href=""><img class="radius-rb" src="<?php echo $this->popularBrandItems[3][3]['prodPhotoPath'].$this->popularBrandItems[3][3]['prodPhotoName']; ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->popularBrandInfo[3]['brandPath'].$this->popularBrandInfo[3]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->popularBrandInfo[3]['brandName']; ?></a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="items" title="Items"><i class="fa fa-th-large"></i><?php echo shortNumStr($this->popularBrandInfo[3]['brandItems']); ?></div>
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->popularBrandInfo[3]['brandLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->popularBrandInfo[3]['brandCollects']); ?></div>
                                    </div>
                                    <div class="tag-info">
                                        <a href="#" title="Premium jeans">Rhgwjdu</a>
                                        <a href="#" title="Jeans">showsdj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>-->
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-hot" class="block_posts type_2 brands">
                <!-- example of Price old and new -->
                    <?php foreach ($this->hottestItems as $key => $value) {
                        echo '<article class="post_type_4">
                                    <div class="product">
                                        <div class="feature">
                                            <div class="image">
                                                <a href=""><img class="radius5" src="'.$value['prodPhotoPath'].$value['prodPhotoName'].'"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                            </div>
                                            <div class="ribbon">
                                              <a href="#">SALE</a>
                                            </div>
                                            <div class="review">
                                                <div class="value" title="'.$value['prodRating'].'"><div style="width:'.setRatingStar($value['prodRating']).'%;"></div></div>
                                                <div class="review-num">'.shortNumStr($value['prodReviews']).' reviews</div>
                                            </div>
                                            <div class="user-action">
                                                <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                                <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                                <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                            </div>
                                            <div class="cart">
                                                <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="info">
                                            <div class="avatar">
                                                <a href="#"><img src="'.$value['brandPath'].$value['brandLogo'].'" alt=""></a>
                                            </div>
                                            <div class="brand-info">
                                                <div class="title-item"><a href="#">'.$value['prodTitle'].'</a></div>
                                                <div class="price-group">
                                                    <div class="price-old"><i class="fa fa-dollar"></i>'.$value['price'].'</div><br/>
                                                    <div class="price-new"><i class="fa fa-dollar"></i>'.$value['price'].'</div>
                                                </div>
                                                <div class="info-left">
                                                    <div class="tags"><strong><a href="#" class="font-color1">'.$value['brandName'].'</a></strong></div>
                                                    <br/>
                                                    <div class="stats show-stat m-top2">
                                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i>'.shortNumStr($value['prodLikes']).'</div>
                                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>'.shortNumStr($value['prodCollects']).'</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>';
                    }
                    ?>
                    <!--<article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->hottestItems[0]['prodPhotoPath'].$this->hottestItems[0]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="ribbon">
                                  <a href="#">SALE</a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->hottestItems[0]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->hottestItems[0]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->hottestItems[0]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->hottestItems[0]['brandPath'].$this->hottestItems[0]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->hottestItems[0]['prodTitle']; ?></a></div>
                                    <div class="price-group">
                                        <div class="price-old"><i class="fa fa-dollar"></i><?php echo $this->hottestItems[0]['price']; ?></div><br/>
                                        <div class="price-new"><i class="fa fa-dollar"></i><?php echo $this->hottestItems[0]['price']; ?></div>
                                    </div>
                                    <div class="info-left">
                                        <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->hottestItems[0]['brandName']; ?></a></strong></div>
                                        <br/>
                                        <div class="stats show-stat m-top2">
                                            <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->hottestItems[0]['prodLikes']); ?></div>
                                            <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->hottestItems[0]['prodCollects']); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->hottestItems[1]['prodPhotoPath'].$this->hottestItems[1]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->hottestItems[1]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->hottestItems[1]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->hottestItems[1]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->hottestItems[1]['brandPath'].$this->hottestItems[1]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->hottestItems[1]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->hottestItems[1]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->hottestItems[1]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->hottestItems[1]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->hottestItems[1]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->hottestItems[2]['prodPhotoPath'].$this->hottestItems[2]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->hottestItems[2]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->hottestItems[2]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->hottestItems[2]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->hottestItems[2]['brandPath'].$this->hottestItems[2]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->hottestItems[2]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->hottestItems[2]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->hottestItems[2]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->hottestItems[2]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->hottestItems[2]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->hottestItems[3]['prodPhotoPath'].$this->hottestItems[3]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->hottestItems[3]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->hottestItems[3]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->hottestItems[3]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->hottestItems[3]['brandPath'].$this->hottestItems[3]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->hottestItems[3]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->hottestItems[3]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->hottestItems[3]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->hottestItems[3]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->hottestItems[3]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>-->
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-new" class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->newItems[0]['prodPhotoPath'].$this->newItems[0]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->newItems[0]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->newItems[0]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->newItems[0]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->newItems[0]['brandPath'].$this->newItems[0]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->newItems[0]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->newItems[0]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->newItems[0]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>-->
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->newItems[0]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->newItems[0]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->newItems[1]['prodPhotoPath'].$this->newItems[1]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->newItems[1]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->newItems[1]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->newItems[1]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->newItems[1]['brandPath'].$this->newItems[1]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->newItems[1]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->newItems[1]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->newItems[1]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items" title="Items"><i class="fa fa-th-large"></i>2223</div>-->
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->newItems[1]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->newItems[1]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->newItems[2]['prodPhotoPath'].$this->newItems[2]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->newItems[2]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->newItems[2]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->newItems[2]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->newItems[2]['brandPath'].$this->newItems[2]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->newItems[2]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->newItems[2]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->newItems[2]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items" title="Items"><i class="fa fa-th-large"></i>2223</div>-->
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->newItems[2]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->newItems[2]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->newItems[3]['prodPhotoPath'].$this->newItems[3]['prodPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->newItems[3]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->newItems[3]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->newItems[3]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->newItems[3]['brandPath'].$this->newItems[3]['brandLogo']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->newItems[3]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->newItems[3]['brandName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->newItems[3]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <!--<div class="items" title="Items"><i class="fa fa-th-large"></i>2223</div>-->
                                        <div class="likes m-zero" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->newItems[3]['prodLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->newItems[3]['prodCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div id="content-celebs" class="block_posts type_2 brands">
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->celebItems[0]['reviewPath'].$this->celebItems[0]['reviewPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->celebItems[0]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->celebItems[0]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->celebItems[0]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->celebItems[0]['celebPath'].$this->celebItems[0]['celebPhotoName']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->celebItems[0]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->celebItems[0]['celebName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->celebItems[0]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->celebItems[0]['reviewLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->celebItems[0]['reviewCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->celebItems[1]['reviewPath'].$this->celebItems[1]['reviewPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->celebItems[1]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->celebItems[1]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->celebItems[1]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->celebItems[1]['celebPath'].$this->celebItems[1]['celebPhotoName']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->celebItems[1]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->celebItems[1]['celebName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->celebItems[1]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->celebItems[1]['reviewLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->celebItems[1]['reviewCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->celebItems[2]['reviewPath'].$this->celebItems[2]['reviewPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->celebItems[2]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->celebItems[2]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->celebItems[2]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->celebItems[2]['celebPath'].$this->celebItems[2]['celebPhotoName']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->celebItems[2]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->celebItems[2]['celebName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->celebItems[2]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->celebItems[2]['reviewLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->celebItems[2]['reviewCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post_type_4">
                        <div class="product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img class="radius5" src="<?php echo $this->celebItems[3]['reviewPath'].$this->celebItems[3]['reviewPhotoName']; ?>"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="<?php echo $this->celebItems[3]['prodRating'];?>"><div style="width:<?php echo setRatingStar($this->celebItems[3]['prodRating']);?>%;"></div></div>
                                    <div class="review-num"><?php echo shortNumStr($this->celebItems[3]['prodReviews']);?> reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="info">
                                <div class="avatar">
                                    <a href="#"><img src="<?php echo $this->celebItems[3]['celebPath'].$this->celebItems[3]['celebPhotoName']; ?>" alt=""></a>
                                </div>
                                <div class="brand-info">
                                    <div class="title-item"><a href="#"><?php echo $this->celebItems[3]['prodTitle']; ?></a></div>
                                    <div class="tags"><strong><a href="#" class="font-color1"><?php echo $this->celebItems[3]['celebName']; ?></a></strong></div>
                                    <div class="price"><i class="fa fa-dollar"></i><?php echo $this->celebItems[3]['price']; ?></div>
                                    <div class="stats show-stat m-top2">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i><?php echo shortNumStr($this->celebItems[3]['reviewLikes']); ?></div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i><?php echo shortNumStr($this->celebItems[3]['reviewCollects']); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="clearboth"></div>
                    <div class="line_1"></div>
                </div>

                <div class="block_general_title_1 w_margin_1 m-top20">
                    <h1><?php echo $this->catNameForPage; ?> items<?php if($this->subCatNameForPage!='') echo ' - '.$this->subCatNameForPage;?></h1>
                    <h2>ENJOY THE BEAUTIFUL ITEMS HERE!</h2>
                </div>

                <div class="block_posts type_1 type_sort general_not_loaded">
                    <div class="posts">
                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/041/0/6416669/il_570xN.587135873_p4mx.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:30%;"></div></div>
                                    <div class="review-num">345456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Product name is djks sads sa sd</a>
                                </div>
                                <div class="price" style="color:#999999;font-weight:normal;text-decoration:line-through;">
                                    <i class="fa fa-dollar"></i>1517.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">AEEDFEsdasdadadsdadR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>15K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/027/1/7789766/il_340x270.628067245_abm6.jpg"><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">456 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">PGJ sjsksaiis jean</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>87.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">ABC 189</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>415K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>3242</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/025/0/8925614/il_570xN.543457281_tgit.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">156 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">fgf fg fgdads sadda sdas d dasd ksaiis jean</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>37.78
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">57HRFG</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>5K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/039/0/5631970/il_300x300.631159389_4kpi.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:90%;"></div></div>
                                    <div class="review-num">556 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">TU fhns jsjdh jsdh ds sds </a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>55
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">OUTSTANDING</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>35K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>1342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="//resize-ec1.thefancy.com/resize/crop/313/thingd/default/668048234370832136_af3cd9d0be32.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:85%;"></div></div>
                                    <div class="review-num">2.5K reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Ejhd fsdi isdj sjdj dk spkd s</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>39.99
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">SHOP 365</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>335K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>4342</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img1.etsystatic.com/037/0/7972092/il_570xN.605326585_sfzm.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:70%;"></div></div>
                                    <div class="review-num">23 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">rhs hdhs hshd h sds sda</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>224.74
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">CARTER GOU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>3235K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>842</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <!--<article class="post_type_1">
                            <div class="feature">
                                <div class="slider">
                                    <a href=""><span class="cat-name"></span></a>
                                    <div id="post_slider_1" class="flexslider">
                                        <ul class="slides">
                                            <li><img src="https://img1.etsystatic.com/035/0/6414460/il_340x270.628381141_iay4.jpg" alt=""></li>
                                            <li><img src="https://img1.etsystatic.com/043/2/8157280/il_340x270.586338037_b3t8.jpg" alt=""></li>
                                            <li><img src="https://img0.etsystatic.com/032/0/9722637/il_224xN.629185334_469v.jpg" alt=""></li>
                                        </ul>
                                        <span class="hover"></span>
                                    </div>
                                    <div class="review">
                                        <div class="num">4.9</div>
                                    </div>
                                    <script type="text/javascript">
                                        jQuery(function() {
                                            init_post_slider_1('#post_slider_1');
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="content">
                                <div class="info">
                                    <div class="tags">brand <strong><a href="" class="font-color1">Bdhrh 198</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes">7895</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>57</div>
                                    </div>
                                </div>
                                <div class="title">
                                    <a href="">Cute and sdksk.</a>
                                </div>
                            </div>
                        </article>-->

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="http://thefancy-media-ec5.thefancy.com/310/20140110/536673437096089618_bdfc848dfbdc.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:95%;"></div></div>
                                    <div class="review-num">7 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">T-shirt hds shs dah shsd dshas d</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>25
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">DDR CHAMPS</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>5K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>42</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/017/1/5322868/il_224xN.475417888_sfzn.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:86%;"></div></div>
                                    <div class="review-num">76 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">Beautiful care rice dd cook</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>37.8
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">CCURE 54 GOU</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>35K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>42</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post_type_1 product">
                            <div class="feature">
                                <div class="image">
                                    <a href=""><img src="https://img0.etsystatic.com/042/0/6122716/il_570xN.618992274_48ry.jpg" alt=""><span class="hover no_icon"></span><span class="overay2 hover no_icon"></span></a>
                                </div>
                                <div class="review">
                                    <div class="value" title="Rating 4.7"><div style="width:60%;"></div></div>
                                    <div class="review-num">74 reviews</div>
                                </div>
                                <div class="user-action">
                                    <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>
                                    <a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>
                                </div>
                                <div class="cart">
                                    <a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="">SO OS jdj Beautiful care rice dd cook</a>
                                </div>
                                <div class="price">
                                    <i class="fa fa-dollar"></i>65
                                </div>
                                <div class="f-clear"></div>
                                <div class="info">
                                    <div class="tags">BY <strong><a href="#" class="font-color1">VIMEO FFR</a></strong></div>
                                    <div class="stats show-stat">
                                        <div class="likes" title="Likes"><i class="fa fa-heart"></i>935K</div>
                                        <div class="comments" title="Collections"><i class="fa fa-cube"></i>402</div>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>

                    <div class="controls">
                        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
                    </div>
                </div>

            </div>
        </div>
        <!-- CONTENT END -->

