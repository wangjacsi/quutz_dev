<!DOCTYPE html>
<html>

<head>
<title>QuuTz</title>

<meta name="keywords" content="">
<meta name="description" content="">

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!--[if lt IE 9]>
<script type="text/javascript" src="layout/plugins/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="<?php echo PUBLIC_PATH; ?>font-awesome-4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo PUBLIC_PATH; ?>layout/style.css" type="text/css">

<script type="text/javascript" src="<?php echo PUBLIC_PATH; ?>layout/js/jquery.js"></script>
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>-->
<!-- for variables javascript -->
<script type="text/javascript" src="<?php echo PUBLIC_PATH; ?>js/config.js"></script>
<script type="text/javascript" src="<?php echo PUBLIC_PATH; ?>layout/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo PUBLIC_PATH; ?>layout/js/main.js"></script>


<!-- For modal -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH; ?>modal/css/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH; ?>modal/css/component.css" />
<script src="<?php echo PUBLIC_PATH; ?>modal/js/modernizr.custom.js"></script>-->

<link rel="stylesheet" href="<?php echo PUBLIC_PATH; ?>modal2/modal.css">
<!--[if lt IE 9]>
    <script src="<?php echo PUBLIC_PATH; ?>modal2/test/lib/html5shiv.js"></script>
    <script src="<?php echo PUBLIC_PATH; ?>modal2/test/lib/bean.js"></script>
<![endif]-->
<?php
if(is_array($this->css) &&  !empty($this->css)){
    foreach($this->css as $css){
        echo '<link rel="stylesheet" href="'.URL .$css.'" />';
    }
}
?>


<script src="<?php echo PUBLIC_PATH; ?>js/config.js"></script>
<script src="<?php echo PUBLIC_PATH; ?>js/common.js"></script>
<script src="<?php echo PUBLIC_PATH; ?>js/ajax.js"></script>
<script src="<?php echo PUBLIC_PATH; ?>js/debug.js"></script>
<script src="<?php echo PUBLIC_PATH; ?>js/main.js"></script>

<?php
// support common basic javascript
if(is_array($this->js) &&  !empty($this->js)){
    foreach($this->js as $js){
        echo '<script type="text/javascript" src="' . URL . $js . '"></script>';
    }
}
?>

</head>

<body class="sticky_footer">
    <div class="wrapper">
        <!-- HEADER BEGIN -->
        <header>
            <div id="header">
                <?php include_once 'topmenu.php';
                    include_once 'topbottom.php';
                    if($this->topMiddleUse == 1)
                        include_once 'topmiddleBrand.php';
                    else if($this->topMiddleUse == 2)
                        include_once 'topmiddleUser.php'
                    /*if($this->page=='product' || $this->page=='storeproduct' || $this->page=='storeinfo'){
                        include_once 'topmiddleUser.php';
                    } else {

                    }*/
                ?>
            </div>
        </header>
        <!-- HEADER END -->