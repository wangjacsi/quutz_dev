               <?php include_once 'modalSignup.php'; ?>
               <?php include_once 'modalLogin.php'; ?>
                <section class="top ">
                    <div class="inner">
                        <div class="block_top_menu">
                            <nav>
                                <ul>
                                    <li class=""><a href="<?php echo URL; ?>"><img src="<?php echo PUBLIC_PATH; ?>images/logo/Untitled-1.png" alt="QuuTz" title="QuuTz"></a></li>
                                    <li><a href="">CATEGORIES</a>
                                        <ul>
                                            <?php global $HOMEMENU;
                                                foreach ($HOMEMENU['category'] as $key => $menu){
                                                    $tempMenu = $key;
                                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                                    echo '<li><a href="'.URL.'category/'.$key.'">'.$tempMenu.'</a></li>';
                                                }
                                            ?>
                                        </ul>
                                    </li>
                                    <li><a href="">Brands</a>
                                        <ul>
                                            <?php
                                                foreach ($HOMEMENU['brands'] as $key => $menu){
                                                    $tempMenu = $key;
                                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                                    echo '<li><a href="'.URL.'brands/'.$key.'">'.$tempMenu.'</a></li>';
                                                }
                                            ?>
                                        </ul>
                                    </li>
                                    <li><a href="">Celebs</a>
                                        <ul>
                                            <?php
                                                foreach ($HOMEMENU['celebs'] as $key => $menu){
                                                    $tempMenu = $key;
                                                    $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                                    $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                                    echo '<li><a href="'.URL.'celebs/'.$key.'">'.$tempMenu.'</a></li>';
                                                }
                                            ?>
                                        </ul>
                                    </li>
                                    <?php if($this->login->userOk!=true){
                                            //echo '<li><a class="md-trigger" data-modal="modal-1">+Signup</a></li><li><a href="shortcodes.html">Login</a></li>';
                                            echo '<li><a href="#modal-show"><i class="fa fa-plus"></i> Signup</a></li><li><a href="#modal-login"><i class="fa fa-sign-in"></i> Login</a></li>';
                                        } else {
                                            echo '<li><a href="">My</a><ul class="my">';
                                            foreach ($HOMEMENU['my'] as $key => $menu){
                                                $tempMenu = $key;
                                                $tempMenu = preg_replace('/_and_/', ' & ', $tempMenu);
                                                $tempMenu = preg_replace('/_/', ' ', $tempMenu);
                                                echo '<li><a href="'.URL.'my/'.$menu.'">'.$tempMenu.'</a></li>';
                                            }
                                            echo '<li><a href="'.URL.'logout"><i class="fa fa-sign-out"></i> logout</a></li></ul></li>';
                                        }
                                    ?>
                                    <li><a href=""><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                </ul>
                            </nav>
                        </div>

                        <div class="block_search">
                            <input type="text" class="w_focus_mark">
                        </div>

                        <div class="block_top_social">
                            <ul class="general_social_1">
                                <li><a href="#" class="social_1">Twitter</a></li>
                                <li><a href="#" class="social_2">Facebook</a></li>
                                <li><a href="#" class="social_3">Vimeo</a></li>
                                <li><a href="#" class="social_4">Pinterest</a></li>
                                <li><a href="#" class="social_5">Instagram</a></li>
                            </ul>
                        </div>

                        <div class="clearboth"></div>
                    </div>
                </section>
                <div class="menu-padding"></div>