        <section style="margin-top:30px;" class="modal--show" id="modal-show" tabindex="-1" role="dialog" aria-labelledby="label-show" aria-hidden="true">
            <div class="modal-inner">
                <header>
                    <h2 id="label-show">Welcome to QuuTz</h2>
                    <div class="modal_social_2">
                        <ul>
                            <li title="Twitter"><a href="#" class="social-link--facebook">Facebook</a></li>
                            <li title="Twitter"><a href="#" class="social-link--twitter">Twitter</a></li>
                            <li title="Twitter"><a href="#" class="social-link--google-plus">Google+</a></li>
                            <!--<li title="Twitter"><a href="#" class="social-link--facebook">Twitter</a></li>-->
                            <!--<li title="Facebook"><a href="#" class="social-small social_2">Facebook</a></li>
                            <li title="Google+"><a href="#" class="social-small social_4">Google+</a></li>
                            <li title="Instagram"><a href="#" class="social-small social_5">Instagram</a></li>-->
                        </ul>
                        <div>&nbsp;&nbsp;&nbsp;Sign up using other services</div>
                    </div>
                    <div class="float-clear"></div>
                </header>
                <div class="modal-content">
                    <div class="modal-form">
                        <form action="#" id="signup_form">
                            <!--<div class="text">
                                <p>Enterprise includes a built-in AJAX contact form with an anti-spam filter. Display your contact form anywhere on your site using the contact-form shortcode.</p>
                            </div>-->

                            <div class="form">
                                <div class="label">Username <span>*</span>&nbsp;&nbsp;<span id="namestatus"></span></div>
                                <div class="field"><input type="text" name="username" id="username"  class="to_send w_focus_mark w_validation required" onblur="checkusername()" onkeyup="restrict('username')" onfocus="emptyElement('namestatus')" maxlength="20"></div>

                                <div class="label">E-mail <span>*</span>&nbsp;&nbsp;<span id="emailstatus"></span></div>
                                <div class="field"><input type="email" name="email" id="email" class="to_send w_focus_mark w_validation required email" onblur="validateEmail()" onfocus="emptyElement('emailstatus')" maxlength="50"></div>

                                <div class="label">Password <span>*</span>&nbsp;&nbsp;<span id="pass1status"></span></div>
                                <div class="field"><input type="password" name="pass1" id="pass1" class="to_send w_focus_mark w_validation required" onblur="checkpassword1()" onfocus="emptyElement('pass1status')" maxlength="20"></div>

                                <div class="label">Confirm Password <span>*</span>&nbsp;&nbsp;<span id="pass2status"></span></div>
                                <div class="field"><input type="password" name="pass2" id="pass2" class="to_send w_focus_mark w_validation required" onblur="checkpassword2()" onfocus="emptyElement('pass2status')" maxlength="20"></div>

                                <div class="label">Insert Characters <span>*</span>&nbsp;&nbsp;<span id="captchastatus"></span></div>
                                <div class="captcha-group">
                                    <div class="captcha">
                                        <input type="text" name="captcha" id="captcha" class="to_send w_focus_mark w_validation required" onkeyup="restrict('captcha')"  placeholder="" maxlength="6">
                                    </div>
                                    <img id="captref" src="<?php echo URL; ?>libs/func/captcha.php" style="float:left;"/>
                                    <a onclick="captrefresh()" class="general_colored_button navy" style="cursor: pointer; float:left;">Refresh</a>
                                </div>
                                <div class="float-clear"></div>
                                <!--<div class="label">About <span class="op">(Option)</span></div>
                                <div class="textarea"><textarea name="message" class="to_send w_focus_mark w_validation" cols="1" rows="1"></textarea></div>

                                <div class="label">First Name <span>*</span></div>
                                <div class="field"><input type="text" name="firstname" class="to_send w_focus_mark w_validation required"></div>
                                <div class="label">Last Name <span>*</span></div>
                                <div class="field"><input type="text" name="lastname" class="to_send w_focus_mark w_validation required"></div>

                                <div id="gender-group" class="inline-group">
                                    <input id="female" type="radio" name="gender" value="female" tabindex="1" checked="checked">
                                    <label for="female" class="label">Female</label>

                                    <input id="male" type="radio" name="gender" value="male" tabindex="2">
                                    <label for="male" class="label">Male</label>

                                    <input id="private" type="radio" name="gender" value="private" tabindex="3">
                                    <label for="private" class="label">Private</label>
                                </div>-->
                                <p class="fineprint">By clicking SIGN UP, you confirm that you accept our <a target="_blank" href="/terms_of_use.php?ref=reg" title="Terms of Use"><strong>Terms of Use</strong></a> and <a target="_blank" href="/help/article/480?ref=reg" title="Privacy Policy"><strong>Privacy Policy</strong></a>.</p>
                                <div id="resultStatus"></div>
                                <div class="button"><a href="#" class="general_colored_button navy signup-submit">Sign Up</a></div>
                            </div>
                        </form>
                    </div>

                </div>
                <!--<footer>
                    <div class="button"><a href="#" class="general_button_type_3 submit">Send message</a></div>
                    <p>Footer</p>
                    </form>
                </footer>-->
            </div>
            <a href="#!" class="modal-close" title="Close this modal" data-dismiss="modal" data-close="Close">&times;</a>
        </section>