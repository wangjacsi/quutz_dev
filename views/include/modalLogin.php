        <section style="margin-top:30px;" class="modal--show" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="label-show" aria-hidden="true">
            <div class="modal-inner">
                <header>
                    <h2 id="label-show">Login to QuuTz</h2>
                    <div class="modal_social_2">
                        <ul>
                            <li title="Twitter"><a href="#" class="social-link--facebook">Facebook</a></li>
                            <li title="Twitter"><a href="#" class="social-link--twitter">Twitter</a></li>
                            <li title="Twitter"><a href="#" class="social-link--google-plus">Google+</a></li>
                        </ul>
                        <div>&nbsp;&nbsp;&nbsp;Login using other services</div>
                    </div>
                    <div class="float-clear"></div>
                </header>
                <div class="modal-content">
                    <div class="modal-form">
                        <form action="#" id="login_form">
                            <div class="form">
                                <div class="label">Username or Email <span>*</span></div>
                                <div class="field">
                                    <?php Session::start(); if(isset($_COOKIE['rem']) && $_COOKIE['rem']==1)
                                        echo '<input type="text" name="loginname" id="loginname"  class="to_send w_focus_mark w_validation required" onkeyup="restrict(\'username\')" onfocus="emptyElement(\'loginStatus\')" onKeyPress="loginEnter(event, this)" maxlength="20" value="'.$_COOKIE['remin'].'">';
                                        else echo '<input type="text" name="loginname" id="loginname"  class="to_send w_focus_mark w_validation required" onkeyup="restrict(\'username\')" onfocus="emptyElement(\'loginStatus\')" onKeyPress="loginEnter(event, this)" maxlength="20">';
                                    ?>
                                </div>
                                <div class="label">Password <span>*</span></div>
                                <div class="field"><input type="password" name="loginpass" id="loginpass" class="to_send w_focus_mark w_validation required" onfocus="emptyElement('loginStatus')" onKeyPress="loginEnter(event, this)" maxlength="20"></div>

                                <div style="margin-top:17px; float:left;">
                                    <?php if(isset($_COOKIE['rem']) && $_COOKIE['rem']==1)
                                        echo '<input type="checkbox" name="remember" id="remember" checked="checked">';
                                        else echo '<input type="checkbox" name="remember" id="remember">';
                                    ?>
                                </div>
                                <div style="margin-top:18px;">
                                <strong>&nbsp;&nbsp;Remember me</strong>
                                </div>

                                <div class="float-clear"></div>

                                <div id="loginStatus" style="margin-top:15px;"></div>
                                <div class="button"><a href="#" class="general_colored_button navy login-submit">Login</a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <a href="#!" class="modal-close" title="Close this modal" data-dismiss="modal" data-close="Close">&times;</a>
        </section>