                <div class="inner">
                    <div class="user-info">
                        <div class="block_about_author_post">
                        <div class="photo"><a href="#"><img src="<?php echo URL.'public/images/users/user1.jpg'; ?>" alt=""></a></div>

                        <div class="content">
                            <div class="name">
                                <a href="#">Clark Gordon</a>
                            </div>
                            <div class="brand-menu">
                                <a href="<?php echo URL.'my';?>" class="feed general_colored_button default" title="feed"><i class="fa fa-rss"></i></a>
                                <a href="<?php echo URL.'my/shops';?>" class="shops general_colored_button default" title="favorite shops"><i class="fa fa-heart"></i></a>
                                <a href="<?php echo URL.'my/celebs';?>" class="celebs general_colored_button default" title="favorite celebs"><i class="fa fa-child"></i></a>
                                <a href="<?php echo URL.'my/collections';?>" class="collections general_colored_button default" title="collections"><i class="fa fa-cube"></i></a>
                                <a href="<?php echo URL.'my/wish';?>" class="wish general_colored_button default" title="wish list"><i class="fa fa-star"></i></a>
                                <a href="<?php echo URL.'my/contact';?>" class="contact general_colored_button default" title="contact"><i class="fa fa-comments-o"></i></a>
                                <a href="<?php echo URL.'my/purchase';?>" class="purchase general_colored_button default" title="purchase"><i class="fa fa-credit-card"></i></a>
                                <a href="<?php echo URL.'my/info';?>" class="info general_colored_button default" title="information"><i class="fa fa-info"></i></a>
                                <a href="<?php echo URL.'my/setting';?>" class="setting general_colored_button default" title="setting"><i class="fa fa-cogs"></i></a>
                            </div>
                            <div class="stats show-stat">
                                <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                <div class="follower" title="Followers"><i class="fa fa-group"></i>333</div>
                                <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                            </div>
                            <div class="f-clear"></div>
                            <div class="description">
                                <p><strong>we are making good materials and clothes and so on...</strong></p>
                                <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis. Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis. Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis. Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                            </div>

                            <div class="social">
                                <ul class="general_social_2">
                                    <li><a href="#" class="social_1">Twitter</a></li>
                                    <li><a href="#" class="social_2">Facebook</a></li>
                                    <li><a href="#" class="social_3">Pinterest</a></li>
                                    <li><a href="#" class="social_4">Google Plus</a></li>
                                    <li><a href="#" class="social_5">Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="user-act">
                            <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-pencil fa-fw"></i>&nbsp;&nbsp;Edit</a>
                            <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i>&nbsp;&nbsp;Favorite Celeb</a>
                        </div>
                    </div>
                    </div>
                </div>