                <div class="inner">
                    <div class="user-info">
                        <div class="block_about_author_post">
                        <div class="photo"><a href="#"><img src="http://thefancy-media-ec6.thefancy.com/commerce/200/20140707/1c844c6c46f1474999d90cf84cb7f5f2.jpg" alt=""></a></div>

                        <div class="content">
                            <div class="name">
                                <a href="#">Clark Gordon</a>
                            </div>
                            <div class="brand-menu">
                                <a href="<?php echo URL.'brands/store/storeID-storeName';?>" class="product general_colored_button default" title="Items"><i class="fa fa-th-large"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/info';?>" class="info general_colored_button default" title="Brand info"><i class="fa fa-info"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/contact';?>" class="contact general_colored_button default" title="Contact"><i class="fa fa-comments-o"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/celebs';?>" class="celebs general_colored_button default" title="Celebs"><i class="fa fa-child"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/followers';?>" class="followers general_colored_button default" title="Followers"><i class="fa fa-group"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/event';?>" class="event general_colored_button default" title="Event"><i class="fa fa-calendar"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/upload';?>" class="upload general_colored_button default" title="Upload"><i class="fa fa-upload"></i></a>
                                <a href="<?php echo URL.'brands/store/storeID-storeName/owner';?>" class="admin general_colored_button default" title="Store admin"><i class="fa fa-line-chart"></i></a>
                            </div>
                            <div class="stats show-stat">
                                <div class="items" title="Items"><i class="fa fa-th-large"></i>223</div>
                                <div class="likes" title="Likes"><i class="fa fa-heart"></i>123</div>
                                <div class="follower" title="Followers"><i class="fa fa-group"></i>333</div>
                                <div class="comments" title="Collections"><i class="fa fa-cube"></i>333</div>
                            </div>
                            <div class="f-clear"></div>
                            <div class="description">
                                <p><strong>we are making good materials and clothes and so on...</strong></p>
                                <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis. Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis. Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis. Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                            </div>

                            <div class="social">
                                <ul class="general_social_2">
                                    <li><a href="#" class="social_1">Twitter</a></li>
                                    <li><a href="#" class="social_2">Facebook</a></li>
                                    <li><a href="#" class="social_3">Pinterest</a></li>
                                    <li><a href="#" class="social_4">Google Plus</a></li>
                                    <li><a href="#" class="social_5">Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="user-act">
                            <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-pencil fa-fw"></i>&nbsp;&nbsp;Edit</a>
                            <a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i>&nbsp;&nbsp;Favorite shop</a>
                        </div>
                    </div>
                    </div>
                </div>