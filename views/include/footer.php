<!-- FOOTER BEGIN -->
        <footer>
            <div id="footer">
                <section class="top">
                    <div class="inner">
                        <div class="block_footer_widgets">
                            <div class="column">
                                <div class="block_footer_categories">
                                    <h3>Posts Categories</h3>

                                    <ul>
                                        <li><a href="category_photography.html">PHOTOGRAPHY</a></li>
                                        <li><a href="category_design.html">DESIGN</a></li>
                                        <li><a href="category_fashion.html">FASHION</a></li>
                                        <li><a href="category_reviews.html">REVIEWS</a></li>
                                        <li><a href="category_music.html">MUSIC</a></li>
                                        <li><a href="category_technology.html">TECHNOLOGY</a></li>
                                        <li><a href="category_life.html">LIFE</a></li>
                                        <li><a href="category_travel.html">TRAVEL</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="column">
                                <div class="block_footer_tags">
                                    <h3>Tags cloud</h3>

                                    <ul>
                                        <li><a href="#">Peoples</a></li>
                                        <li><a href="#">Design</a></li>
                                        <li><a href="#">Technology</a></li>
                                        <li><a href="#">Travel</a></li>
                                        <li><a href="#">Music</a></li>
                                        <li><a href="#">Photography</a></li>
                                        <li><a href="#">Fashion</a></li>
                                        <li><a href="#">Creative</a></li>
                                        <li><a href="#">Apple</a></li>
                                        <li><a href="#">News</a></li>
                                        <li><a href="#">Development</a></li>
                                    </ul>
                                </div>

                                <div class="block_footer_twitter">
                                    <h3>Twitter Widget</h3>

                                    <div id="tweets"></div>


                                </div>
                            </div>

                            <div class="column">
                                <div class="block_footer_pics">
                                    <h3>Instagram</h3>

                                    <ul>
                                        <li><a href="#"><img src="<?php echo PUBLIC_PATH; ?>images/pic_footer_1.jpg" alt=""><span class="hover"></span></a></li>
                                        <li><a href="#"><img src="<?php echo PUBLIC_PATH; ?>images/pic_footer_2.jpg" alt=""><span class="hover"></span></a></li>
                                        <li><a href="#"><img src="<?php echo PUBLIC_PATH; ?>images/pic_footer_3.jpg" alt=""><span class="hover"></span></a></li>
                                        <li><a href="#"><img src="<?php echo PUBLIC_PATH; ?>images/pic_footer_4.jpg" alt=""><span class="hover"></span></a></li>
                                        <li><a href="#"><img src="<?php echo PUBLIC_PATH; ?>images/pic_footer_5.jpg" alt=""><span class="hover"></span></a></li>
                                        <li><a href="#"><img src="<?php echo PUBLIC_PATH; ?>images/pic_footer_6.jpg" alt=""><span class="hover"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="middle">
                    <div class="inner">
                        <div class="block_bottom_menu">
                            <nav>
                                <ul>
                                    <li><a href="index.html">HOMEPAGE</a></li>
                                    <li><a href="blog_post.html">POSTS</a></li>
                                    <li><a href="category_photography.html">CATEGORIES</a></li>
                                    <li><a href="full_width.html">PAGES</a></li>
                                    <li><a href="shortcodes.html">FEATURES</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </section>

                <section class="bottom">
                    <div class="inner">
                        <div class="block_copyrights"><p>&copy; Copyright 2013 by WebLionMedia. All Rights Reserved.</p></div>
                    </div>
                </section>
            </div>
        </footer>
        <!-- FOOTER END -->
    </div>


    <!--<?php //if($this->page=='product'): ?>-->
    <script type="text/javascript">
        $(document).ready(function() {
            var arr = <?=json_encode($this->naviMenu)?>;
            navi = arr.slice(1);
            //debugObject(navi);
            //alert(navi);
            //alert(navi.length);
            if(navi.length > 0){
                selectMenu(navi[navi.length-1], navi.length-1);
            }
        });
    </script>
    <!--<?php //endif; ?>-->

    <?php
    // support common basic javascript
    if(is_array($this->jsFooter) &&  !empty($this->jsFooter)){
        foreach($this->jsFooter as $jsFooter){
            echo '<script type="text/javascript" src="' . URL . $jsFooter . '"></script>';
        }
    }
    ?>
    <!--<div class="md-overlay"></div>

        <script src="<?php echo PUBLIC_PATH; ?>modal/js/classie.js"></script>
        <script src="<?php echo PUBLIC_PATH; ?>modal/js/modalEffects.js"></script>

        <script>
            // this is important for IEs
            var polyfilter_scriptpath = PUBLIC_PATH+'modal/js/';
        </script>
        <script src="<?php echo PUBLIC_PATH; ?>modal/js/cssParser.js"></script>
        <script src="<?php echo PUBLIC_PATH; ?>modal/js/css-filters-polyfill.js"></script>-->

        <!-- JS for Modal -->
        <script src="<?php echo PUBLIC_PATH; ?>modal2/modal.js"></script>

        <!-- Plugins -->
        <script src="<?php echo PUBLIC_PATH; ?>modal2/plugins/html5video.js"></script>
        <script src="<?php echo PUBLIC_PATH; ?>modal2/plugins/modal-maxwidth.js"></script>
        <script src="<?php echo PUBLIC_PATH; ?>modal2/plugins/modal-resize.js"></script>
        <script src="<?php echo PUBLIC_PATH; ?>modal2/plugins/gallery.js"></script>

</body>

</html>