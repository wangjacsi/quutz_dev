var navi = new Array();
var path = URL;
var viewSubMenu;
var listPerPage = 12;

function naviArraySet(act, level){
    act = act.replace(' ', '_');
    navi[level] = act;
    path = URL;

    for(var i=navi.length; i>=0; i--){
        if(i>level)
            navi.splice(i,1);
    }
    for(var i=0; i < navi.length; i++){
        path +=navi[i]+'/';
    }
}

function naviArraySet2(act, act2, level){
    act = act.replace(' ', '_');
    act2 = act2.replace(' ', '_');
    navi[level] = act2;
    path = URL;

    for(var i=navi.length; i>=0; i--){
        if(i>level)
            navi.splice(i,1);
    }
    for(var i=0; i < navi.length; i++){
        path +=navi[i]+'/';
    }
    navi[level] = act;
}

function viewPathSubMenu(act, level){
    viewSubMenu = act;
    viewSubMenu = viewSubMenu.trim();
    viewSubMenu = viewSubMenu.replace('_and_', ' & ');
    viewSubMenu = viewSubMenu.replace('_', ' ');

    var selMenu = '<li id="level_'+level+'"><a href="'+path+'">'+viewSubMenu+'</a></li><li><a><i class="fa fa-caret-right"></i></a></li>';

    if(!$('.block_secondary_menu nav ul').children('#level_'+level).length >0){
        $('.block_secondary_menu nav ul').append(selMenu);
    } else{
        var str = $('.block_secondary_menu nav ul #level_'+level).index() - 1;
        $('.block_secondary_menu nav ul li:gt('+str+')').remove();
        $('.block_secondary_menu nav ul').append(selMenu);
        var str = $('.dropdown .specific-menu-1#level_'+level).index();
        $('.dropdown .specific-menu-1:gt('+(str)+')').remove();
    }
}

function searchBrand(sChar){
    //alert(sChar);
    var rx = new RegExp;
    var filterd;
    rx = /[^a-z]/gi;
    filterd = sChar.replace(rx, "");
    filterd = filterd.toLowerCase();
    naviArraySet(filterd, 2);
    viewPathSubMenu(filterd, 2);
    getDataAjax(2);
}

function selectMenu(act, level){
    naviArraySet(act, level);
    viewPathSubMenu(act, level);
    getDataAjax(level);
}

function myFunc(act, level){
    var act2 = '';
    if(act == 'favorite_shops'){
        act2 = 'shops';
    } else if(act == 'favorite_celebs'){
        act2 = 'celebs';
    } else if(act == 'wish_list'){
        act2 = 'wish';
    } else if(act == 'purchases_and_reviews'){
        act2 = 'purchase';
    } else if(act == 'account_settings'){
        act2 = 'settings';
    } else {
        act2 = act;
    }
    naviArraySet2(act, act2, level);
    viewPathSubMenu(act, level);
    getDataAjax(level);
}

function getDataAjax(level){
    var ajax = ajaxObj("POST", URL+"menuset/getmenu");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) == true) {
            var resp = ajax.responseText;
                resp = resp.trim();
            if(resp == 'level high'){

            } else{
                try {
                    var json = JSON.parse(ajax.responseText);
                } catch(e) {
                    alert('data is not json' + ajax.responseText);
                }
                //var json = eval('('+ajax.responseText+')');
                var subMenu = '';

                //for Search brands go here
                for(var i=0; i<navi.length; i++){
                    $('.dropdown .specific-menu-1#level_'+(i+1)).remove();
                    if(i==1 && navi[1]=='search'){
                        subMenu += '<div class="specific-menu-1" id="level_'+(i+1)+'"><ul>';
                        var cntJ = 0;
                        for(var j in json[navi[i]]){
                            var viewSubMenu2 = json[navi[i]][j];
                            cntJ++;
                            if(cntJ==5){
                                subMenu += '<li class="f-left" style="text-align:center; min-width:10px;"><a onclick="searchBrand(\''+viewSubMenu2+'\')">'+viewSubMenu2+'</a></li></ul><ul>';
                                cntJ = 0;
                            } else {
                                subMenu += '<li class="f-left" style="text-align:center; min-width:10px;"><a onclick="searchBrand(\''+viewSubMenu2+'\')">'+viewSubMenu2+'</a></li>';
                            }
                        }
                        subMenu += '</ul></div>';
                        //subMenu += '</ul><ul><li><a><i class="fa fa-angle-right"></i></a></li></ul></div>';
                    } else if(i==0 && navi[0]=='my'){ // for change url short
                        subMenu += '<div class="specific-menu-1" id="level_'+(i+1)+'"><ul>';
                        for(var j in json[navi[i]]){
                            var viewSubMenu2 = json[navi[i]][j];
                            viewSubMenu2 = viewSubMenu2.trim();
                            viewSubMenu2 = viewSubMenu2.replace('_and_', ' & ');
                            viewSubMenu2 = viewSubMenu2.replace('_', ' ');
                            subMenu += '<li><a onclick="myFunc(\''+json[navi[i]][j]+'\', \''+(i+1)+'\')">'+viewSubMenu2+'</a></li>';
                        }
                        subMenu += '</ul></div>';
                    } else{
                        var cntJ2 = 0;
                        if($.isArray(json[navi[i]])){
                            var lastPage = Math.ceil(json[navi[i]].length/listPerPage);
                            if(lastPage == 1){
                                var page = '';
                            } else{
                                var page = '<div class="submenu-page"><a class="button">1</a><a><i class="fa fa-angle-right"></i></a><a><i class="fa fa-angle-double-right"></i></a><a><i class="fa fa-ellipsis-h"></i></a>&nbsp;&nbsp;<a class="button">'+lastPage+'</a></div>';
                            }
                        } else {
                            var page = '';
                        }
                        var firstDisp = 'block';
                        for(var j in json[navi[i]]){
                            if(cntJ2 == 0){
                                subMenu += '<div class="specific-menu-1" id="level_'+(i+1)+'" style="display:'+firstDisp+'"><ul>';
                                if(firstDisp == 'block')
                                    firstDisp = 'none';
                            }
                            cntJ2++;
                            var viewSubMenu2 = json[navi[i]][j];//
                            viewSubMenu2 = viewSubMenu2.trim();
                            viewSubMenu2 = viewSubMenu2.replace('_and_', ' & ');
                            viewSubMenu2 = viewSubMenu2.replace('_', ' ');
                            if(cntJ2 == 12){
                                subMenu += '<li><a onclick="selectMenu(\''+json[navi[i]][j]+'\', \''+(i+1)+'\')">'+viewSubMenu2+'</a></li>';
                                subMenu += '</ul>'+page+'</div>';
                                cntJ2 = 0;
                            } else{
                                subMenu += '<li><a onclick="selectMenu(\''+json[navi[i]][j]+'\', \''+(i+1)+'\')">'+viewSubMenu2+'</a></li>';
                            }
                        }
                        if(cntJ2 != 0){
                            subMenu += '</ul></div>';
                        }
                    }

                }
                $('.dropdown').append(subMenu);
                // selected style set
                $( ".dropdown ul li a" ).css( "color", "#000" ).parent().css({"border":"1px solid #fff","-webkit-border-radius":"5px","-moz-border-radius":"5px","border-radius":"5px"});
                for(var i=0; i < navi.length; i++){
                    $(".dropdown #level_"+i+" ul li a").each(function() {
                        //alert(navi[i]);
                        var tempSelect = navi[i];
                        tempSelect = tempSelect.trim();
                        tempSelect = tempSelect.replace('_and_', ' & ');
                        tempSelect = tempSelect.replace('_', ' ');
                        if( ($(this).text() == tempSelect) )
                            $(this).css("color", "#53b7f9" ).parent().css({"border":"1px solid #ccc","-webkit-border-radius":"5px","-moz-border-radius":"5px","border-radius":"5px"});
                    });
                }
            }
        }
    }
    var data = {
        level : level,
        data : navi
    };
    //alert('Send Data : ' + data.data);
    ajax.send($.param(data));
}

function navigation(){
    //debugObject(navi);
    var path = URL;
    for(var i=0; i < navi.length; i++){
        path +=navi[i]+'/';
    }
    window.location = path;
}