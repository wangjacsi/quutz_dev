// JavaScript Document
// ******** ajax.js ********

function ajaxObj( meth, url ) {
    //var x = new XMLHttpRequest();
    if(window.XMLHttpRequest){
        var x = new window.XMLHttpRequest();
    } else {
        try {
            var x = new ActiveXObject('Msxml2.XMLHTTP');
        } catch(e){
            var x = new ActiveXObject('Microsoft', XMLHTTP);
        }
    }
    x.open( meth, url, true );
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    return x;
}
function ajaxReturn(x){
    if(x.readyState == 4 && x.status == 200){
        return true;
    }
}

function debugGetAjax(url){
    $.get(url, function(data){
        $('body').html(data);
    })
}

function debugPostAjax(url, param){
    //param -> {'id': id}
    $.post(url, param, function(data){
        $('body').html(data);
    })
}

function debugGetJSON(url){
    $.getJSON(url, function(data){
        $( "body" ).empty();
        $.each(data, function(key, value){
            $('body').append(key + ' : ' + value);
        })
    })
}
//we can use to transfer data to parameters for Post or Get
//1. $.param, 2. serialize, 3. serializeArray





