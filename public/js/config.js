var URL = 'http://localhost/quutz/';
var PUBLIC_PATH = 'http://localhost/quutz/public/';
var ASSETS_PATH = 'http://localhost/quutz/public/assets/';


function setBrandsPageVar(ind){
    if(ind==1){ // brands introduce grid change
        var brandsTitle = {
            today: ['Today\'s brands','Unique brands here! Find your style and Enjoy!'],
            favorite: ['Favorite brands','Popular brands here!'],
            newbrand:['New brands','Additional brands come to us!']
        }
        return brandsTitle;
    } else if(ind==2) { // Category's intro grid
        var titles = {
            brands: ['Favorite brands','Unique brands here! Find your style and Enjoy!'],
            hot: ['Hot Items','Popular items here!'],
            newitems:['New Items','Additional itmes come to us!'],
            celebs:['Popular Celebs', 'Lead Trends and Make your own style!']
        }
        return titles;
    }
}

var color1 = '#555';
var color2 = '#DB9B92';
var DISP_NUM = 20; // constant.php 변경시 참고