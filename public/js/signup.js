var emailVal = 0;
var pass1Val = 0;
var pass2Val = 0;
var userVal = 0;
function restrict(elem){
    var tf = _(elem);
    var rx = new RegExp;
    if(elem == "username"){
        rx = /[^a-z0-9]/gi;
    } else if(elem == "captcha"){
        rx = /[^0-9]/gi;
    } /*else if(elem == "lastname" || elem=="firstname"){
        rx = /[^\w\s]/gi;
    }*/
    tf.value = tf.value.replace(rx, "");
}

function validateEmail() {
    var emailText = _('email').value;
    var pattern = /^[a-zA-z0-9_\-\.]+@[a-zA-Z0-9_\-]+\.[a-zA-Z]{2,}$/;// /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z0-9]{2,6}$/;
    if(emailText!=''){
        if (pattern.test(emailText)) {
            emailVal = 1;
            jQuery('#email').parent().removeClass('errored');
            return true;
        } else {
            _("emailstatus").innerHTML = 'Invalid email address';
            _("emailstatus").style.color = "#F00";
            jQuery('#email').parent().addClass('errored');
            emailVal = 0;
            return false;
        }
    }
}

function checkusername(){
    var u = _("username").value;
    if(u != ""){
        _("namestatus").innerHTML = '<strong style="color:#009900;">checking ...</strong>';
        var ajax = ajaxObj("POST", 'signup/userNameCheck');
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) == true) {
                var resp = ajax.responseText;
                    resp = resp.trim();
                    //alert(resp);
                if(resp == 'You can use this name.'){
                    userVal = 1;
                    jQuery('#username').parent().removeClass('errored');
                    _("namestatus").innerHTML = '<strong style="color:#009900;">'+resp+'</strong>';
                } else {
                    userVal = 0;
                    jQuery('#username').parent().addClass('errored');
                    _("namestatus").innerHTML = '<strong style="color:#F00;">'+resp+'</strong>';
                }
            }
        }
        ajax.send("usernamecheck="+u);
    }
}

function captrefresh(){
    var ajax = ajaxObj("POST", URL+'libs/func/captcha.php');
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) == true) {
            _("captref").src=URL+'libs/func/captcha.php';
        }
    }
    ajax.send();
}

function checkpassword1(){
    var pass = _("pass1").value;
    var len = pass.length;
    var pattern = /^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,20}$/;
    if (pattern.test(pass)) {
        emptyElement("pass1status");
        jQuery('#pass1').parent().removeClass('errored');
        pass1Val = 1;
    }else if(pass!=''){
        _("pass1status").innerHTML = '8 ~ 20 characters (must include 1 letter and 1 number)';
        _("pass1status").style.color = "#F00";
        jQuery('#pass1').parent().addClass('errored');
        pass1Val = 0;
    } else {
        pass1Val = 0;
    }
}
function checkpassword2(){
    var pass = _("pass2").value;
    var len = pass.length;
    var pass1 = _("pass1").value;
    var pattern = /^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,20}$/;

    if(pass==''){
        pass2Val = 0;
    }
    else if(!pattern.test(pass)){
        _("pass2status").innerHTML = '8 ~ 20 characters (must include 1 letter and 1 number)';
        _("pass2status").style.color = "#F00";
        jQuery('#pass2').parent().addClass('errored');
        pass2Val = 0;
    }
    else if(pass1!=pass){
        _("pass2status").innerHTML = 'Your password fields do not match!';
        _("pass2status").style.color = "#F00";
        jQuery('#pass2').parent().addClass('errored');
        pass2Val = 0;
    }
    else{
        _("pass2status").innerHTML = 'Your password fields are matched!';
        _("pass2status").style.color = "#009900";
        jQuery('#pass2').parent().removeClass('errored');
        pass2Val = 1;
        pass1Val = 1;
    }
}

function signup(){
    var u = _("username").value;
    var e = _("email").value;
    var p1 = _("pass1").value;
    var p2 = _("pass2").value;
    var cap = _("captcha").value;
    var status = _("resultStatus");
    /*var status = _("status2");
    var agree = _('agreement').checked;
    var about = _('textDescription').value;
    var firstname = _('firstname').value;
    var lastname = _('lastname').value;
    var day = _('day').value;
    var month = _('month').value;
    var year = _('year').value;
    var gender = _('gender').value;
    var web = _('website').value;
    var facebook = _('facebook').value;
    var twitter = _('twitter').value;
    var googleplus = _('googleplus').value;
    var youtube = _('youtube').value;
    var profileRule = _('profileRule').value;
    var timelineRule = _('timelineRule').value;
    var country = _('country').value;
    var address1 = _('address1').value;
    var address2 = _('address2').value;
    var city = _('city').value;
    var state = _('state').value;
    var zip = _('postal').value;

    var addSNSNum = $("#addsns div.row").length;
    var sns = '';
    if(facebook != ''){
        sns += 'Facebook:'+facebook+',';
    }
    if(twitter != ''){
        sns += 'Twitter:'+twitter+',';
    }
    if(googleplus != ''){
        sns += 'Google:'+googleplus+',';
    }
    if(youtube != ''){
        sns += 'Youtube:'+youtube+',';
    }
    for(var i=0; i<addSNSNum; i++){
        var newSNS = $("select#snslist:eq("+i+")")[0].value;
        var newSNSID = $("input#snslist:eq("+i+")")[0].value;

        if(sns.search(newSNS) == -1){
            sns += newSNS+':'+newSNSID+',';
        }
    }
    sns = sns.slice(0, sns.length-1);
    */

    //if(agree == true){
        if(u == "" || e == "" || p1 == "" || p2 == "" || cap==""){
            status.innerHTML = "Empty fields are exist.";
            status.style.color = "#F00";
        } else if(userVal==0){
            status.innerHTML = "Username field has an error.";
            status.style.color = "#F00";
        } else if(pass1Val != 1 || pass2Val != 1){
            status.innerHTML = "Password field has an error.";
            status.style.color = "#F00";
        } else if(emailVal==0){
            status.innerHTML = "Invalid E-mail Address.";
            status.style.color = "#F00";
        } else {
            status.innerHTML = '<i class="fa fa-fw fa-spinner fa-spin"></i> please wait ...';
            status.style.color = "#009900";

            var ajax = ajaxObj("POST", "signup/signup");
            ajax.onreadystatechange = function() {
                if(ajaxReturn(ajax) == true) {
                    var resp = ajax.responseText;
                    resp = resp.trim();

                    if(resp != "signup_success"){
                        status.innerHTML = ajax.responseText;
                        status.style.color = "#F00";
                    } else {
                        window.scrollTo(0,0);
                        //_("regNowBtn").click();
                        status.innerHTML = 'Account creation OK! ... <i class="fa fa-fw fa-spinner fa-spin"></i>';
                        status.style.color = "#009900";
                        window.location = URL;
                        //_("regResult").innerHTML = 'Welcome! Registration OK! Check your E-mail for Activation!';
                        //_("avatarFile").click();
                    }
                }
            }
            var data = {
                u : u,
                e : e,
                p : p1,
                cap : cap
                /*about : about,
                first : firstname,
                last : lastname,
                day : day,
                month : month,
                year : year,
                g : gender,
                web : web,
                sns : sns,
                prule : profileRule,
                trule : timelineRule,
                c : country,
                add1 : address1,
                add2 : address2,
                city : city,
                state : state,
                zip : zip*/
            };
            ajax.send($.param(data));
        }
    //} else {
    //    return false;
    //}
}

function loginEnter(e, textarea){
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) { //Enter keycode
        login();
    }
}

function login(){
    var e = _("loginname").value;
    var p = _("loginpass").value;
    var r = _("remember").checked;
    var status = _("loginStatus");

    if(r==true){
        r = 1;
    } else {
        r = 0;
    }
    if(e == "" || p == ""){
        status.innerHTML = "Fill out all of the form data";
        status.style.color = "#F00";
    } else {
        //_("loginbtn").style.display = "none";
        status.innerHTML = 'please wait ...';
        status.style.color = "#009900";
        var ajax = ajaxObj("POST", 'login/login');
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) == true) {
                var resp = ajax.responseText;
                resp = resp.trim();

                if(resp == "login_failed"){
                    status.innerHTML = "Login unsuccessful, please try again.";
                    status.style.color = "#F00";
                    //_("loginbtn").style.display = "block";
                } else if(resp.search("not_active") != -1){
                    alert('not active user');
                    var datArray = resp.split("|");
                    window.location = URL+"index?active=no&nuser="+datArray[1];
                }
                else {
                    //alert('active user');
                    window.location = URL;//+"index?user="+resp;
                }
            }
            return false;
        }
        ajax.send("signup=OK&e="+e+"&p="+p+"&r="+r);
    }
    return false;
}

var snsid = 0;
var setListFlag = 0;
var selList = '';
var snscnt = 0;
function addsns(){
    if(snscnt < 5){
        _("addsns").style.display = "block";
        var element = _("addsns");
        var snsdiv = document.createElement("div");
        snsdiv.className = "row";
        snsdiv.setAttribute("id", "snslist_"+snsid);

        if(setListFlag == 0 ){
            for (var i=4; i<8; i++)
            {
                selList += '<option value="'+SNSLIST[i]+'">'+SNSLIST[i]+'</option>';
                //alert(selList);
            }
            setListFlag = 1;
        }
        snsdiv.innerHTML = '<div class="col-md-3"><select id="snslist">'+selList+'</select></div>';
        snsdiv.innerHTML += '<div class="col-md-6"><input type="text" id="snslist" class="col-md-6 form-control" onkeyup="" maxlength="255"/></div>';
        snsdiv.innerHTML += '<div class="col-md-1"><button class="btn btn-info btn-stroke" onclick="delsns(\''+snsid+'\')">Remove</button></div><div class="separator"></div>';
        element.appendChild(snsdiv);

        var sepdiv = document.createElement("div");
        sepdiv.className = "separator";
        sepdiv.setAttribute("id", "sep_"+snsid);
        element.appendChild(sepdiv);
        snsid++;
        snscnt++;
    }
}

function delsns(id){
    var parent = _("addsns");
    var rem = _("snslist_"+id);
    var remsep = _("sep_" + id);
    parent.removeChild(rem);
    parent.removeChild(remsep);
    snscnt--;
}