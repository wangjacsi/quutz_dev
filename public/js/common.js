// JavaScript Document
function _(x){
    return document.getElementById(x);
}
function emptyElement(x){
    _(x).innerHTML = "";
}
function toggleElement(x){
    var x = _(x);
    if(x.style.display == 'block'){
        x.style.display = 'none';
    }else{
        x.style.display = 'block';
    }
}
function statusMax(field, maxlimit) {
    if (field.value.length > maxlimit){
        alert(maxlimit+" maximum character limit reached");
        field.value = field.value.substring(0, maxlimit);
    }
}

function logout(){
    //alert('11111');
    var ajax = ajaxObj("POST", URL+'logout');
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) == true) {
            var resp = ajax.responseText;
            resp = resp.trim();
            if(resp == "logout"){
                window.location = URL+"shop";
            }
        }
        return false;
    }
    ajax.send();
}
/**
 * display function
 *
 */
function setRatingStar(rating){
    var width = 0;
    if(rating == ''){
        return width;
    } else {
        width = parseFloat(rating) * 20; // string to float
    }
    return width;
}
function shortNumStr(n){
    n = parseFloat(n);
    if (n < 1000){
        return n_format = n;
    } else if (n < 1000000) {
        // Anything less than a million
        return n_format = (n/1000).toFixed(1) + 'K';
    } else if (n < 1000000000) {
        // Anything less than a billion
        return n_format = (n/1000000).toFixed(1)+ 'M';
    } else {
        // At least a billion
        return n_format = (n/1000000000).toFixed(1)+ 'B';
    }
    //return n_format;
}
/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
12345678.9.format(2, 3, '.', ',');  // "12.345.678,90"
123456.789.format(4, 4, ' ', ':');  // "12 3456:7890"
12345678.9.format(0, 3, '-');       // "12-345-679"
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

/**
 * pagination function
 *
 */
function makePaginationHtml(last, curr){
    var numPages = 5;
    var pageHtml = '';
    var start;
    var end;
    //pageHtml += '<div class="block_pager_1">';
    if(last < curr){
        curr = last;
    }
    if(last < 6){
        start = 1;
        end = last + 1;
    } else {
        if(curr < 4){
            start = 1;
            end = numPages + 1;
        } else if(curr > last - 3){
            start = last -4;
            end = last + 1;
        } else {
            start = curr-2;
            end = curr+3;
        }
    }
    if(start == 1 && curr==1){
        pageHtml += '<ul><li class="prev"><a class="disable"><i class="fa fa-chevron-left"></i></a></li>';
    } else{
        pageHtml += '<ul><li class="prev"><a class="pointer" value="prev"><i class="fa fa-chevron-left"></i></a></li>';
    }
    if(start!= 1){
        pageHtml += '<li><a class="pointer" value="1">1</a></li>';
        if(start!=2){
            pageHtml += '<li class="skip"><i class="fa fa-ellipsis-h"></i></li>';
        }
    }
    for(var i=start; i<end; i++){
        if(i==curr)
            pageHtml += '<li class="current"><a>'+curr+'</a></li>';
        else
            pageHtml += '<li><a class="pointer" value="'+i+'">'+i+'</a></li>';
    }
    if(end < last + 1){
        if(end!=last){
            pageHtml += '<li class="skip"><i class="fa fa-ellipsis-h"></i></li>';
        }
        pageHtml += '<li><a class="pointer" value="'+last+'">'+last+'</a></li>';
    }
    if(last != curr)
        pageHtml += '<li class="next"><a class="pointer" value="next"><i class="fa fa-chevron-right"></i></a></li></ul>';
    else
        pageHtml += '<li class="next"><a class="disable"><i class="fa fa-chevron-right"></i></a></li></ul>';
    pageHtml += '<div class="info" last="'+last+'">Page '+curr+' of '+last+'</div><div class="clearboth"></div>';//</div>';
    return pageHtml;
}

