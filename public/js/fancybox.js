$(document).ready(function() {
    //Fancy
    /*$(document).bind("fullscreenerror", function() {
        alert("Browser rejected fullscreen change");
    });*/
    var celeb='', likes=0, collect=0, brand='', title='', price=0;

    $('a.fancy2').attr('rel', 'gallery').fancybox({
        padding : 5,
        margin      : [20, 60, 20, 60],
        openEffect  : 'none',
        closeEffect : 'none',
        beforeLoad: function() {
            celeb = $(this.element).attr('celeb');
            likes = $(this.element).attr('likes');
            collect = $(this.element).attr('collect');
            brand = $(this.element).attr('brand');
            title = $(this.element).attr('title');
            price = $(this.element).attr('price');
        },
        afterShow: function() { //mouse over after show the title
            $('<div class="product"><div class="user-action"><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>'+
              '<a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>'+
              '<a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>'+
              '</div><div class="cart"><a href="#" class="general_colored_button default" title="Add to cart"><i class="fa fa-lg fa-shopping-cart"></i></a>'+
            '</div></div>').appendTo(this.inner);

            $(".fancybox-title").wrapInner('<div />').show();

            $(".fancybox-wrap").hover(function() {
                $(".fancybox-title").show();
                //$(".expander").show();
                $(".product").show();
            }, function() {
                $(".fancybox-title").hide();
                //$(".expander").hide();
                $(".product").hide();
            });
        },
        afterLoad: function() {
            if (this.title) {
                this.title = '<div class="product"><div class="title"><a href="">'+title+'</a>'+
                            '</div><div class="price"><i class="fa fa-dollar"></i>'+price+'</div><div class="f-clear"></div>'+
                            '<div class="info"><div class="tags">BRAND <strong><a href="#" class="font-color1">'+brand+'</a></strong></div>'+
                '<div class="stats show-stat"><div class="likes" title="Likes"><i class="fa fa-heart"></i>'+likes+'</div>'+
                '<div class="comments" title="Collections"><i class="fa fa-cube"></i>'+collect+'</div>'+
                '</div></div></div>';

                this.title += '<div class="social"><ul class="general_social_2">'+
                            '<li><a href="#" class="social_1">Twitter</a></li>'+
                            '<li><a href="#" class="social_2">Facebook</a></li>'+
                            '<li><a href="#" class="social_3">Pinterest</a></li>'+
                            '<li><a href="#" class="social_4">Google Plus</a></li>'+
                            '<li><a href="#" class="social_5">Instagram</a></li>'+
                        '</ul></div>';
            }
        },
        beforeShow: function () {

        },
        beforeClose: function(){

        },
        afterClose: function() {
            $('#slider article').css('display', 'block');
            $('#post_slider_1 .slider .slides li').css('display', 'block');
            $('.isotope article').css('display', 'block');
        },
        helpers : {
            title: {
                type: 'inside'
            }
        }
    });


    $(".celebs a.fancybox").attr('rel', 'gallery').fancybox({
        padding : 5,
        margin      : [20, 60, 20, 60],
        openEffect  : 'none',
        closeEffect : 'none',
        //"hideOnContentClick" : false,
        beforeLoad: function() {
            celeb = $(this.element).attr('celeb');
            likes = $(this.element).attr('likes');
            collect = $(this.element).attr('collect');
            //this.title -> title 세팅함.
            //this.title = $(this.element).attr('caption');
            /*var el, id = $(this.element).data('title-id');
            if (id) {
                el = $('#' + id);

                if (el.length) {
                    this.title = el.html();
                }
            }*/
        },
        afterShow: function() { //mouse over after show the title
            /*$('<div class="expander"></div>').appendTo(this.inner).click(function() {
                //$(document).toggleFullScreen();
            });*/

            $('<div class="product"><div class="user-action"><a href="#" class="general_colored_button blue" title="Like"><i class="fa fa-lg fa-heart-o"></i></a>'+
              '<a href="#" class="general_colored_button blue" title="Add to my collection"><i class="fa fa-lg fa-navicon"></i></a>'+
              '<a href="#" class="general_colored_button blue" title="Share"><i class="fa fa-lg fa-mail-forward"></i></a>'+
              '</div></div>').appendTo(this.inner);


            $(".fancybox-title").wrapInner('<div />').show();

            $(".fancybox-wrap").hover(function() {
                $(".fancybox-title").show();
                //$(".expander").show();
                $(".product").show();
            }, function() {
                $(".fancybox-title").hide();
                //$(".expander").hide();
                $(".product").hide();
            });

            // Render tweet button
            //twttr.widgets.load();
        },
        afterLoad: function() {
            if (this.title) {

                this.title = '<div class="product"><div class="info"><div class="tags">CELEB <strong><a href="#" class="font-color1">'+celeb+'</a></strong></div>'+
                '<div class="stats show-stat"><div class="likes" title="Likes"><i class="fa fa-heart"></i>'+likes+'</div>'+
                '<div class="comments" title="Collections"><i class="fa fa-cube"></i>'+collect+'</div>'+
                '</div></div></div>';

                //this.title = '<a href="' + this.href + '" class="brand-name">'+this.title+'</a> ';
                // New line
                //this.title += '<br />';

                this.title += '<div class="social"><ul class="general_social_2">'+
                            '<li><a href="#" class="social_1">Twitter</a></li>'+
                            '<li><a href="#" class="social_2">Facebook</a></li>'+
                            '<li><a href="#" class="social_3">Pinterest</a></li>'+
                            '<li><a href="#" class="social_4">Google Plus</a></li>'+
                            '<li><a href="#" class="social_5">Instagram</a></li>'+
                        '</ul></div>';


                // Add tweet button
                ////this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="' + this.href + '">Tweet</a> ';
                //this.title += '<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html" style="width:130px; height:20px;"></iframe>';
                // Add FaceBook like button
                //this.title += '<iframe src="//www.facebook.com/plugins/like.php?href=' + this.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:23px;" allowTransparency="true"></iframe>';
            }
            //this.title = '<a href="' + this.href + '">'+this.title+'</a> ';
        },
        beforeShow: function () {
            /*if (this.title) {
                this.title = '<a href="' + this.href + '">'+this.title+'</a> ';
                // New line
                this.title += '<br />';

                // Add tweet button
                ////this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="' + this.href + '">Tweet</a> ';
                this.title += '<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html" style="width:130px; height:20px;"></iframe>';

                // Add FaceBook like button
                this.title += '<iframe src="//www.facebook.com/plugins/like.php?href=' + this.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:23px;" allowTransparency="true"></iframe>';
            }*/
        },
        beforeClose: function(){
            //init_slider_3('#slider');
        },
        afterClose: function() {
            $('#slider article').css('display', 'block');
            $('#post_slider_1 .slider .slides li').css('display', 'block');
            $('.isotope article').css('display', 'block');
        },
        helpers : {
            title: {
                type: 'inside'
            }
        }
    });

//Montage Image
     var $container2  = $('#am-container2'),
        $imgs2       = $container2.find('img').hide(),
        totalImgs2   = $imgs2.length;

    $imgs2.each(function(e) {
        var $img    = $(this);
        var $imgLink = $img.parent();

        $('<img/>').load(function() {

        }).error(function(){$img.remove();$imgLink.remove();}).attr('src',$img.attr('src'));
    });
    $imgs2.show();
    $container2.montage({
        liquid  : false,
        minw : 100,
        //fixedHeight : 100
        //fillLastRow : true
        alternateHeight : true,
        alternateHeightRange : {
            min : 50,
            max : 200
        }
        //margin : 1
    });


    //Montage Image
     var $container  = $('#am-container'),
        $imgs       = $container.find('img').hide(),
        totalImgs   = $imgs.length,
        cnt         = 0;

    $imgs.each(function(e) {
        var $img    = $(this);
        var $imgLink = $img.parent();

        $('<img/>').load(function() {
            /*++cnt;
            if( cnt === totalImgs ) {
                alert('show images');
                $imgs.show();
                $container.montage({
                    liquid  : false,
                    fixedHeight : 60
                    //fillLastRow : true
                    /*alternateHeight : true,
                    alternateHeightRange : {
                        min : 90,
                        max : 240
                    },
                    margin : 1
                });

                $('#overlay').fadeIn(500);
                // image 미리 데모용으로 73개를 준비중이며 그것에 순서 포지션을 랜덤하게
                // 돌려서 이미지 로딩 할 시에 랜덤하게 뽑아주기 위한 코드임.
                var imgarr  = new Array();
                for( var i = 1; i <= 73; ++i ) {
                    imgarr.push( i );
                }
                $('#loadmore').show().bind('click', function() {
                    var len = imgarr.length;
                    for( var i = 0, newimgs = ''; i < 15; ++i ) {
                        var pos = Math.floor( Math.random() * len ),
                            src = imgarr[pos];
                        newimgs += '<a href=""><img src="'+ASSETS_PATH+'AutomaticImageMontage/images/' + src + '.jpg"/></a>';
                    }

                    var $newimages = $( newimgs );
                    $newimages.imagesLoaded( function(){
                        $container.append( $newimages ).montage( 'add', $newimages );
                    });
                });
            }*/
        }).error(function(){$img.remove();$imgLink.remove();}).attr('src',$img.attr('src'));
    });
    $imgs.show();
    $container.montage({
        liquid  : false,
        fixedHeight : 60
        //fillLastRow : true
        /*alternateHeight : true,
        alternateHeightRange : {
            min : 90,
            max : 240
        },
        margin : 1*/
    });

    // image 미리 데모용으로 73개를 준비중이며 그것에 순서 포지션을 랜덤하게
    // 돌려서 이미지 로딩 할 시에 랜덤하게 뽑아주기 위한 코드임.
    var imgarr  = new Array();
    for( var i = 1; i <= 73; ++i ) {
        imgarr.push( i );
    }
    $('#loadmore').show().bind('click', function() {
        var len = imgarr.length;
        for( var i = 0, newimgs = ''; i < 15; ++i ) {
            var pos = Math.floor( Math.random() * len ),
                src = imgarr[pos];
            newimgs += '<a title="'+src+'" href="'+ASSETS_PATH+'AutomaticImageMontage/images/' + src + '.jpg" rel="fancybox" class="fancybox"><img alt="1" title="dhsj ahdjadhsjdhajdhasj dshjkadshdj" src="'+ASSETS_PATH+'AutomaticImageMontage/images/' + src + '.jpg"></img></a>';
            //<a href=""><img src="'+ASSETS_PATH+'AutomaticImageMontage/images/' + src + '.jpg"/></a>';
        }
        var $newimages = $( newimgs );
        $newimages.imagesLoaded( function(){
            $container.append( $newimages ).montage( 'add', $newimages );
        });
    });
});