// external js:
// http://isotope.metafizzy.co/beta/isotope.pkgd.js

$( function() {

  var $container = $('.isotope').isotope({
    itemSelector: '.item',
    masonry: {
      columnWidth: 100
    }
  });
  var $stampElem = $container.find('.stamp');
  var isStamped = false;

  $('#toggle-stamp').on( 'click', function() {
    // stamp or unstamp element
    if ( isStamped ) {
      $container.isotope( 'unstamp', $stampElem );
    } else {
      $container.isotope( 'stamp', $stampElem );
    }
    // trigger layout
    $container.isotope('layout');
    isStamped = !isStamped;
  });

});
