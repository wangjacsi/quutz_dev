// external js:
// http://isotope.metafizzy.co/beta/isotope.pkgd.js

$( function() {

  var $container = $('.isotope');
  $container.isotope({
    itemSelector: '.item',
    masonry: {
      columnWidth: 80
    },
    // filter items with odd numbers
    filter: function() {
      var number = $( this ).find('.number').text();
      return parseInt( number, 10 ) % 2;
    },
    // sort by number
    sortBy: 'number',
    getSortData: {
      'number': '.number parseInt'
    }
  });

  $('#insert').on( 'click', function() {
    // create new item elements
    var $elems = getItemElement().add( getItemElement() ).add( getItemElement() );
    // insert new elements
    $container.isotope( 'insert', $elems );
  });

});

// make <div class="item width# height#" />
function getItemElement() {
  var $item = $('<div class="item"></div>');
  // add width and height class
  var wRand = Math.random();
  var hRand = Math.random();
  var widthClass = wRand > 0.85 ? 'width3' : wRand > 0.7 ? 'width2' : '';
  var heightClass = hRand > 0.85 ? 'height3' : hRand > 0.5 ? 'height2' : '';
  $item.addClass( widthClass ).addClass( heightClass );
  // add random number
  var number = Math.floor( Math.random() * 100 );
  $item.append( '<p class="number">' + number + '</p>' );
  return $item;
}
