// external js:
// http://isotope.metafizzy.co/beta/isotope.pkgd.js

$( function() {

  var $container = $('.isotope').isotope({
    itemSelector: '.item',
    masonry: {
      columnWidth: 100
    }
  });

  $container.on( 'click', '.item', function() {
    // remove clicked element
    $container.isotope( 'remove', this )
    // layout remaining item elements
      .isotope('layout');
  });

});
