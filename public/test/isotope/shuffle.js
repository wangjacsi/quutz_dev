// external js:
// http://isotope.metafizzy.co/isotope.pkgd.js

var isoOptions = {
  masonry: {
    columnWidth: 50
  }
};

$( function() {
  // init isotope
  var $container = $('.isotope').isotope( isoOptions );

  $('#shuffle').on( 'click', function() {
    $container.isotope('shuffle');
  });

});
