
<?php
/**
 * DooDu - A PHP Framework For Web Application
 *
 * @package  DooDu
 * @author   Shinil Choi <wangjacsi@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Define Super Values
|--------------------------------------------------------------------------
*/
//date_default_timezone_set('UTC');
/*
|--------------------------------------------------------------------------
| Load Init Set
|--------------------------------------------------------------------------
*/
require_once 'config/paths.php';
require_once 'config/constants.php';
require_once 'config/database.php';
require_once 'config/envset.php';
require_once LIBS . 'func/common.php';

/*
|--------------------------------------------------------------------------
| Use autoload
|--------------------------------------------------------------------------
*/
//Also spl_autoload register (take a look at it if you like)
/*function __autoload($class){
	require_once LIBS . $class . ".php";
}*/

require LIBS . 'core/autoload.php';


/*
|--------------------------------------------------------------------------
| Run Bootstrap
|--------------------------------------------------------------------------
*/
$bootstrap = new Bootstrap();
//Optional set the each path
//$bootstrap->setControllerPath('c');
//$bootstrap->setModelPath('m');
//$bootstrap->setDefaultFile('crunk.php');
//$bootstrap->setErrorFile('e.php');
$bootstrap->init();

