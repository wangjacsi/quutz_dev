<?php
$DEFAULT_CONF = array(
        'LOGIN' => 0,
        'OWNER' => 0,
        'AVATAR' => DEFAULT_AVATAR,//IMAGE_PATH.'/avatar/'.DEFAULT_AVATAR,
        'AVATAR35' => DEFAULT_AVATAR35,
        'AVATAR50' => DEFAULT_AVATAR50,
        'COVER' => DEFAULT_COVER,
        'ID' => 0,
        'NAME' => 0,
        'PAGENAME' => 0,
        'SHOP_NAME' => '',
        'COVER_STATUS' => 'Feel So Good!!',
        'ALERT' => 0,
        'FRIENDBTN' => 0,
        'NUM_FRIENDS' => 0,
        'FRIENDS_WIDGET' => 0,
        'NUM_PHOTO' => 0,
        'NUM_MESSAGE' => 0,
        'LIKES' => 0,
        'TIMEZONE' => 0,
        'TIMEOFFSET' => 0,
        'TODAY' => 0,
        'TOTAL' => 0,
        'RELATION' => 0
);

$RULES = array(
               'SHOW_TODAYS' => 0

               );


// SNS List order
// SNS list Database => Facebook(0), twitter(1), Google+(2), youtube(3), Pinterest(4),
// Instagram(5), Flickr(6), Tumblr(7), ...
$SNSList = array(
             'Facebook' => 'Facebook',
             'Twitter' => 'Twitter',
             'Google' => 'Google',
             'Youtube' => 'Youtube',
             'Pinterest' => 'Pinterest',
             'Instagram' => 'Instagram',
             'Flickr' => 'Flickr',
             'Tumblr' =>'Tumblr'
             );

