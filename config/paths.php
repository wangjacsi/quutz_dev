<?php

define('ROOT', $_SERVER['DOCUMENT_ROOT']);

define('PRJ_NAME', 'QuuTz');

define('URLUP', 'http://localhost/');

define('URL', 'http://localhost/quutz/');
define('PUBLIC_PATH', URL.'public/');
define('ASSETS_PATH', PUBLIC_PATH . 'assets/');








define('LIBS', 'libs/');

// javascript file include
define('JS_PATH', 'public/js');

// style sheet file include
define('STYLE_PATH', 'public/css');

// image file include
define('IMAGE_PATH', 'public/images');

// Database function include
define('DB_PATH', LIBS . 'db/');

// Common function include
define('FUNC_PATH', LIBS . 'func/');
// Script function include
define('SCRIPT_PATH', LIBS . 'script/');
// Mail function include
define('MAIL_PATH', LIBS . 'email/');

// mail contents include
define('MAIL_CONTENTS', 'views/email/');

// Utility path, it includes for debuging code and error handler
define('UTIL_PATH', LIBS . 'util/');

// User Data Path
$pathInPieces = explode('/', ROOT);
array_pop($pathInPieces);
$pathInPieces = implode('/',$pathInPieces);
//print_r($pathInPieces);
//echo ROOT;
//die;

//define('USERDATA_PATH', $pathInPieces . '/csi_prjA_data');
//define('USERS_PATH', $pathInPieces . '/csi_prjA_data/users');
define('USERDATA_PATH', URLUP . 'social_commerce_data');
define('USERS_PATH', URLUP . 'social_commerce_data/users');
define('GROUPS_PATH', URLUP . 'social_commerce_data/groups');
define('SHOPS_PATH', URLUP . 'social_commerce_data/shops');

define('LOCAL_USERDATA_PATH', ROOT . '/social_commerce_data');
define('LOCAL_USERS_PATH', ROOT.'/social_commerce_data/users');
define('LOCAL_GROUPS_PATH', ROOT.'/social_commerce_data/groups');
define('LOCAL_SHOPS_PATH', ROOT.'/social_commerce_data/shops');

//system error log file
define('LOCAL_ERROR_PATH', ROOT.'/social_commerce_data/system/error');
