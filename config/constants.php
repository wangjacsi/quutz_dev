

<?php

/**
 * Security
 * Key, encode method
 *
 */
//The sitewide Hash Key , do not change this because its used for password
define('HASH_GENERAL_KEY', 'sksmsekdtlsdmftkfkdgksek');
//this is for database password only
define('HASH_PASSWORD_KEY', 'sksmsekdtlsdmftkfkdgksek');
//Hash type
//md5, sha256 ...
define('HASH_TYPE', 'sha256');
define('HASH_TYPE2', 'md5');

/**
 *  General Define
 *  Name, Default timezone
 *
 */
define('OUR_NAME', 'DooDu');
//time zone setting
define('TIME_ZONE', 'UTC');//'Asia/Seoul');
date_default_timezone_set(TIME_ZONE);

/**
 *  Setting Define
 *  Name input limit, Friends, Display limit,
 *  Defualt avatar, Cover, ...etc images (path)
 */
define('MAX_USERNAME', 20);
define('MIN_USERNAME', 5);
//SNS Friends MAX number
define('MAX_FRIENDS', 5000);
//display friends numbers
define('DISP_FRIENDS', 15);
//User default Photo
define('DEFAULT_AVATAR', URL.'public/images/avatar/avatardefault_150.jpg');
define('DEFAULT_AVATAR35', URL.'public/images/avatar/avatardefault_35.jpg');
define('DEFAULT_AVATAR50', URL.'public/images/avatar/avatardefault_50.jpg');
define('DEFAULT_COVER', URL.'public/images/cover/cover2.jpg');
//Mail Image
define('MAIL_IMAGE', 'e-mail_icon.gif');
//Mail Image
define('MAIL_IMAGE2', 'glyphicons_244_conversation.png');
//Check Image
define('CHECK_IMAGE', 'glyphicons_001_leaf.png');
//Private Message image
define('PM_IMAGE', 'icon_pm.gif');
define('PM_IMAGE2', 'glyphicons_124_message_plus.png');
//User photo
define('MAX_FILE_UPLOADSIZE', 2097152);
define('UP_WMAX', 200); //User photo width max
define('UP_HMAX', 200); //User photo height max
//User Gallery
define('ALLOW_NUM_PHOTO', 15);
define('GALLERY_WMAX', 800);
define('GALLERY_HMAX', 600);
// user status comment Post
define('NUM_USER_POST', 10);

/**
 *  group setting
 *  naming, images
 *
 */
//groups
define('MAX_GROUPNAME', 30);
define('MIN_GROUPNAME', 3);
define('DEFAULT_GLOGO', 'group_icon.jpg');

/**
 *  Blog setting
 *
 */
//blog
define('DIFF', ',');


/**
 * Admin setting
 * Keys
 *
 */
//for admin
define('SUPER_ADMIN_KEY', 'abc');
//for activate key
define('COOKIE_KEY', 'iuDou');
define('ACTIVATE_KEY', '2nedu');


/**
 * Feed setting
 * Friends feed number limit
 * Friends suggestions
 */
//feed
//SNS Friends MAX number
define('MAX_FEED', 20);
define('MAX_FEED_REPLY', 20);
//friends suggestion
define('MAX_FRIEND_SUGGES', 20);

// Menu Global variables
$MAIN_SLIDER_PIC = array('pic1'=>'http://enterprise-html.weblionmedia.com/images/pic_slider_1_3.jpg',
                         'pic2'=>'http://enterprise-html.weblionmedia.com/images/pic_slider_1_2.jpg',
                         'pic3'=>'http://enterprise-html.weblionmedia.com/images/pic_slider_1_1.jpg',
                         'pic4'=>'http://enterprise-html.weblionmedia.com/images/pic_slider_1_4.jpg',
                         'pic5'=>'http://enterprise-html.weblionmedia.com/images/pic_slider_1_5.jpg',
                         'pic6'=>'http://enterprise-html.weblionmedia.com/images/pic_slider_1_6.jpg');
$CATEGORY_PIC = array('women' =>array('main'=>'https://img1.etsystatic.com/041/0/6416669/il_570xN.587135873_p4mx.jpg',
                                      'sub'=>'https://img1.etsystatic.com/037/0/6086212/il_340x270.624260545_tsgg.jpg',
                                      'title'=>'Fashion items here.'),
                      'men'=>array('main'=>'https://img1.etsystatic.com/027/1/7789766/il_340x270.628067245_abm6.jpg',
                                   'sub'=>'https://img1.etsystatic.com/032/1/5430610/il_300x300.625670695_bgm4.jpg',
                                   'title'=>'\'s items here.'),
                      'home_and_living'=>array('main'=>'https://img1.etsystatic.com/025/0/8925614/il_570xN.543457281_tgit.jpg',
                                               'sub'=>'https://img1.etsystatic.com/014/0/7005346/il_570xN.440811419_awa6.jpg',
                                               'title'=>'\'s items here.'),
                      'art'=>array('main'=>'https://img1.etsystatic.com/032/1/5430610/il_300x300.625670695_bgm4.jpg',
                                   'sub'=>'http://thingd-media-ec2.thefancy.com/default/207590274073887189_0e73a9be3774.jpg',
                                   'title'=>'\'s items here.'),
                      'idea_and_trend'=>array('main'=>'https://img1.etsystatic.com/039/0/5631970/il_300x300.631159389_4kpi.jpg',
                                              'sub'=>'//resize-ec3.thefancy.com/resize/crop/313/thingd/default/661314598737020668_06e624e95aad.jpg',
                                              'title'=>'\'s items here.'),
                      'furniture'=>array('main'=>'//resize-ec1.thefancy.com/resize/crop/313/thingd/default/668048234370832136_af3cd9d0be32.jpg',
                                         'sub'=>'https://img0.etsystatic.com/007/1/7835796/il_224xN.471503316_n1y2.jpg',
                                         'title'=>'Beautiful mind in it.'),
                      'kids'=>array('main'=>'https://img1.etsystatic.com/035/0/6414460/il_340x270.628381141_iay4.jpg',
                                    'sub'=>'https://img0.etsystatic.com/007/1/7835796/il_224xN.471503316_n1y2.jpg',
                                    'title'=>' Cute and sdksk.'),
                      'gift'=>array('main'=>'https://img0.etsystatic.com/017/1/5322868/il_224xN.475417888_sfzn.jpg',
                                    'sub'=>'https://img0.etsystatic.com/007/1/7835796/il_224xN.471503316_n1y2.jpg',
                                    'title'=>' Special day Gift.'),
                      'more'=>array('main'=>'https://img0.etsystatic.com/042/0/6122716/il_570xN.618992274_48ry.jpg',
                                    'sub'=>'https://img0.etsystatic.com/007/1/7835796/il_224xN.471503316_n1y2.jpg',
                                    'title'=>'Voluptatem sequi nesciunt.')
                      );


$HOMEMENU = array(
    "category" => array(
        "women" => array('top'=>array('t1','t2','t3','t4','t5','t6'),
                         'bottom'=>array('bottom'),
                         'outer'=>'',
                         'dress'=>'',
                         'shoes'=>'',
                         'bag'=>'',
                         'accessory'=>'',
                         'other'=>'',
                         'brands'=>''),
        "men" => array('top'=>array('f','f','f','f'),
                       'bottom'=>'',
                       'outer'=>'',
                       'suit_and_shirt'=>'',
                       'shoes'=>'',
                       'bag'=>'',
                       'accessory'=>'',
                       'other'=>'',
                       'brands'=>''),
        "home_and_living" => array('bath'=>'',
                                 'beauty'=>'',
                                 'interior'=>'',
                                 'kitchen'=>'',
                                 'lighting'=>'',
                                 'bedding'=>'',
                                 'other'=>'',
                                 'brands'=>''),
        "art" => array('PAINTING'=>'',
                       'ILLUSTRATION'=>'',
                       'PHOTOGRAPHY'=>'',
                       'PRINTING'=>'',
                       'SCULPTURE'=>'',
                       'POSTER'=>'',
                       'other'=>'',
                       'brands'=>''),
        "idea_and_trend" => array('electronics'=>'',
                                'mobile_and_accessory'=>'',
                                'computer_and_accessory'=>'',
                                'digital_device'=>'',
                                'sound_equipment'=>'',
                                'car_device'=>'',
                                'funny_and_idea'=>'',
                                'other'=>'',
                                'brands'=>''),
        "furniture" => array('furniture'=>array('furniture'), 'brands'=>''),
        "kids" => array('Top'=>'',
                        'Bottom'=>'',
                        'outer'=>'',
                        'infant'=>'',
                        'shoes'=>'',
                        'equipment'=>'',
                        'accessory'=>'',
                        'other'=>'',
                        'brands'=>''),
        "gift" => array('birthday'=>'',
                        'wedding'=>'',
                        'majority'=>'',
                        'graduation'=>'',
                        'baby'=>'',
                        'love'=>'',
                        'other'=>'',
                        'brands'=>''),
        "more" => array('sports'=>'','special_promotion'=>'','new_brand'=>'','coming_up_event'=>'')
    ),
    "brands" => array('search'=>array('a'=>'','b'=>'','c'=>'','d'=>'','e'=>'','f'=>'','g'=>'','h'=>'','i'=>'','j'=>'',
                                      'k'=>'','l'=>'','m'=>'','n'=>'','o'=>'','p'=>'','q'=>'','r'=>'','s'=>'','t'=>'',
                                      'u'=>'','v'=>'','w'=>'','x'=>'','y'=>'','z'=>'','etc'=>''),
                      'favorite'=>array(),
                      'new_brands'=>array(),
                      'event'=>array()),
    "celebs" => array('collections'=>array(),
                      'review_items'=>array()
                      ),
    "my" => array('feed'=>'feed','favorite_shops'=>'shops','favorite_celebs'=>'celebs','collections'=>'collections', 'wish_list'=>'wish', 'purchases_and_reviews'=>'purchase','contact'=>'contact','account_settings'=>'setting')
    );

$PAGETITLES =array('brands'=>
                   array('search'=>
                         array('main'=>'Search Brands', 'sub'=>'Searching for a brand you are looking for'),
                         'new'=>
                         array('main'=>'New Brands', 'sub'=>'New brands come to us!'),
                         'event'=>
                         array('main'=>'Events', 'sub'=>'Special offers of brands'),
                         'favorite'=>
                         array('main'=>'Most Popular Brands', 'sub'=>'POPULAR and Unique brands here', 'main2'=>'brands Lists', 'sub2'=>'There are so many lovely brands'),
                         'store'=>
                         array('hot'=>array('main'=>'Hot Items', 'sub'=>'Popular items here!'),
                               'new'=>array('main'=>'New Items', 'sub'=>'Additional itmes come to us!'),
                               'celebs'=>array('main'=>'Popular Celebs', 'sub'=>'Lead Trends and Make your own style!'),
                               'product'=>array('main'=>'items', 'sub'=>'ENJOY THE BEAUTIFUL ITEMS HERE!'),
                               'info'=>array('main'=>'Story about us', 'sub'=>'need to change later iiiii'),
                               'contact'=>array('main'=>'Contact us', 'sub'=>'leave a message to your brand'),
                               'celebs'=>array('main'=>'Brand\'s celebs', 'sub'=>'Celebrities of this brand!'),
                               'followers'=>array('main'=>'Brand\'s followers', 'sub'=>'People who follow this brand!'),
                               'event'=>array('main'=>'Brand\'s events', 'sub'=>'SPECIAL OFFERS FROM BRAND')
                               )
                         ),
                    'category' => array('main'=>'Favorite brands', 'sub'=>'Unique brands here! Find your style and Enjoy!',
                                        'prod_main'=>' items', 'prod_sub'=>'ENJOY THE BEAUTIFUL ITEMS HERE!'),
                    'celebs'=>array('collections'=>array('main'=>'Popular collections', 'sub'=>'the hottest collections of celebs','main2'=>'Popular Collection lists','sub2'=>'other celebs collections here'),
                                    'review_items'=>array('main'=>'Best Review items', 'sub'=>'','main2'=>'popular review items', 'sub2'=>'')
                                    ),
                    'my'=>array('feed'=>array('main'=>'my feed','sub'=>'items of my favorite brands & celebs'),
                                'shops'=>array('main'=>'my favorite brands','sub'=>''),
                                'celebs'=>array('main'=>'my favorite celebs','sub'=>''),
                                'collections'=>array('main'=>'my collections', 'sub'=>''),
                                'wish'=>array('main'=>'my wish lists','sub'=>''),
                                'contact'=>array('main'=>'contact me','sub'=>'LEAVE your MESSAGE'))
                   );



define('FETCH_MODE', PDO::FETCH_ASSOC);
define('DISP_NUM', 20); // config.js 변경시 참고
