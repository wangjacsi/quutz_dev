<?php
class loadCSS
{
    //Initialize some vars
    public $cssFiles;
    public $_cssFilesPath;

    public function __construct(){
        $this->cssFiles = array();
        $this->_cssFilesPath = array('brands'=>'public/css/brands.css',
                                     'itemdisplay'=>'public/css/itemdisplay.css',
                                     'product'=>'public/css/product.css',
                                     'briefinfo'=>'public/css/briefinfo.css',
                                     'TestIsotopeFilterButton'=>'public/test/isotope/TestIsotope_filter.css',
                                     'TestIsotopeFilterSelect'=>'public/test/isotope/TestIsotope_filterSelect.css',
                                     'TestIsotopeFilterRadio'=>'public/test/isotope/TestIsotope_filterRadio.css',
                                     'TestIsotopeFilterCombination'=>'public/test/isotope/TestIsotope_filterCombination.css',
                                     'TestIsotopeSorting'=>'public/test/isotope/sorting.css',
                                     'TestIsotopeLayout'=>'public/test/isotope/layout.css',
                                     'TestIsotopeLayoutVerticalTable'=>'public/test/isotope/layoutVerticalTable.css',
                                     'TestIsotopeAppended'=>'public/test/isotope/appended.css',
                                     'TestIsotopeInsert'=>'public/test/isotope/insert.css',
                                     'TestIsotopeLayoutToggle'=>'public/test/isotope/layoutToggle.css',
                                     'TestIsotopePrepended'=>'public/test/isotope/prepended.css',
                                     'TestIsotopeRemove'=>'public/test/isotope/remove.css',
                                     'TestIsotopeShuffle'=>'public/test/isotope/shuffle.css',
                                     'TestIsotopeStamp'=>'public/test/isotope/stamp.css',
                                     'TestIsotopeEventLayoutComplete'=>'public/test/isotope/eventLayoutComplete.css'
                                     );
        // else css files --> fancybox, montage
    }

    public function loadCSSArray($array){
        foreach ($array as $value) {
            if(array_key_exists($value, $this->_cssFilesPath)){
                array_push($this->cssFiles,$this->_cssFilesPath[$value]);
            } else if($value == 'fancybox'){
                $this->loadFancyBox();
            } else if($value == 'montage'){
                $this->loadMontage();
            }
        }
        return $this->cssFiles;
    }

    public function loadCSS($action){
        if($action=='index'){

        } else if($action=='category'){
            array_push($this->cssFiles, 'public/css/itemdisplay.css', 'public/css/product.css');
        } else if($action=='product'){
            array_push($this->cssFiles, 'public/css/briefinfo.css', 'public/css/product.css');
            $this->loadFancyBox();
            $this->loadMontage();
        } else if($action == 'brands'){
            array_push($this->cssFiles, 'public/css/brands.css');
        } else if($action == 'storecontact'){
            array_push($this->cssFiles, 'public/css/briefinfo.css');
        } else if($action == 'storeowner'){
            array_push($this->cssFiles, 'public/css/briefinfo.css', 'public/css/product.css');
            $this->loadFancyBox();
        } else {

        }
        return $this->cssFiles;
    }

    public function loadFancyBox(){
        array_push($this->cssFiles, 'public/fancybox-2.1.5/source/jquery.fancybox.css?v=2.1.5',
                   'public/fancybox-2.1.5/source/helpers/jquery.fancybox-buttons.css?v=1.0.5',
                   'public/fancybox-2.1.5/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7',
                   'public/css/fancybox.css');
    }

    public function loadMontage(){
        array_push($this->cssFiles, 'public/assets/AutomaticImageMontage/css/style.css');
    }
}
?>