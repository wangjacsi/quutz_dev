<?php
class loadJS
{
    //Initialize some vars
    public $jsFiles;
    public $footJsFiles;
    public $_jsFilesPath;

    public function __construct(){
        $this->jsFiles = array();
        $this->footJsFiles = array();
        $this->_jsFilesPath = array('signup'=>'public/js/signup.js',
                                    'product'=>'views/product/js/product.js',
                                    'category'=>'views/category/js/category.js',
                                    'brands'=>'views/brands/js/brands.js',
                                    'storeproduct'=>'views/brands/js/storeproduct.js',
                                    'jscroll'=>'public/js/plugins/jquery.jscroll.min.js',
                                    'isotope-cell-by-row'=>'public/js/plugins/isotope.cells-by-row.js',
                                    'TestIsotopeFilterButton' => 'public/test/isotope/TestIsotope_filter.js',
                                    'TestIsotopeFilterSelect'=>'public/test/isotope/TestIsotope_filterSelect.js',
                                    'TestIsotopeFilterRadio'=>'public/test/isotope/TestIsotope_filterRadio.js',
                                    'TestIsotopeFilterCombination'=>'public/test/isotope/TestIsotope_filterCombination.js',
                                    'TestIsotopeSorting'=>'public/test/isotope/sorting.js',
                                    'TestIsotopeLayout'=>'public/test/isotope/layout.js',
                                    'TestIsotopeLayoutVerticalTable'=>'public/test/isotope/layoutVerticalTable.js',
                                    'TestIsotopeAppended'=>'public/test/isotope/appended.js',
                                    'TestIsotopeInsert'=>'public/test/isotope/insert.js',
                                    'TestIsotopeLayoutToggle'=>'public/test/isotope/layoutToggle.js',
                                    'TestIsotopePrepended'=>'public/test/isotope/prepended.js',
                                    'TestIsotopeRemove'=>'public/test/isotope/remove.js',
                                    'TestIsotopeShuffle'=>'public/test/isotope/shuffle.js',
                                    'TestIsotopeStamp'=>'public/test/isotope/stamp.js',
                                    'TestIsotopeEventLayoutComplete'=>'public/test/isotope/eventLayoutComplete.js'
                                    );
        //$this->loadJS($action, $mode);
    }

    public function loadJSArray($array, $array2 = array()){
        foreach ($array as $value) {
            if(array_key_exists($value, $this->_jsFilesPath)){
                array_push($this->jsFiles,$this->_jsFilesPath[$value]);
            } else if($value == 'fancybox'){
                $this->loadFancyBox();
            } else if($value == 'montage'){
                $this->loadMontage();
            }
        }
        foreach ($array2 as $value) {
            if(array_key_exists($value, $this->_jsFilesPath)){
                array_push($this->footJsFiles,$this->_jsFilesPath[$value]);
            } else if($value == 'fancybox'){
                $this->loadFancyBox();
            } else if($value == 'montage'){
                $this->loadMontage();
            }
        }
        return $this->jsFiles;
    }

    public function loadJS($action, $mode){
        if($mode==0){
            $this->jsFiles = array('public/js/signup.js');
        }
        if($action=='index'){
            /*if($mode==0){
                $this->jsFiles = array('public/js/signup.js');
            }*/
        } else if($action=='product'){
            array_push($this->jsFiles, 'views/'.$action.'/js/product.js');
            array_push($this->footJsFiles, 'public/js/fancybox.js');
            $this->loadFancyBox();
            $this->loadMontage();
        } else if($action =='category'){
            array_push($this->jsFiles, 'views/'.$action.'/js/category.js');
        } else if($action=='brands'){
            array_push($this->jsFiles, 'views/'.$action.'/js/brands.js');
        } else if($action == 'storeproduct'){
            array_push($this->jsFiles, 'views/brands/js/storeproduct.js');
        } else if($action == 'storecontact'){
            array_push($this->jsFiles, 'views/product/js/product.js', 'views/brands/js/storeproduct.js');
        } else if($action == 'storeowner'){
            array_push($this->jsFiles, 'views/brands/js/brands.js', 'views/brands/js/storeproduct.js');
            $this->loadFancyBox();
        } else {
            /*if($mode==0){
                $this->jsFiles = array('public/js/signup.js');
            }*/
        }
        return $this->jsFiles;
    }

    public function loadFancyBox(){
        array_push($this->jsFiles, 'public/fancybox-2.1.5/lib/jquery.mousewheel-3.0.6.pack.js',
                   'public/fancybox-2.1.5/source/jquery.fancybox.pack.js?v=2.1.5',
                   'public/fancybox-2.1.5/source/helpers/jquery.fancybox-buttons.js?v=1.0.5',
                   'public/fancybox-2.1.5/source/helpers/jquery.fancybox-media.js?v=1.0.6');
        array_push($this->footJsFiles, 'public/js/fancybox.js');
    }

    public function loadMontage(){
        array_push($this->jsFiles, 'public/assets/AutomaticImageMontage/js/jquery.montage.min.js');
    }

}
?>