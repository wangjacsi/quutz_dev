<?php
/**
 * @author      Shinil Choi
 * @copyright   Copyright (C)
 *
 * Files that inculde this file at the very top would NOT require
 * connection to database or session_start(), be careful.
 *
 */

class loginStatus
{
    //Initialize some vars
    public $user;
    public $userOk;
    public $logID;
    public $logUserName;
    private $logPassword;
    public $db;

    public function __construct($db=NULL){
        if($db==NULL)
            $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        else $this->db = $db;
    }

    public function init(){
        Session::start();
        $this->user = '';
        $this->userOk = false;
        $this->logID = "";
        $this->logUserName = "";
        $this->logPassword = "";
    }

    //if first login and dose not inform about timeoffset
    //set timeoffset from Database info
    public function timeZoneSet(){
        Session::start();
        //print_r($_SESSION);die;
        if(!isset($_SESSION['timeoffset'])){
            if(isset($this->user['toffset']) && $this->user['toffset']!='')
                Session::set('timeoffset', $this->user['toffset']);
        }
    }

    /**
     * Evaluation Logged User by search Database
     */
    public function evalLoggedUser($id,$u,$p){
        //echo $id; echo $u; echo $p;
        $result = $this->db->select("SELECT * FROM useroptions WHERE id = :id AND username=:username
            AND spass=:spass LIMIT 1", array(':id' => $id, ':username'=>$u, ':spass'=>$p), 1);

        if($this->db->rowNum >0){
            $this->user = $this->db->select("SELECT * FROM users WHERE id = :id AND username=:username
                AND activated=:activated  LIMIT 1",array(':id' => $id, ':username'=>$u, ':activated'=>1), 1);
            if($this->db->rowNum >0){
                return true;
            }
            else{
                return false;
            }
        } else {
            return false;
        }
    }

    public function lastLogintimeUpdate($id){
        $sth = $this->db->prepare("UPDATE users SET lastlogin=now() WHERE id=:id LIMIT 1");
        $sth->execute(array(':id'=>$id));
    }

    public function statusCheck(){
        $this->init();
        if(isset($_SESSION["userid"]) && isset($_SESSION["username"]) && isset($_SESSION["password"])){
            $this->logID = Validate::userIDSet($_SESSION["userid"]);
            $this->logUserName = Validate::userNameSet($_SESSION["username"]);
            $this->logPassword = Validate::passSet($_SESSION["password"]);
            $this->userOk = $this->evalLoggedUser($this->logID, $this->logUserName, $this->logPassword);
        }
        else if(isset($_COOKIE["id"]) && isset($_COOKIE["user"]) && isset($_COOKIE["pass"])){
            Session::set('userid', Validate::userIDSet($_COOKIE["id"]));
            Session::set('username', Validate::userNameSet($_COOKIE["user"]));
            Session::set('password', Validate::passSet($_COOKIE["pass"]));
            $this->logID = Session::fetch('userid');
            $this->logUserName = Session::fetch('username');
            $this->logPassword = Session::fetch('password');
            $this->userOk = $this->evalLoggedUser($this->logID, $this->logUserName, $this->logPassword);
            if($this->userOk==true){
                //update their last login datetime
                $this->lastLogintimeUpdate($this->logID);
            }
        }
        $this->timeZoneSet();
        return $this->userOk;
    }

}

