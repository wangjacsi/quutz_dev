<?php
/**
 * Pagination 관련 함수
 */
function pagination($total){
    $last = ceil($total/DISP_NUM);
    if($last < 1){
        $last = 1;
    }
    return $last;
}

function makePaginationHtml($last, $curr){
    $numPages = 5;
    $pageHtml = '';
    $pageHtml .= '<div class="block_pager_1">';
    if($last < $curr){
        $curr = $last;
    }
    if($last < 6){ // page수가 5개 이하일 때
        $start = 1;
        $end = $last+1;
    } else { // 5개 보다 많을때
        if($curr < 4){
            $start = 1;
            $end = $numPages+1;
        } else if($curr > $last-3){
            $start = $last-4;
            $end = $last+1;
        } else {
            $start = $curr-2;
            $end = $curr+3;
        }
    }
    if($start==1 && $curr==1){
        $pageHtml .= '<ul><li class="prev"><a class="disable"><i class="fa fa-chevron-left"></i></a></li>';
    } else{
        $pageHtml .= '<ul><li class="prev"><a class="pointer" value="prev"><i class="fa fa-chevron-left"></i></a></li>';
    }
    if($start!=1){
        $pageHtml .= '<li><a class="pointer" value="1">1</a></li>';
        if($start!=2){
            $pageHtml .= '<li class="skip"><i class="fa fa-ellipsis-h"></i></li>';
        }
    }
    for($i=$start; $i<$end; $i++){
        if($i==$curr)
            $pageHtml .= '<li class="current"><a>'.$curr.'</a></li>';
        else
            $pageHtml .= '<li><a class="pointer" value="'.$i.'">'.$i.'</a></li>';
    }
    if($end<$last+1){
        if($end!=$last){
            $pageHtml .= '<li class="skip"><i class="fa fa-ellipsis-h"></i></li>';
        }
        $pageHtml .= '<li><a class="pointer" value="'.$last.'">'.$last.'</a></li>';
    }
    if($last != $curr)
        $pageHtml .= '<li class="next"><a class="pointer" value="next"><i class="fa fa-chevron-right"></i></a></li></ul>';
    else
        $pageHtml .= '<li class="next"><a class="disable"><i class="fa fa-chevron-right"></i></a></li></ul>';
    $pageHtml .= '<div class="info" last="'.$last.'">Page '.$curr.' of '.$last.'</div><div class="clearboth"></div></div>';
    return $pageHtml;
}


/**
 * 각종 미리 지정한 사진 파일의 데이터베이스와 실제 저장된 파일 셋을 연결해 주기 위한 작업을 진행
 *
 */
function photoNameSet($type, $value){
    $newStr = '';
    if($type=='prod_s1'){
        $temp = explode('-', $value);
        $temp2 = explode(".", $temp[1]);
        $newStr = $temp[0] . '-239xN-'. $temp2[0].'-s1-1.'.$temp2[1];
    } else if($type=='prod_s2'){
        $temp = explode('-', $value);
        $temp2 = explode(".", $temp[1]);
        $newStr = $temp[0] . '-144xN-'. $temp2[0].'-s2-1.'.$temp2[1];
    } else if($type=='avatar_s1'){
        $temp = explode('-', $value);
        $temp2 = explode(".", $temp[1]);
        //print_r($temp);
        $newStr = $temp[0] . '-61x61-'. $temp2[0].'-s1.'.$temp2[1];
    }
    return $newStr;
}

/**
 * 셀럽의 포토 관련 세팅을 위한 함수
 *
 */
function celebPhotoNameSet($type, $value, $celebID, $prodID){
    $newStr = '';
    if($type == 'review_s1'){ // 1-239xN-123456789-c1-n7.jpg
        $temp = explode('-', $value);
        $temp2 = explode(".", $temp[3]);
        $newStr = $temp[0] . '-239xN-'. $temp[1].'-'.$temp[2].'-'.$temp2[0].'.'.$temp2[1];
    }
    return $newStr;
}

/**
 * 각 제품의 Rating값에 따른 CSS Star 평점의 width를 설정해줌.
 * rating 5.0 => width 100%
 * rating 0 =>width : 0%
 */
function setRatingStar($rating){
    $width = 0;
    if($rating == ''){
        return $width;
    } else {
        $width = (float)$rating * 20;
    }
    return $width;
}

/**
 * 각 숫자에 관해서 kilo(10^3), mega(10^6), giga(10^9), tera(10^12)
 *
 * @return [type] [description]
 */
function shortNumStr($n, $precision = 1, $f = '.', $s=','){
    if ($n < 1000){
        return $n_format = $n;
    } else if ($n < 1000000) {
        // Anything less than a million
        // $n_format = (number_format($n /1000, $precision, $f, $s)) . 'K';
        return $n_format = (round($n /1000, 1)) . 'K';
    } else if ($n < 1000000000) {
        // Anything less than a billion
        //$n_format = (number_format($n / 1000000, $precision, $f, $s)) . 'M';
        return $n_format = (round($n / 1000000, 1)) . 'M';
    } else {
        // At least a billion
        //$n_format = (number_format($n / 1000000000, $precision, $f, $s)) . 'B';
        return $n_format = (number_format($n / 1000000000, 1)) . 'B';
    }
    /*echo $n_format;
    echo '<br>';
    echo number_format(1500, 1,'.',',');
    echo '<br>';
    echo number_format(1500000/1000, 1,'.',',');
    echo '<br>';
    echo number_format(1500000000000/1000000000, 1,'.',',');
    die;*/
    //return number_format($n_format);
}

/**
 * This is for generate random word
 * @param $len length of generated words
 * @return string
 */
function getSalt($len){
    return $salt = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, $len);
}

/**
 * @return array
 * @param array $src
 * @param array $in
 * @param int|string $pos
*/
function array_push_before($src,$in,$pos){
    if(is_int($pos)) $R=array_merge(array_slice($src,0,$pos), $in, array_slice($src,$pos));
    else{
        foreach($src as $k=>$v){
            if($k==$pos)$R=array_merge($R,$in);
            $R[$k]=$v;
        }
    }return $R;
}

/**
 * @return array
 * @param array $src
 * @param array $in
 * @param int|string $pos
*/
function array_push_after($src,$in,$pos){
    if(is_int($pos)) $R=array_merge(array_slice($src,0,$pos+1), $in, array_slice($src,$pos+1));
    else{
        foreach($src as $k=>$v){
            $R[$k]=$v;
            if($k==$pos)$R=array_merge($R,$in);
        }
    }return $R;
}

/**
 * This is for generate Date form general
 * @param integer $type year, month, day
 * @param string $current 0000, 00, 00
 * @return string html select form
 */
function generateDateForm($type, $current) {
    global $LNG;
    $rows = '';
    // MONTHS
    $month=['January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'];

    if($type == 0) {
        for ($i = date('Y'); $i >= (date('Y') - 100); $i--) {
            if($i == $current) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $rows .= '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
        }
    } elseif($type == 1) {
        for ($i = 1; $i <= 12; $i++) {
            if($i == $current) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $rows .= '<option value="'.$i.'"'.$selected.'>'.$month["$i"-1].'</option>';
        }
    } elseif($type == 2) {
        for ($i = 1; $i <= 31; $i++) {
            if($i == $current) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $rows .= '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
        }
    }
    //echo $rows; die;
    return $rows;
}

function getGenderList($selected = null) {
    $gender = array('n' => 'No Gender', 'm' =>'Male', 'f'=>'Female');
    $output = '';
    foreach($gender as $key => $value)
    {
        if (strtolower($key) == strtolower($selected) && $selected != null)
        {
            $output .= "<option value='$key' selected='selected'>$value</option>";
        }
        else
        {
            $output .= "<option value='$key'>$value</option>";
        }
    }
    return $output;
}
function getSNSList() {
    global $SNSList;
    $output = '';
    foreach($SNSList as $key => $value)
    {
        $output .= "<option value='$key'>$value</option>";
    }
    return $output;
}
// add the born value
function dateFormat($data){
    $output = '';
    if(isset($data['day']) || isset($data['month']) || isset($data['year'])) {
        $output = date("Y-m-d", mktime(0, 0, 0, $data['month'], $data['day'], $data['year']));
    }
    return $output;
}

//File functions
// URL file check using CURL
function is_url_exist($url){
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if($code == 200){
       $status = true;
    }else{
      $status = false;
    }
    curl_close($ch);
   return $status;
}

function getFileListOfDir($path){
    $files = scandir($path);
    return $files;
}

