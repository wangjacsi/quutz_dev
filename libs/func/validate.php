<?php
/**
 * @author      Shinil Choi <wangjacsi@gmail.com>
 * @copyright   Copyright (C), 2013 CSI
 *
 */

class Validate
{
    public static function userNameVal($user){
        if (!preg_match("/^[a-zA-Z0-9]*$/",$user)){
            return "Only letters and Numbers allowed";
        }
        else{
            return "Success";
        }
    }

    public static function photoVal($file){
        return preg_match("/\.(gif|jpg|jpeg|png)$/i", $file);
    }

    public static function fileNameSet($file){
        return preg_replace("/[^a-z 0-9_-]/i", '', $file);
    }

    public static function wordSet($str){
        return preg_replace("/[^a-z]/i", '', $str);
    }

    public static function userNameSet($user){
        return preg_replace("/[^a-z0-9]/i", '', $user);
    }

    public static function timeZoneSet($time){
        return preg_replace("/[^a-z 0-9-+.]/i", '', $time);
    }

    public static function userIDSet($id){
        return preg_replace("/[^0-9]/", '', $id);
    }

    public static function passSet($pass){
        return preg_replace("/[^a-z0-9!@#$%]/i", '', $pass);
    }

    public static function realNameSet($name){
        return preg_replace("/[^\w\s]/i", '', $name);
    }

    public static function emailVal($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return "E-mail is not valid";
        }
        else
        {
            return "Success";
        }
    }

    public static function emailVal2($email){
        return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email);
    }


    public static function ipAddrVal($ip){
        //if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE)) {
        if (!filter_var($ip, FILTER_VALIDATE_IP)) {
            echo "This IP address is not valid.";
        }
        else{
            return "Success";
        }
    }

    /*
    Between Start -> ^
    And End -> $
    of the string there has to be at least one number -> (?=.*\d)
    and at least one letter -> (?=.*[A-Za-z])
    and it has to be a number, a letter or one of the following: !@#$% -> [0-9A-Za-z!@#$%]
    and there have to be 8-12 characters -> {8,12}
    */
    public static function passVal($pass){
        if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,20}$/', $pass)) {
            echo 'The password does not meet the requirements! <br/>
            At least 1 letter, 1 number and 8 ~ 20 characters include(!@#$%)';
        }
        else{
            return "Success";
        }
    }

    public static function inputVal($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlentities($data);//htmlspecialchars($data);
      return $data;
    }

    public static function inputVal2($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      //$data = htmlentities($data);//htmlspecialchars($data);
      return $data;
    }

    public static function dateVal($date){
        if((isset($date['day']) && $date['day']!="") &&
               isset($date['month']) && $date['month']!="" &&
               isset($date['year']) && $date['year']!="") {
            if($date['year'] < date('Y') - 100 || ($date['year'] > date('Y')) || checkdate($date['month'], $date['day'], $date['year']) == false) {
                return 'Incorrect date';
            } else {
                return "Success";
            }
        } else {
            return 'Empty date input';
        }

    }

    public static function urlVal($url){
        if(!filter_var($url, FILTER_VALIDATE_URL)) {
            return 'Invalid URL';
        } else {
            return 'Success';
        }
    }



    /**
     * length - Require min and max length in one call
     *
     * @param string $value
     * @param array $param
     *
     * @return string For an error
     * @throws Exception For a malformed argument
     */
    public function length($value, $param)
    {

        if (!is_array($param) || count($param) > 2)
        throw new \Exception(__CLASS__ . ': Length Parameter must be an array of 1 (exact) or 2 (min/max).');

        $len = strlen($value);

        if (count($param) == 1)
        {
            if ($len != $param[0])
            return "must be exactly $param[0] characters.";
        }
        else
        {
            if ($len < $param[0] || $len > $param[1])
            return "must be between $param[0] and $param[1] characters.";
        }
    }

    /**
     * minlength - Require a minimum length
     *
     * @param string $value
     * @param integer $param
     *
     * @return string For an error
     */
    public function minlength($value, $param)
    {
        if (strlen($value) < $param)
        return "must be atleast $param in length";
    }

    /**
     * maxlength - Require a maximum length
     *
     * @param string $value
     * @param integer $param
     *
     * @return string For an error
     */
    public function maxlength($value, $param)
    {
        if (strlen($value) > $param)
        return "must be no more than $param in length";
    }

    /**
     * exact - Require an exact length
     *
     * @param string $value
     * @param integer $param
     *
     * @return string For an error
     */
    public function exactlength($value, $param)
    {
        if (strlen($value) != $param)
        return "must be exactly $param in length";
    }

    /**
     * gt - The value must be greater than the param
     *
     * @param string $value
     * @param mixed $param
     *
     * @return string For an error
     */
    public function gt($value, $param)
    {
        if (!is_int($param))
        throw new \Exception(__CLASS__ .": must supply an integer: $method");

        if ($value <= $param)
        return "must be greater than $param";
    }

    /**
     * lt - The value must be less than the param
     *
     * @param string $value
     * @param mixed $param
     *
     * @return string For an error
     */
    public function lt($value, $param)
    {
        if (!is_int($param))
        throw new \Exception(__CLASS__ .": must supply an integer: $method");

        if ($value >= $param)
        return "must be less than $param";
    }

    /**
     * eq - Make sure a value matches/equals something
     *
     * @param string $value
     * @param mixed $param
     *
     * @return string For an error
     */
    public function eq($value, $param)
    {
        if ($value != $param)
        return "does not match";
    }

    /**
     * eqany - Require atleast a single match inside of an array
     *
     * @todo: This should combine with match, and you can pass an array
     *
     * @param string $value
     * @param array $param
     * @param boolean $caseSensitive Default: true
     *
     * @return string For an error
     */
    public function eqany($value, $param = array(), $caseSensitive = true)
    {
        if ($caseSensitive == false)
        {
            $value = strtolower($value);
            $param = array_map('strtolower', $param);
        }

        if (!is_array($param))
        throw new \Exception(__CLASS__ . ': matchAny $param must be any array');

        if (!in_array($value, $param))
        return "is not valid";
    }

    /**
     * regex - Require a match of every item
     *
     * @param string $value
     * @param string $param Regular Expression
     */
    public function regex($value, $param)
    {
        if (!preg_match($param, $value))
        return 'must match regular expression';
    }

    /**
     * digit - Require a digit
     *
     * @param mixed $value
     *
     * @return string For an error
     */
    public function digit($value)
    {
        if (!is_numeric($value))
        return 'must be numeric.';
    }

    /**
     * float - Require float value
     *
     * @param float $value
     *
     * @return string For an error
     */
    public function float($value)
    {
        if (!is_float($value))
        return 'must be a float.';
    }

    /**
     * alpha - Require only alphabetical characters
     *
     * @param string $value
     *
     * @return string For an error
     */
    public function alpha($value)
    {
        if (!ctype_alpha($value))
        return 'must be alphabetical only.';
    }

    /**
     * alphanum - Require only alphanumeric characters
     *
     * @param string $value
     *
     * @return string For an error
     */
    public function alphanum($value)
    {
        if (!ctype_alnum($value))
        return 'must be alphanumeric only.';
    }

    /**
     * email - Require an email
     *
     * @param string $value
     *
     * @return string For an error
     */
    public function email($value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL))
        return 'invalid email format.';
    }

    /**
     * age - Checks to see if a person is old enough
     *
     * @param string $dob Use MySQL Format YYYY-MM-DD
     * @param string $min Minimum age allowed
     *
     * @return integer
     */
    public function agemin($dob, $min = 18)
    {
        try {
            $result = $this->_ageCalc($dob);
        } catch(\Exception $e) {
            return $e->getMessage();
        }

        if ($result <= 0 || $result >= 125)
        return "invalid date provided";

        if ($result < $min)
        return "minimum age is $min";
    }

    /**
     * age - Checks to see if a person is young enough
     *
     * @param string $dob Use MySQL Format YYYY-MM-DD
     * @param string $min Minimum age allowed
     *
     * @return integer
     */
    public function agemax($dob, $max = 50)
    {
        try {
            $result = $this->_ageCalc($dob);
        } catch(\Exception $e) {
            return $e->getMessage();
        }

        if ($result <= 0 || $result >= 125)
        return "invalid date provided";

        if ($result > $max)
        return "maximum age is $max";
    }

    /**
     * _ageCalc - Return the age
     *
     * @param string $dob Date of birth
     *
     * @return integer Age based on date calculation
     */
    private function _ageCalc($dob)
    {
        /** Make sure a valid string is being passed for the date */
        if (!preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $dob)) {
            throw new \Exception('date format must be in YYYY-MM-DD');
        }

        list($yyyy, $mm, $dd) = explode('-', $dob);

        if (($mm = (date('m') - $mm)) < 0) {
            $yyyy++;
        } elseif ($mm == 0 && date('d') - $dd < 0) {
            $yyyy++;
        }

        return (int) date('Y') - $yyyy;
    }

    /**
     * __call - Handles non-existant methods
     *
     * @param string $method
     * @param string $arg
     *
     * @throws Exception
     */
    public function __call($method, $arg = null)
    {
        $args = func_get_args();
        array_shift($args); // Remove the method name
        /**
         * Aliases
         */
        switch ($method) {
            case 'len':
                return $this->length($args[0][0], $args[0][1]);
                break;
            case 'minlen':
                return $this->minlength($args[0][0], $args[0][1]);
                break;
            case 'maxlen':
                return $this->maxlength($arg[0][0], $args[0][1]);
                break;
            case 'match':
                return $this->eq($args[0][0], $args[0][1]);
                break;
            case 'matchany':
                return $this->eqany($args[0], $args[0][1]);
                break;
            case 'greaterthan':
                return $this->gt($args[0][0], $args[0][1]);
                break;
            case 'lessthan':
                return $this->lt($args[0][0], $args[0][1]);
                break;
            default:
                throw new \Exception(__CLASS__ .": Does not have any method called: $method");
        }
    }
}