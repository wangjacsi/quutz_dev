

<?php

class Security {
    /**
     *
     */
    public $randomPassSeed64 = array(52,53,60,24,49,35,63,39,6,9,
                        10,61,55,51,57,40,58,22,41,18,
                        4,21,7,54,44,2,43,27,32,46,
                        28,30,11,56,31,45,3,15,33,17,
                        42,36,37,34,26,19,48,12,20,23,
                        29,0,8,50,47,38,5,16,62,1,
                        14,25,59,13
    );

    public $match = 'abcdefghijklmnopqrstuvwxyz0123456789^*$!?:;@~abcdefghijklmnopqrstuvwxyz0123456789^*$!?:;@~abcdefghijklmnopqrstuvwxyz0123456789^*$!?:;@~';
    //public $match = '^*$!?:;@~^*$!?:;@~^*$!?:;@~^*$!?:;@~^*$!?:;@~';
    public $keyPosition = array(4,7,13,16,22);

    function __construct(){

    }

    public function systemKeygen($key){
        $newStr = substr(str_shuffle($this->match), 0, 25);
        for($i=0; $i<5; $i++){
            $newStr[$this->keyPosition[$i]] = $key[$i];
        }
        return $newStr;
    }

    public function systemKeyMatch($pass, $key){
        $decPass='';
        for($i=0; $i<5; $i++){
            $decPass .= $pass[$this->keyPosition[$i]];
        }
        if($decPass != $key){
            return 0;
        } else {
            return 1;
        }
    }

    public static function randomPassSeed64($pass){
        $passNew='';
        for ($i=0; $i<64; $i++){
            $passNew .= $pass[$randomPassSeed64[$i]];
        }
        return $passNew;
    }

    public static function randomPassSeed64Dec($pass){
        $passNew='';
        for ($i=0; $i<64; $i++){
            $passNew .= $pass[$randomPassSeed64[$i]];
        }
        return $passNew;
    }
}