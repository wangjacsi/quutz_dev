<?php
class Crud {
    public $fieldsName;
    public $data;
    public $tableFields;
    public $id;
    public $success;
    public $listTables;
    public $completed;

    function __construct($db=NULL){
        //parent::__construct();
        if($db==NULL)
            $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        else
            $this->db = $db;
        $this->success = '';
        $this->listTables = 0;
        $this->completed = 0;
    }

    // Main process------ -//
    public function crudProcess($tableName, $id=null, $process){

        $this->listTables = $this->getListTables();
        $this->completed = 0;
        if($process=='index'){
            $this->data = $this->getData($tableName, null);
            $this->fieldsNameSet($this->data[0]);
            //return 'system/index';
        } else if($process=='edit'){
            if(isset($_POST['edit_values'])){
                $param = $_POST;
                $this->id = $_GET['id'];
                $result = $this->editData($tableName, $param, $this->id);
                if($result != 0){
                    $this->completed = 1;
                }
            } else if(isset($_GET['id'])){
                $this->id = $_GET['id'];
                $this->tableFields = $this->listFields($tableName);
                $this->data = $this->getData($tableName, $this->id);
            } else {
                header('location: '. URL .'system');
                exit();
            }
        } else if($process=='delete'){
            if(isset($_GET['delete'])){
                $this->id = $_GET['id'];
                $result = $this->deleteData($tableName, $this->id);
                if($result == 1){
                    $this->completed = 1;
                    $this->success =  'Delete Success';
                    //return 'system/delete';
                } else {
                    $this->completed = 0;
                    $this->success = 'There is a problem';
                    //return 'system/delete';
                }
            } else if(isset($_GET['id'])){
                $this->id = $_GET['id'];
                //return 'system/delete';
            } else {
                header('location: '. URL .'system');
                exit();
            }
        } else if($process=='insert'){
            if(isset($_POST['insert_values'])){
                $param = $_POST;
                //print_r($param);
                if(array_key_exists('insert_values', $param)){
                    unset($param['insert_values']);
                }
                $result = $this->insertData($tableName, $param);
                if($result==0){
                    $this->success = 'There is a problem';
                    $this->completed = 0;
                    //return 'system/insert';
                } else {
                    $this->completed = 1;
                }
                //header('location: '. URL .'system');
                //exit();
            } else {
                $this->tableFields = $this->listFields($tableName);
                //return 'system/insert';
            }
        }
    }

    public function getListTables(){
        $query = $this->db->query("show tables");
        //return $result = $query->fetchAll();
        $result = $query->fetchAll();

        if(count($result)!= 0){
            $keyName = 'Tables_in_'.strtolower(DB_NAME);
            foreach($result as $key => $value){
                if($_SESSION['superlevel']!='f'){
                    if($value[$keyName] != 'super_doodu' && $value[$keyName] != 'doodu_action')
                        $table[] = $value[$keyName];
                } else {
                    $table[] = $value[$keyName];
                }

            }
            return $table;
        } else {
            return 0;
        }
    }

    public function insertData($tableName, $data){
        return $this->db->insert($tableName, $data);//prepare("INSERT INTO $tableName ($fieldsName) VALUES ($bindParam)");
    }

    public function deleteData($tableName, $id){
        $where ="id='$id'";
        return $this->db->delete($tableName,$where);
    }

    public function editData($tableName, $param, $id){
        unset($param['edit_values']);
        $where = "id='$id'";
        return $this->db->update($tableName, $param, $where);

    }
    // Main process------ -//

    public function getData($tableName, $id=null){
        if(isset($id)){
            $result = $this->db->select("SELECT * FROM $tableName WHERE id=:id ORDER BY id ASC",
                array(':id' => $id), 0);
        } else {
            $result = $this->db->select("SELECT * FROM $tableName ORDER BY id ASC",
                array(':id' => $id), 0);
        }
        $rowNum = $this->db->rowNum;

        if($rowNum != 0) {
            return $result;
        } else {
            return 0;
        }
    }

    public function fieldsNameSet($array){
        $this->fieldsName = array();
        if($array!=0){
            foreach($array as $key =>$value){
                $this->fieldsName[] = $key;
            }
        } else {
            $this->fieldsName = 0;
        }
        return $this->fieldsName;
    }

    public function listFields($table){
        $sth = $this->db->prepare("DESCRIBE $table");
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}
