<?php

function myAutoload($classname)
{
    //Can't use __DIR__ as it's only in PHP 5.3+
    $directorys = array(
        LIBS,
        LIBS . 'mvc/',
        LIBS . 'core/',
        LIBS . 'db/',
        LIBS . 'func/',
        LIBS . 'email/',
        LIBS . 'data/'
    );

    //for each directory
    foreach($directorys as $directory)
    {
        $filename = $directory.strtolower($classname).'.php';
        $filename2 = $directory.'class.'.strtolower($classname).'.php';

        if (is_readable($filename)) {
            require $filename;
        }
        if(is_readable($filename2)) {
            require $filename2;
        }
    }
}

if (version_compare(PHP_VERSION, '5.1.2', '>=')) {
    //SPL autoloading was introduced in PHP 5.1.2
    if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
        spl_autoload_register('myAutoload', true, true);
    } else {
        spl_autoload_register('myAutoload');
    }
} else {
    //Fall back to traditional autoload for old PHP versions
    function __autoload($classname)
    {
        myAutoload($classname);
    }
}


