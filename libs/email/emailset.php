<?php

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
//date_default_timezone_set('Etc/UTC');

class Emailset {

	public $mail;

	function __construct(){
		//Create a new PHPMailer instance
		$this->mail = new PHPMailer();
		//Tell PHPMailer to use SMTP
		$this->mail->isSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$this->mail->SMTPDebug = 0;
		//Ask for HTML-friendly debug output
		$this->mail->Debugoutput = 'html';
		//Set the hostname of the mail server
		$this->mail->Host = 'smtp.gmail.com';
		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$this->mail->Port = 587;
		//Set the encryption system to use - ssl (deprecated) or tls
		$this->mail->SMTPSecure = 'tls';
		//Whether to use SMTP authentication
		$this->mail->SMTPAuth = true;
		//Username to use for SMTP authentication - use full email address for gmail
		$this->mail->Username = "wangjacsi@gmail.com";
		//Password to use for SMTP authentication
		$this->mail->Password = "c04190419";
		//Set who the message is to be sent from
		$this->mail->setFrom('DooDu@DooDu.com', OUR_NAME .' Service');
		//Set an alternative reply-to address
		$this->mail->addReplyTo('replyto@DooDu.com', OUR_NAME .' Service');
		//Set who the message is to be sent to
		$this->mail->addAddress('wangjacsi@naver.com', 'John Doe');
		//Set the subject line
		$this->mail->Subject = 'PHPMailer GMail SMTP test';
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$this->mail->msgHTML(file_get_contents(MAIL_CONTENTS .'Activation.html'), IMAGE_PATH . '/');
		//Replace the plain text body with one created manually
		$this->mail->AltBody = 'This is a  ' .  OUR_NAME .'message!';
		//Attach an image file
		//$mail->addAttachment('images/phpmailer_mini.gif');
		$this->mail->addAttachment(IMAGE_PATH . '/logo.png');
	}

	public function setTo($addr, $name){
		$this->mail->addAddress($addr, $name);
	}
	public function setSubject($str){
		$this->mail->Subject = $str;
	}

	public function setMessage($str, $dir="IMAGE_PATH . '/'"){
		$this->mail->msgHTML($str);
	}

	public function setHTMLContents($file, $dir="IMAGE_PATH . '/'"){
		$this->mail->msgHTML(file_get_contents($file), dirname($dir));
	}

	public function addAttachment($file){
		$this->mail->addAttachment($file);
	}

	public function sendMail($result = 'off'){
		//send the message, check for errors
		if($result == 'on'){
			if (!$this->mail->send()) {
			    return "Mailer Error: " . $this->mail->ErrorInfo;
			} else {
			    return "Message sent!";
			}
		}
		else{
			if (!$this->mail->send()) {
			    echo "Mailer Error: " . $this->mail->ErrorInfo;
			}
		}

	}
}


