<?php

class Controller{

    function __construct(){
        //view object create!
        $this->view = new View();
    }
    
    /**
     * @param string $name Name of the model
     * @param string $modelPath Location of the models
     */
    public function loadModel($name, $modelPath = 'models/'){
        //$path = 'models/' . $name . '_model.php';
        $path = $modelPath . $name . '_model.php';
        //echo $path;//just check

        if(file_exists($path)) {
            //require 'models/' . $name . '_model.php';
            require $modelPath . $name . '_model.php';

            $modelName = $name . '_Model';
            $this->model = new $modelName();

            //echo '<br />' .$modelName . '<br />';
            //echo 'load model <br />';//just check
        }
        else{ //just check
                //echo 'NO NO NO';
        }
    }
}