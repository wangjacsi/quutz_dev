<?php

class View {
    public $CONFIG_SET;
    public $page;

    function __construct(){
        //echo 'This is View';
        $this->CONFIG_SET = '';
        //$this->appJs = '';
        $this->js = array();
        //$this->appConJs = '';
        $this->jsFooter = array();
        // CSS files include
        $this->css = array();
        //set page
        $this->page = 'index';
        // when we brand or user information need this view, default not use 0
        $this->topMiddleUse = 0;
    }

    public function render($name, $noInclude = false){
        //$file = 'views/' . $name . '.php';
        //$file2 = 'localhost/social_commerce/views/' . $name . '.html';
        //if(file_exists($file))
            require 'views/' . $name . '.php';
        //else {
        //    require $file2;
        //}


    }
}